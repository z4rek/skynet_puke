local CMD = {}
CMD.Main_ClientAndGameSvr_TableModle         =303;  --游戏主命令

CMD.Sub_ClientToGameSvr_TableModle_Base		=1000;
CMD.Sub_GameSvrToClient_TableModle_Base		=2000;

--客户端到服务器
CMD.Sub_ClientToGameSvr_TableModle_StandUpReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+1;   --离开桌子
CMD.Sub_ClientToGameSvr_TableModle_OutCardReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+2;   --出牌
CMD.Sub_ClientToGameSvr_TableModle_DoAnGangReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+3; 
CMD.Sub_ClientToGameSvr_TableModle_CanelAnGangReq			= CMD.Sub_ClientToGameSvr_TableModle_Base+4; 
CMD.Sub_ClientToGameSvr_TableModle_DoJiaGangReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+5;
CMD.Sub_ClientToGameSvr_TableModle_CanelJiaGangReq			= CMD.Sub_ClientToGameSvr_TableModle_Base+6; 
CMD.Sub_ClientToGameSvr_TableModle_CanelJieGangReq			= CMD.Sub_ClientToGameSvr_TableModle_Base+7; 
CMD.Sub_ClientToGameSvr_TableModle_CanelPengReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+8;
CMD.Sub_ClientToGameSvr_TableModle_DoJieGangReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+9;
CMD.Sub_ClientToGameSvr_TableModle_DoPengReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+10;
CMD.Sub_ClientToGameSvr_TableModle_GoOnGameReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+11;  --结算界面时点击继续游戏
CMD.Sub_ClientToGameSvr_TableModle_WantVoiceReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+12;  --9个内置语音的发送，只发送编号即可
CMD.Sub_ClientToGameSvr_TableModle_WantFaceReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+13;
CMD.Sub_ClientToGameSvr_TableModle_WantJieSanReq			= CMD.Sub_ClientToGameSvr_TableModle_Base+14;  --请求解散
CMD.Sub_ClientToGameSvr_TableModle_DoChoice					= CMD.Sub_ClientToGameSvr_TableModle_Base+15;
CMD.Sub_ClientToGameSvr_TableModle_DoHuReq					= CMD.Sub_ClientToGameSvr_TableModle_Base+16;
CMD.Sub_ClientToGameSvr_TableModle_CancelHuReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+17;
CMD.Sub_ClientToGameSvr_TableModle_SendVoiceReqs			= CMD.Sub_ClientToGameSvr_TableModle_Base+19;
CMD.Sub_ClientToGameSvr_TableModle_SendVoiceReqs			= CMD.Sub_ClientToGameSvr_TableModle_Base+19;
CMD.Sub_ClientToGameSvr_TableModle_CancelDelicadeReq		= CMD.Sub_ClientToGameSvr_TableModle_Base+20;
CMD.Sub_ClientToGameSvr_TableModle_SendTxtReqs				= CMD.Sub_ClientToGameSvr_TableModle_Base+21;  --聊天输入文字发送
CMD.Sub_ClientToGameSvr_TableModle_PropReq				    = CMD.Sub_ClientToGameSvr_TableModle_Base+22;  --客户端请求操作道具(友趣的扔鸡蛋) 

--zql	2017/01/12
CMD.Sub_ClientToGameSvr_TableModle_DoChiReq					= CMD.Sub_ClientToGameSvr_TableModle_Base+30;
CMD.Sub_ClientToGameSvr_TableModle_CanelChiReq				= CMD.Sub_ClientToGameSvr_TableModle_Base+31;
CMD.Sub_ClientToGameSvr_TableModle_PingPong					= CMD.Sub_ClientToGameSvr_TableModle_Base+32; --客户端发送网络状态监测包信号
CMD.Sub_ClientToGameSvr_TableModle_ReadyActionReq			= CMD.Sub_ClientToGameSvr_TableModle_Base+33; --客户端举手准备的相关动作
CMD.Sub_ClientToGameSvr_TableModle_GPSData                  = CMD.Sub_ClientToGameSvr_TableModle_Base+34; --客户端发送自己的GPS信息

CMD.Sub_ClientToGameSvr_TableModle_QiangZhuang              = CMD.Sub_ClientToGameSvr_TableModle_Base+40;--玩家抢庄
CMD.Sub_ClientToGameSvr_TableModle_SelectBeiLv				= CMD.Sub_ClientToGameSvr_TableModle_Base+41;--玩家押注
CMD.Sub_ClientToGameSvr_TableModle_UserDealCard				= CMD.Sub_ClientToGameSvr_TableModle_Base+42;--玩家理牌	
CMD.Sub_ClientToGameSvr_TableModle_XianJiaSelect			= CMD.Sub_ClientToGameSvr_TableModle_Base+43;--闲家选择
CMD.Sub_ClientToGameSvr_TableModle_BankerSelect				= CMD.Sub_ClientToGameSvr_TableModle_Base+44;--庄家选择
CMD.Sub_ClientToGameSvr_TableModle_XianJiaSelect2			= CMD.Sub_ClientToGameSvr_TableModle_Base+45;--闲家选择

CMD.Sub_ClientToGameSvr_TableModle_ReportTingResp			= CMD.Sub_ClientToGameSvr_TableModle_Base+46; --报听回复消息

--服务器到客户端
CMD.Sub_GameSvrToClient_TableModle_AllPlayersInfoToMe		= CMD.Sub_GameSvrToClient_TableModle_Base+1; 
CMD.Sub_GameSvrToClient_TableModle_MyPlayerInfoToOhter		= CMD.Sub_GameSvrToClient_TableModle_Base+2; 
CMD.Sub_GameSvrToClient_TableModle_PlayerQuit				= CMD.Sub_GameSvrToClient_TableModle_Base+3;

CMD.Sub_GameSvrToClient_TableModle_GameStart				= CMD.Sub_GameSvrToClient_TableModle_Base+4;    --游戏开始
CMD.Sub_GameSvrToClient_TableModle_GameStartData			= CMD.Sub_GameSvrToClient_TableModle_Base+5;    --游戏开始数据

CMD.Sub_GameSvrToClient_TableModle_UserOutCard				= CMD.Sub_GameSvrToClient_TableModle_Base+6;    --玩家出牌
CMD.Sub_GameSvrToClient_TableModle_UserCancelAnGangOk		= CMD.Sub_GameSvrToClient_TableModle_Base+7;
CMD.Sub_GameSvrToClient_TableModle_UserDoAnGangOk			= CMD.Sub_GameSvrToClient_TableModle_Base+8;
CMD.Sub_GameSvrToClient_TableModle_DispatchCardToMe			= CMD.Sub_GameSvrToClient_TableModle_Base+9;    --发牌给自己
CMD.Sub_GameSvrToClient_TableModle_UserCancelJiaGangOk		= CMD.Sub_GameSvrToClient_TableModle_Base+10;
CMD.Sub_GameSvrToClient_TableModle_UserDoJiaGangOk			= CMD.Sub_GameSvrToClient_TableModle_Base+11;
CMD.Sub_GameSvrToClient_TableModle_CanPengOrJieGang			= CMD.Sub_GameSvrToClient_TableModle_Base+12;
CMD.Sub_GameSvrToClient_TableModle_UserDoJieGangOk			= CMD.Sub_GameSvrToClient_TableModle_Base+13;
CMD.Sub_GameSvrToClient_TableModle_UserDoPengOk				= CMD.Sub_GameSvrToClient_TableModle_Base+14;
CMD.Sub_GameSvrToClient_TableModle_UserCancelPengOk			= CMD.Sub_GameSvrToClient_TableModle_Base+15;

CMD.Sub_GameSvrToClient_TableModle_UserWantVoiceResult		= CMD.Sub_GameSvrToClient_TableModle_Base+19; --内置9个语音
CMD.Sub_GameSvrToClient_TableModle_UserWantFaceResult		= CMD.Sub_GameSvrToClient_TableModle_Base+20;
CMD.Sub_GameSvrToClient_TableModle_8JuGameOver				= CMD.Sub_GameSvrToClient_TableModle_Base+21; --8局结束(好友场)

CMD.Sub_GameSvrToClient_TableModle_OfflineBackData			= CMD.Sub_GameSvrToClient_TableModle_Base+22; --断线重回

CMD.Sub_GameSvrToClient_TableModle_PlayerJieSanReqBroad		= CMD.Sub_GameSvrToClient_TableModle_Base+23; --通知所有玩家，某某某请求解散房间
CMD.Sub_GameSvrToClient_TableModle_PlayerJisSanMsgBradd		= CMD.Sub_GameSvrToClient_TableModle_Base+24; --某个玩家操作是否同意解散房间后，通知当前结果
CMD.Sub_GameSvrToClient_TableModle_UpdateTingCardData		= CMD.Sub_GameSvrToClient_TableModle_Base+25; --听牌数据
CMD.Sub_GameSvrToClient_TableModle_TableOwnerDissTable		= CMD.Sub_GameSvrToClient_TableModle_Base+26; --房主强行解散桌子
CMD.Sub_GameSvrToClient_TableModle_TableUserReady			= CMD.Sub_GameSvrToClient_TableModle_Base+27; --玩家准备，继续游戏
CMD.Sub_GameSvrToClient_TableModle_UpdateAllPlayerScore		= CMD.Sub_GameSvrToClient_TableModle_Base+28; --更新成绩信息

CMD.Sub_GameSvrToClient_TableModle_CanDianPaoHu				= CMD.Sub_GameSvrToClient_TableModle_Base+29; --别人放炮，可以胡
CMD.Sub_GameSvrToClient_TableModle_HuPai					= CMD.Sub_GameSvrToClient_TableModle_Base+30;
CMD.Sub_GameSvrToClient_TableModle_HuPaiResult				= CMD.Sub_GameSvrToClient_TableModle_Base+31;
CMD.Sub_GameSvrToClient_TableModle_UserCancelHu				= CMD.Sub_GameSvrToClient_TableModle_Base+32; --玩家取消胡牌

CMD.Sub_GameSvrToClient_TableModle_RoomJieSan				= CMD.Sub_GameSvrToClient_TableModle_Base+33; --房间解散了，前端直接显示8局结果

CMD.Sub_GameSvrToClient_TableModle_ChouZhang				= CMD.Sub_GameSvrToClient_TableModle_Base+34; --玩家流局
CMD.Sub_GameSvrToClient_TableModle_UpdateFengHuState		= CMD.Sub_GameSvrToClient_TableModle_Base+35; --玩家封胡状态改变
CMD.Sub_GameSvrToClient_TableModle_UpdateDisconnectState	= CMD.Sub_GameSvrToClient_TableModle_Base+36; --玩家断线状态改变
CMD.Sub_GameSvrToClient_TableModle_DispatchCardToOther		= CMD.Sub_GameSvrToClient_TableModle_Base+37; --发牌信息给其他人
CMD.Sub_GameSvrToClient_TableModle_WaitingForAnotherHu		= CMD.Sub_GameSvrToClient_TableModle_Base+38; --等待别的玩家选择胡操作

CMD.Sub_GameSvrToClient_TableModle_SendVoice		        = CMD.Sub_GameSvrToClient_TableModle_Base+39; --发送给桌子玩家当前的声音

CMD.Sub_GameSvrToClient_TableModle_UpdateDelicadeState      = CMD.Sub_GameSvrToClient_TableModle_Base+40; --更新托管状态

CMD.Sub_GameSvrToClient_TableModle_UpdateAllPlayerJinBi		= CMD.Sub_GameSvrToClient_TableModle_Base+41; --更新玩家金币

--zql 2017/01/11
CMD.Sub_GameSvrToClient_TableModle_UserDoChiOk				= CMD.Sub_GameSvrToClient_TableModle_Base+42; --吃牌完成
CMD.Sub_GameSvrToClient_TableModle_GameHuaData				= CMD.Sub_GameSvrToClient_TableModle_Base+45; --花牌数据
CMD.Sub_GameSvrToClient_TableModle_UserCancelChiOk			= CMD.Sub_GameSvrToClient_TableModle_Base+50; --玩家选择放弃吃操作
CMD.Sub_GameSvrToClient_TableModle_PingPong					= CMD.Sub_GameSvrToClient_TableModle_Base+51; --返回客户端网络状态监测包信号

CMD.Sub_GameSvrToClient_TableModle_CommonHuPaiResult        = CMD.Sub_GameSvrToClient_TableModle_Base+52; --麻将通用结算协议
CMD.Sub_GameSvrToClient_TableModle_SendTxt		            = CMD.Sub_GameSvrToClient_TableModle_Base+53; --发送给桌子玩家当前的输入文字

CMD.Sub_GameSvrToClient_TableModle_OfflineBackNewData       = CMD.Sub_GameSvrToClient_TableModle_Base+54; --断线重回新增协议，返回封胡，听牌数据

CMD.Sub_Sub_GameSvrToClient_TableModle_FangxingData			= CMD.Sub_GameSvrToClient_TableModle_Base+55; --发送翻醒数据

CMD.Sub_GameSvrToClient_TableModleMaPai_UnUse               = CMD.Sub_GameSvrToClient_TableModle_Base + 60; --手上马牌数据给客户端
CMD.Sub_GameSvrToClient_TableModleMaPai_Used                = CMD.Sub_GameSvrToClient_TableModle_Base + 61; --摊牌的马牌数据给客户端

CMD.Sub_GameSvrToClient_TableModle_ReadyActionResp			= CMD.Sub_GameSvrToClient_TableModle_Base + 62; --准备动作的返回结果
CMD.Sub_GameSvrToClient_TableModle_10MIN_CLOSE				= CMD.Sub_GameSvrToClient_TableModle_Base + 63; --10分钟关闭倒计时
CMD.Sub_GameSvrToClient_TableModle_1MIN_CLOSE				= CMD.Sub_GameSvrToClient_TableModle_Base + 64; --1分钟关闭倒计时

CMD.Sub_GameSvrToClient_TableModle_SendTableInfo            = CMD.Sub_GameSvrToClient_TableModle_Base + 70; --用于2.3人麻将时游戏模块内发送给客户端当前是几人游戏

CMD.Sub_GameSvrToClient_TableModle_GameStartOthersData		= CMD.Sub_GameSvrToClient_TableModle_Base + 71; --回放时别人的开局信息
CMD.Sub_GameSvrToClient_TableModle_DispatchOhtersCardToMe   = CMD.Sub_GameSvrToClient_TableModle_Base + 72; --回放时别人摸的牌

CMD.Sub_GameSvrToClient_TableModle_UpdateBaoHuState			= CMD.Sub_GameSvrToClient_TableModle_Base + 73; --玩家包胡状态改变

CMD.Sub_GameSvrToClient_TableModle_PropResp                 = CMD.Sub_GameSvrToClient_TableModle_Base + 74; --客户端操作道具的返回消息（友趣的扔鸡蛋）

CMD.Sub_GameSvrToClient_TableModle_ReportTing               = CMD.Sub_GameSvrToClient_TableModle_Base + 75; --豆花报听
CMD.Sub_GameSvrToClient_TableModle_UpdateBaoTingState       = CMD.Sub_GameSvrToClient_TableModle_Base + 76; --广播报听
CMD.Sub_GameSvrToClient_TableModle_ReportTingToOther        = CMD.Sub_GameSvrToClient_TableModle_Base + 77; --豆花报听给其他人
CMD.Sub_GameSvrToClient_TableModle_FinishReportTing         = CMD.Sub_GameSvrToClient_TableModle_Base + 78; --结束报听
CMD.Sub_GameSvrToClient_TableModle_GPS                      = CMD.Sub_GameSvrToClient_TableModle_Base + 90; --GPS数据通知
CMD.Sub_GameSvrToClient_TableModle_Ext						= CMD.Sub_GameSvrToClient_TableModle_Base + 500;--预留扩展



----------------------------------------------------------------------------------------------------------------------------------

CMD.Sub_GameSvrToClient_TableModle_Game_Puke_StartData		= CMD.Sub_GameSvrToClient_TableModle_Base + 771; --扑克游戏开始数据
CMD.Sub_GameSvrToClient_TableModle_UserOutCardResult		= CMD.Sub_GameSvrToClient_TableModle_Base + 772; --玩家出牌是否有误
CMD.Sub_GameSvrToClient_TableModle_UserCompareCardResult	= CMD.Sub_GameSvrToClient_TableModle_Base + 773; --玩家比牌结果
CMD.Sub_GameSvrToClient_TableModle_PuKeGameOver				= CMD.Sub_GameSvrToClient_TableModle_Base + 774; --扑克结束结算
CMD.Sub_GameSvrToClient_TableModle_PuKeOffBackStat			= CMD.Sub_GameSvrToClient_TableModle_Base + 788; --八张断线重连

CMD.Sub_GameSvrToClient_TableModle_Game_ShuiYu_StartData	= CMD.Sub_GameSvrToClient_TableModle_Base + 775; --水鱼游戏开始数据
CMD.Sub_GameSvrToClient_TableModle_QiangZhuangResult		= CMD.Sub_GameSvrToClient_TableModle_Base + 776; --更新抢庄
CMD.Sub_GameSvrToClient_TableModle_SelectBeiLvResult		= CMD.Sub_GameSvrToClient_TableModle_Base + 777; --更新玩家押注
CMD.Sub_GameSvrToClient_TableModle_UserDealCardResult		= CMD.Sub_GameSvrToClient_TableModle_Base + 778; --更新玩家理牌
CMD.Sub_GameSvrToClient_TableModle_XianJiaSelectResult		= CMD.Sub_GameSvrToClient_TableModle_Base + 779; --闲家选择结果
CMD.Sub_GameSvrToClient_TableModle_BankerSelectResult		= CMD.Sub_GameSvrToClient_TableModle_Base + 780; --庄家选择结果
CMD.Sub_GameSvrToClient_TableModle_BankerOperate			= CMD.Sub_GameSvrToClient_TableModle_Base + 781; --庄家操作
CMD.Sub_GameSvrToClient_TableModle_XianJiaOperate			= CMD.Sub_GameSvrToClient_TableModle_Base + 782; --闲家操作
CMD.Sub_GameSvrToClient_TableModle_ShuiYu_GameStart		    = CMD.Sub_GameSvrToClient_TableModle_Base + 783; --水鱼游戏开始 
CMD.Sub_GameSvrToClient_TableModle_QiangZhuangStart			= CMD.Sub_GameSvrToClient_TableModle_Base + 784; --开始抢庄
CMD.Sub_GameSvrToClient_TableModle_UserIsQiang				= CMD.Sub_GameSvrToClient_TableModle_Base + 785; --玩家是否抢庄
CMD.Sub_GameSvrToClient_TableModle_DanJuJieSuan				= CMD.Sub_GameSvrToClient_TableModle_Base + 786; --水鱼单局结算
CMD.Sub_GameSvrToClient_TableModle_ZongJieSuan				= CMD.Sub_GameSvrToClient_TableModle_Base + 787; --总结算协议
CMD.Sub_GameSvrToClient_TableModle_ShuiYuOffBack			= CMD.Sub_GameSvrToClient_TableModle_Base + 789; --水鱼断线重连协议
CMD.Sub_GameSvrToClient_TableModle_XianJiaHasShuiYu			= CMD.Sub_GameSvrToClient_TableModle_Base + 790; --闲家有水鱼

CMD.Sub_GameSvrToClient_TableModle_HuangShiHuPaiResult		= CMD.Sub_GameSvrToClient_TableModle_Base + 801; --黄石麻将结算协议
CMD.Sub_GameSvrToClient_TableModle_OutHuangShiGangPai       = CMD.Sub_GameSvrToClient_TableModle_Base + 802; --黄石麻将出杠牌
CMD.Sub_GameSvrToClient_TableModle_HuangShi8JuOver          = CMD.Sub_GameSvrToClient_TableModle_Base + 803; --黄石麻将一大局完毕

	
CMD.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_StartData	= CMD.Sub_GameSvrToClient_TableModle_Base + 900; --扑克游戏开始数据
CMD.Sub_GameSvrToClient_TableModle_UserCompareShiSanZhangResult	= CMD.Sub_GameSvrToClient_TableModle_Base + 901; --玩家比牌结果
CMD.Sub_GameSvrToClient_TableModle_PuKeShiSanZhangGameOver		= CMD.Sub_GameSvrToClient_TableModle_Base + 902; --扑克结束结算
CMD.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_MaData		= CMD.Sub_GameSvrToClient_TableModle_Base + 903; --马数据
CMD.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_StyleData	= CMD.Sub_GameSvrToClient_TableModle_Base + 904; --先比花色还是先比点数
CMD.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang8JuOver		= CMD.Sub_GameSvrToClient_TableModle_Base + 905; --总结算协议
CMD.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang_DaQiang		= CMD.Sub_GameSvrToClient_TableModle_Base + 906; --打枪数据
CMD.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang_WanFa		= CMD.Sub_GameSvrToClient_TableModle_Base + 907; --玩法数据



--------------------------------------

CMD.Sub_GameSvrToClient_TableModle_ReplaceXiPai         = CMD.Sub_GameSvrToClient_TableModle_Base + 300;    --替换喜牌
CMD.Sub_GameSvrToClient_TableModle_AskFanJiang          = CMD.Sub_GameSvrToClient_TableModle_Base + 301;    --询问是否翻将
CMD.Sub_GameSvrToClient_TableModle_FanJiangRet          = CMD.Sub_GameSvrToClient_TableModle_Base + 302;    --翻将的值
CMD.Sub_GameSvrToClient_TableModle_AskLiaoLong          = CMD.Sub_GameSvrToClient_TableModle_Base + 303;    --询问是否撂龙
CMD.Sub_GameSvrToClient_TableModle_LiaoLongRet          = CMD.Sub_GameSvrToClient_TableModle_Base + 304;    --撂龙结果
CMD.Sub_GameSvrToClient_TableModle_FirstOutCard         = CMD.Sub_GameSvrToClient_TableModle_Base + 305;    --可以开始出牌了
CMD.Sub_GameSvrToClient_TableModle_CurActHuShu          = CMD.Sub_GameSvrToClient_TableModle_Base + 306;    --当前操作引起的胡数  
CMD.Sub_GameSvrToClient_TableModle_AskMaiZhuang         = CMD.Sub_GameSvrToClient_TableModle_Base + 307;    --询问是否买庄

CMD.Sub_GameSvrToClient_TableModle_NTGameStartData	    = CMD.Sub_GameSvrToClient_TableModle_Base + 310;    --游戏开始数据

CMD.Sub_GameSvrToClient_TableModle_UserDoDuGangOk       = CMD.Sub_GameSvrToClient_TableModle_Base + 311; 

CMD.Sub_GameSvrToClient_TableModle_NTHuPaiResult        = CMD.Sub_GameSvrToClient_TableModle_Base + 320;    --胡牌协议
CMD.Sub_GameSvrToClient_TableModle_NT8JuGameOver        = CMD.Sub_GameSvrToClient_TableModle_Base + 330
--断线重回
CMD.Sub_GameSvrToClient_TableModle_NTOfflineBackData    = CMD.Sub_GameSvrToClient_TableModle_Base + 340


-----------
CMD.Sub_ClientToGameSvr_TableModle_FanJiangOrNot        =  CMD.Sub_ClientToGameSvr_TableModle_Base + 300;
CMD.Sub_ClientToGameSvr_TableModle_LiaoLongOrNot        =  CMD.Sub_ClientToGameSvr_TableModle_Base + 301;
CMD.Sub_ClientToGameSvr_TableModle_MaiZhuangOrNot       =  CMD.Sub_ClientToGameSvr_TableModle_Base + 302;

CMD.Sub_ClientToGameSvr_TableModle_DoDuGangReq   	    =  CMD.Sub_ClientToGameSvr_TableModle_Base + 310; 


------南通斗地主
CMD.Sub_ClientToGameSvr_TableModle_ntDdzMaiZhuang		= CMD.Sub_ClientToGameSvr_TableModle_Base+71;		--买不买庄		       
CMD.Sub_ClientToGameSvr_TableModle_ntDdzJiaoDiZhu       = CMD.Sub_ClientToGameSvr_TableModle_Base+72;       --叫地主
CMD.Sub_ClientToGameSvr_TableModle_ntDdzNotJiao			= CMD.Sub_ClientToGameSvr_TableModle_Base+73;		--不叫	
CMD.Sub_ClientToGameSvr_TableModle_ntDdzTiChuaiGou		= CMD.Sub_ClientToGameSvr_TableModle_Base+74;		--踢踹勾
CMD.Sub_ClientToGameSvr_TableModle_ntDdzOutCard			= CMD.Sub_ClientToGameSvr_TableModle_Base+75;		--用户打牌
CMD.Sub_ClientToGameSvr_TableModle_ntDdzPress			= CMD.Sub_ClientToGameSvr_TableModle_Base+76;		--用户过牌
CMD.Sub_ClientToGameSvr_TableModle_ntDdzConfirmDZ		= CMD.Sub_ClientToGameSvr_TableModle_Base+77;		--地主被确认时客户端返回

CMD.Sub_GameSvrToClient_TableModle_ntDdzStart         	= CMD.Sub_GameSvrToClient_TableModle_Base + 1201;	--游戏开始数据
CMD.Sub_GameSvrToClient_TableModle_ntDdzJiaoDiZhu     	= CMD.Sub_GameSvrToClient_TableModle_Base + 1202;	--叫地主
CMD.Sub_GameSvrToClient_TableModle_ntDdzTiChuaiGou		= CMD.Sub_GameSvrToClient_TableModle_Base + 1203;	--踢踹勾
CMD.Sub_GameSvrToClient_TableModle_ntDdzOutCard         = CMD.Sub_GameSvrToClient_TableModle_Base + 1204;	--用户打牌
CMD.Sub_GameSvrToClient_TableModle_ntDdzConclued        = CMD.Sub_GameSvrToClient_TableModle_Base + 1205;	--斗地主结算
CMD.Sub_GameSvrToClient_TableModle_ntDdzPress			= CMD.Sub_GameSvrToClient_TableModle_Base + 1206;	--用户过牌
CMD.Sub_GameSvrToClient_TableModle_ntDdzTotalConclued   = CMD.Sub_GameSvrToClient_TableModle_Base + 1207;	--斗地主总结算
CMD.Sub_GameSvrToClient_TableModle_ntDdzZhaDanUpdate    = CMD.Sub_GameSvrToClient_TableModle_Base + 1208;	--斗地主炸弹触发更新  
CMD.Sub_GameSvrToClient_TableModle_ntDdzOfflineBack     = CMD.Sub_GameSvrToClient_TableModle_Base + 1209;	--斗地主断线重连
CMD.Sub_GameSvrToClient_TableModle_ntDdzMaiZhuang       = CMD.Sub_GameSvrToClient_TableModle_Base + 1210;	--买庄


return CMD