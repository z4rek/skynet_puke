local CMD = {}

-- 网关模块
CMD.Main_ClientToSvr_NetGate               = 200
CMD.Main_SvrToClient_NetGate               = 201

CMD.Sub_ClientToSvr_NetGate_Base           = 1000
CMD.Sub_SvrToClient_NetGate_Base           = 2000

CMD.Sub_ClientToGate_Hand                  = CMD.Sub_ClientToSvr_NetGate_Base + 1
CMD.Sub_ClientToGate_Auth                  = CMD.Sub_ClientToSvr_NetGate_Base + 2
CMD.Sub_ClientToGate_ConnectGameSer        = CMD.Sub_ClientToSvr_NetGate_Base + 3
CMD.Sub_ClientToGate_CheckNet              = CMD.Sub_ClientToSvr_NetGate_Base + 4
CMD.Sub_ClientToGate_GetNetInfor           = CMD.Sub_ClientToSvr_NetGate_Base + 5
CMD.Sub_ClientToGate_LoginSuccess          = CMD.Sub_ClientToSvr_NetGate_Base + 6

CMD.Sub_GateToClient_Hand                  = CMD.Sub_SvrToClient_NetGate_Base + 1
CMD.Sub_GateToClient_Auth                  = CMD.Sub_SvrToClient_NetGate_Base + 2
CMD.Sub_GateToClient_ConnectGameSer        = CMD.Sub_SvrToClient_NetGate_Base + 3
CMD.Sub_GateToClient_CheckNet              = CMD.Sub_SvrToClient_NetGate_Base + 4
CMD.Sub_GateToClient_PreTableID            = CMD.Sub_SvrToClient_NetGate_Base + 5


CMD.Main_ClientAndGameSvr_UserModle         = 300      --大厅主命令
CMD.Main_ClientAndGameSvr_TableModle        = 303      --好友场主命令

CMD.Main_Max                                = 500
CMD.sub_Max                                 = 3000    


--**************************************中心服模块**********************************************************--

CMD.Main_Center_module                     = 210
--子码索引
CMD.Sub_ModuleToCenter_Center_Base         = 1000
CMD.Sub_CenterToModule_Center_Base         = 2000

-- 发送到中心服
CMD.Sub_ModuleToCenter_ClearTableReq       = CMD.Sub_ModuleToCenter_Center_Base + 1 	--客户端清理用户身上的桌子
CMD.Sub_ModuleToCenter_CreateTable         = CMD.Sub_ModuleToCenter_Center_Base + 2     --游戏服务器桌子创建
CMD.Sub_ModuleToCenter_UpdateTable         = CMD.Sub_ModuleToCenter_Center_Base + 3     --游戏服务器更新桌子信息
CMD.Sub_ModuleToCenter_CheckNet            = CMD.Sub_ModuleToCenter_Center_Base + 4     --游戏服务器心跳
CMD.Sub_ModuleToCenter_Query               = CMD.Sub_ModuleToCenter_Center_Base + 5     --游戏服务器通过网关查询用户桌子id 
CMD.Sub_ModuleToCenter_UserDisn            = CMD.Sub_ModuleToCenter_Center_Base + 6     --网关通知中心服用户断线
CMD.Sub_ModuleToCenter_ClearMatchReq       = CMD.Sub_ModuleToCenter_Center_Base + 7 	--客户端清理用户身上的比赛状态

CMD.Sub_ModuleToCenter_GSReg               = CMD.Sub_ModuleToCenter_Center_Base + 100   --游戏服务器注册
CMD.Sub_ModuleToCenter_GSUnReg             = CMD.Sub_ModuleToCenter_Center_Base + 101   --游戏服务器注销

--中心服发到客户端
CMD.Sub_CenterToModule_ClearTableId        = CMD.Sub_CenterToModule_Center_Base + 1     --清理用户身上的桌子信息返回给客户端
CMD.Sub_CenterToModule_ClearMatch          = CMD.Sub_CenterToModule_Center_Base + 2     --清理用户身上的比赛状态返回给客户端
--CMD.Sub_CenterToModule_QueryResult         = CMD.Sub_CenterToModule_Center_Base + 2  --中心服务器返回查询给客户端用户桌子id



--**************************************大厅服模块**********************************************************--

CMD.Main_hall_module                       = 215
--子码索引
CMD.Sub_ModuleToHall_Base         		   = 1000
CMD.Sub_HallToModule_Base         		   = 2000
--客户端到服务器


CMD.Sub_ClientToHall_CreateAgentTableReq   = CMD.Sub_ModuleToHall_Base + 101   -- 代理创建桌子
CMD.Sub_ClientToHall_CreateAgentTableReq2  = CMD.Sub_ModuleToHall_Base + 102   -- 代理创建桌子二次确认
CMD.Sub_ClientToHall_AgentGetRoomList      = CMD.Sub_ModuleToHall_Base + 103   -- 代理察看已开桌子(未开始游戏的)
CMD.Sub_ClientToHall_AgentDismissTable     = CMD.Sub_ModuleToHall_Base + 104   -- 代理解散桌子(未开始游戏的)

CMD.Sub_ClientToHall_UserActGroup		   = CMD.Sub_ModuleToHall_Base + 110   -- 客户端请求加或退亲友群
CMD.Sub_ClientToHall_GetUserGroupID		   = CMD.Sub_ModuleToHall_Base + 111   -- 获取自己亲友群
CMD.Sub_ClientToHall_GetUserApplyGroupList  = CMD.Sub_ModuleToHall_Base + 112   --获取亲友群操作记录
CMD.Sub_ClientToHall_QueryUserGroupID  = CMD.Sub_ModuleToHall_Base + 113   		--查询某人的亲友群

 
--CMD.Sub_ClientToHall_ClientUpdateDiamondReq = CMD.Sub_ModuleToHall_Base + 105  -- 客户端请求刷新钻石 




CMD.Sub_HallToModule_BoardTohall           = CMD.Sub_HallToModule_Base + 8
CMD.Sub_HallToModule_BoardTogame           = CMD.Sub_HallToModule_Base + 9

CMD.Sub_RegToHall                          = CMD.Sub_HallToModule_Base + 100   -- 游戏服务器注册
CMD.Sub_CreateAgentTableResp               = CMD.Sub_HallToModule_Base + 101   -- 返回代理创建桌子的结果
CMD.Sub_CreateAgentTableResp2              = CMD.Sub_HallToModule_Base + 102   -- 返回代理二次创建桌子的结果
CMD.Sub_AgentRoomList                      = CMD.Sub_HallToModule_Base + 103   -- 返回代理创建的等待中桌子
CMD.Sub_AgentDismissTableResp              = CMD.Sub_HallToModule_Base + 104   -- 返回代理解散桌子的结果
CMD.Sub_NotifyClientUpdateDiamond          = CMD.Sub_HallToModule_Base + 105   -- 服务器通知客户端刷新钻石
-- CMD.Sub_ClientUpdateDiamondResp            = CMD.Sub_HallToModule_Base + 106   -- 服务器返回钻石信息
CMD.Sub_NotifyTableDismiss                 = CMD.Sub_HallToModule_Base + 107   -- 服务器通知桌子解散(游戏服务器通知中心服)

CMD.Sub_HallToClient_UserActGroup		   = CMD.Sub_HallToModule_Base + 110   -- 客户端请求加或退亲友群
CMD.Sub_HallToClient_GetUserGroupID		   = CMD.Sub_HallToModule_Base + 111	--获取亲友群
CMD.Sub_HallToClient_GetUserApplyGroupList  = CMD.Sub_HallToModule_Base + 112	--获取亲友群操作记录
CMD.Sub_HallToClient_QueryUserGroupID  	= CMD.Sub_HallToModule_Base + 113   		--查询某人的亲友群

CMD.Sub_UnRegToHall                        = CMD.Sub_HallToModule_Base + 200   -- 游戏服务器注销

--*********************************************消息模块**********************************************--

CMD.Main_MsgSer_module                      = 220 
CMD.Sub_ModuleToMsgSer_Base         	    = 1000
CMD.Sub_MsgToMoMsgSer_Base         		    = 2000

CMD.Sub_ModuleToMsgSer_HasNewMsgReq        = CMD.Sub_ModuleToMsgSer_Base + 5    --获取是否有新消息
CMD.Sub_ModuleToMsgSer_GetNewMsgReq        = CMD.Sub_ModuleToMsgSer_Base + 6    --获取未读邮件和消息
CMD.Sub_ModuleToMsgSer_GetMsgInfoReq       = CMD.Sub_ModuleToMsgSer_Base + 7    --获取消息内容
CMD.Sub_ModuleToMsgSer_DeleteMsgReq        = CMD.Sub_ModuleToMsgSer_Base + 8    --请求删除消息
CMD.Sub_ModuleToMsgSer_GetMailAward        = CMD.Sub_ModuleToMsgSer_Base + 9      --比赛领奖

CMD.Sub_MsgSerToModule_HasNewMsgResult     = CMD.Sub_MsgToMoMsgSer_Base + 4      --返回是是否有新消息
CMD.Sub_MsgSerToModule_NewMsgBack          = CMD.Sub_MsgToMoMsgSer_Base + 5      --未读邮件和消息返回
CMD.Sub_MsgSerToModule_MsgInfoBack         = CMD.Sub_MsgToMoMsgSer_Base + 6      --消息内容详情返回
CMD.Sub_MsgSerToModule_MsgDeleteBack       = CMD.Sub_MsgToMoMsgSer_Base + 7      --消息删除结果返回
CMD.Sub_ModuleToMsgSer_GetMailAwardResult  = CMD.Sub_MsgToMoMsgSer_Base + 8      --比赛领奖结果
--********************************************活动服和其他*********************************************--

CMD.Main_ActivityAndElse_module              = 225
--子码索引
CMD.Sub_ModuleToActivity_Base                = 1000
CMD.Sub_CenterToActivity_Base                = 2000


CMD.Sub_ModuleToActivity_SetWXShare          = CMD.Sub_ModuleToActivity_Base + 1     --客户端微信分享成功
CMD.Sub_ModuleToActivity_IsWXShare           = CMD.Sub_ModuleToActivity_Base + 2     --客户端请求是否有微信分享活动
CMD.Sub_ModuleToActivity_GetActiveList       = CMD.Sub_ModuleToActivity_Base + 3     --客户端获取所有活动列表
CMD.Sub_ModuleToActivity_SetActiveRead       = CMD.Sub_ModuleToActivity_Base + 4     --客户端设置一个活动已读

--服务器到客户端 
CMD.Sub_ActivityToModule_IsWXShare           = CMD.Sub_CenterToActivity_Base + 1     --分享按钮是否加领奖标识
CMD.Sub_ActivityToModule_GetWXShare          = CMD.Sub_CenterToActivity_Base + 2     --分享之后是否可以领奖
CMD.Sub_ActivityToModule_BackActiveList      = CMD.Sub_CenterToActivity_Base + 3     --返回活动列表



--******************************************* 比赛相关 *********************************************--

CMD.GameType_Friend                = 1                                                -- 好友场
CMD.GameType_Gold                  = 2                                                -- 金币场
CMD.GameType_Match                 = 3                                                -- 比赛场

CMD.MatchState_RunServer           = 10                                               --启动服务器  --未发布 非 已发布  发布,编辑,删除
CMD.MatchState_ReadyMatch          = 20                                               --准备比赛    --撤销、查看、删除
CMD.MatchState_Registered          = 21                                               --已报名
CMD.MatchState_Matching            = 30                                               --比赛中      --查看、暂停
CMD.MatchState_Match_end           = 40                                               --比赛结束    --撤销、查看、删除
CMD.MatchState_Pauseing            = 50                                               --暂停中      --查看、下线
CMD.MatchState_Error               = 60                                               --异常        --查看, 删除
CMD.MatchState_Invalid             = 70                                               --失效        --编辑、删除

CMD.Match_ChuSai                   = 1                                                -- 初赛状态 --比赛服务器
CMD.Match_YuSai                    = 2                                                -- 预赛状态- -比赛服务器
CMD.Match_JueSai                   = 3                                                -- 决赛状态 --比赛服务器


--比赛模块需要处理
CMD.MacthContrl_pause              = 1                                                --暂停
CMD.MacthContrl_publish            = 2                                                --发布
CMD.MacthContrl_not_publish        = 3                                                --撤消
CMD.MacthContrl_not_online         = 4                                                --下线

--协调模块可处理                   
CMD.MatchContrl_edit               = 5                                                --编辑
CMD.MatchContrl_delete             = 6                                                --删除
CMD.MatchContrl_view               = 7                                                --查看
CMD.MatchContrl_new                = 8                                                --新增


CMD.MatchType_fullmatch            = 1                                                --人满开赛
CMD.MatchType_timematch            = 2                                                --定时开赛

-- 主码 --
CMD.Main_match_module       	   = 230

--子码索引
CMD.Sub_ModuleToMatch_Base         = 1000
CMD.Sub_MatchToModule_Base         = 2000

CMD.Sub_ModuleToMatchManage_Base   = 2500                                         	 -- 比赛协调模块基础
CMD.Sub_MatchManage_BaseToModule   = 2600                                         	 -- 比赛协调模块基础

--客户端到服务器
CMD.Sub_ModuleToMatch_GetBiSaiInfoReq						= CMD.Sub_ModuleToMatchManage_Base + 1 --请求比赛大厅信息消息
CMD.Sub_ModuleToMatch_GetSingleBiSaiReq					    = CMD.Sub_ModuleToMatch_Base + 1 --获取比赛详细信息请求
CMD.Sub_ModuleToMatch_PlayerBaoMingReq						= CMD.Sub_ModuleToMatch_Base + 2 --报名消息
CMD.Sub_ModuleToMatch_UpdateNowNomberReq					= CMD.Sub_ModuleToMatchManage_Base + 3 --人数刷新请求
CMD.Sub_ModuleToMatch_BiSaiRankDataReq						= CMD.Sub_ModuleToMatch_Base + 4 --比赛排行榜数据请求
CMD.Sub_ModuleToMatch_GetBiSaiRoomInfoReq					= CMD.Sub_ModuleToMatch_Base + 5 --获取比赛房间信息请求
CMD.Sub_ModuleToMatch_CancelBaoMinReq						= CMD.Sub_ModuleToMatch_Base + 6 --取消报名请求
CMD.Sub_ModuleToMatch_BiSaiOffBack                          = CMD.Sub_ModuleToMatch_Base + 7 --断线重连请求
CMD.Sub_ModuleToMatch_BiSaiPreJoinBack                      = CMD.Sub_ModuleToMatch_Base + 8 --进入假桌子返回
CMD.Sub_ModuleToMatch_GetBiSaiHelpInfo						= CMD.Sub_ModuleToMatch_Base + 9 --获取比赛帮助信息
CMD.Sub_ModuleToMatch_GetBaoMingNum                         = CMD.Sub_ModuleToMatch_Base + 10 --在假桌子等待，刷新人数
--服务器到客户端
CMD.Sub_MatchToModule_BiSaiInfoBack						    = CMD.Sub_MatchManage_BaseToModule + 1 --比赛大厅信息返回消息
CMD.Sub_MatchToModule_SingleBiSaiBack						= CMD.Sub_MatchToModule_Base + 1 --比赛详细信息返回
CMD.Sub_MatchToModule_PlayerBaoMingResult					= CMD.Sub_MatchToModule_Base + 2 --玩家报名结果消息
CMD.Sub_MatchToModule_UpdateNowNomberBack					= CMD.Sub_MatchManage_BaseToModule + 3 --人数刷新数据返回
CMD.Sub_MatchToModule_BiSaiRankDataBack					    = CMD.Sub_MatchToModule_Base + 4 --比赛排行榜数据返回
CMD.Sub_MatchToModule_NotifyMatchStart                      = CMD.Sub_MatchToModule_Base + 5 --通知比赛开始
CMD.Sub_ModuleToMatch_CancelBaoMinBack						= CMD.Sub_MatchToModule_Base + 6 --取消报名返回
CMD.Sub_ModuleToMatch_NotifyMatchStart5Min					= CMD.Sub_MatchToModule_Base + 7 --5分钟通知
CMD.Sub_ModuleToMatch_NotifyMatchPause					    = CMD.Sub_MatchToModule_Base + 8 --比赛暂停通知已报名的用户
CMD.Sub_ModuleToMatch_BiSaiOffBackHall                      = CMD.Sub_MatchToModule_Base + 9 --断线重连大厅数据返回
CMD.Sub_ModuleToMatch_BiSaiOffBackMatch                     = CMD.Sub_MatchToModule_Base + 10 --断线重连比赛数据返回
CMD.Sub_ModuleToMatch_BiSaiHelpInfoBack						= CMD.Sub_MatchToModule_Base + 11 --帮助信息返回
CMD.Sub_ModuleToMatch_RankChangeBack						= CMD.Sub_MatchToModule_Base + 12 --排名变化，返回用户的排位
CMD.Sub_ModuleToMatch_UserNumChangeBack						= CMD.Sub_MatchToModule_Base + 13 --报名人数变化，返回当前的报名人数
CMD.Sub_ModuleToMatch_SendAllUserNikeName	                = CMD.Sub_MatchToModule_Base + 14 --通知客户端排行榜的用户昵称
CMD.Sub_ModuleToMatch_BackBaoMingNum	                    = CMD.Sub_MatchToModule_Base + 15 --在假桌子等待，刷新人数返回
CMD.Sub_ModuleToMatch_SendAllUserLiuJu	                    = CMD.Sub_MatchToModule_Base + 16 --通知本场比赛由于流局加赛
CMD.Sub_ModuleToMatch_SendUserFailJoinMatch   	            = CMD.Sub_MatchToModule_Base + 17 --通知玩家比赛由于各种原因没能加入比赛桌（比如安卓切后台一直没回复预加入成功消息）

--比赛模块到游戏服务器
CMD.Sub_CreateMatchTable		       					    = CMD.Sub_ModuleToMatch_Base     + 101     -- 比赛创建桌子
CMD.Sub_NotifyMatchOver                                     = CMD.Sub_ModuleToMatch_Base     + 102     -- 通知比赛结束(针对全部比赛桌)
CMD.Sub_NotifyMatchStart                                    = CMD.Sub_ModuleToMatch_Base     + 103     -- 通知比赛开始(到达指定桌子)

--游戏服务器与比赛模块
CMD.Sub_RecvCreateMatchTable		       				    = CMD.Sub_MatchToModule_Base     + 101   -- 比赛创建桌子返回
CMD.Sub_RecvTableScore      		       				    = CMD.Sub_MatchToModule_Base     + 102   -- 比赛桌子的分数


CMD.Sub_MatchToModule_PreEnterRoomData						= CMD.Sub_MatchToModule_Base    + 151 --预加入比赛房消息
CMD.Sub_MatchToModule_RoundOverData							= CMD.Sub_MatchToModule_Base    + 152 --一轮比赛结束等待通知消息
CMD.Sub_MatchToModule_RoundOverJinJiData					= CMD.Sub_MatchToModule_Base    + 153 --一轮比赛结束晋级消息通知
CMD.Sub_MatchToModule_BiSaiOverResult						= CMD.Sub_MatchToModule_Base    + 154 --比赛总结果下发消息
CMD.Sub_MatchToModule_BiSaiRoomInfo							= CMD.Sub_MatchToModule_Base    + 155 --比赛房间信息消息


-------------绑定手机-------------

CMD.Main_RegPhoneSer_module                     = 227 
CMD.Sub_ModuleToRegPhoneSer_Base           	    = 1000
CMD.Sub_RegPhoneToMoMsgSer_Base        		    = 2000

CMD.Sub_RequryHadRegisterdReq                   = CMD.Sub_ModuleToRegPhoneSer_Base + 1     --查询是否注册过
CMD.Sub_SendVerificationCodeReq                 = CMD.Sub_ModuleToRegPhoneSer_Base + 2     --发送验证码请求
CMD.Sub_RequstBindPhoneReq                      = CMD.Sub_ModuleToRegPhoneSer_Base + 3     --绑定手机请求

CMD.Sub_RequryHadRegisterdResp                  = CMD.Sub_RegPhoneToMoMsgSer_Base + 1      --回复查询是否注册过
CMD.Sub_SendVerificationCodeResp                = CMD.Sub_RegPhoneToMoMsgSer_Base + 2      --发送验证码请求
CMD.Sub_RequstBindPhoneResp                     = CMD.Sub_RegPhoneToMoMsgSer_Base + 3      --绑定手机请求

-------------------------------
-- 豆花代理系统
CMD.Main_Douhua_module                          = 235    -- 主码

CMD.Sub_ClientToServer_Base                     = 1000
CMD.Sub_ServerToClient_Base                     = 2000

CMD.Sub_Douhua_UserAgentQuery                   = CMD.Sub_ClientToServer_Base + 1         -- 用户查询代理状态
CMD.Sub_Douhua_CheckPhoneNumberReq              = CMD.Sub_ClientToServer_Base + 2         -- 用户查询手机号码是否可用
CMD.Sub_Douhua_MPVcodeReq                       = CMD.Sub_ClientToServer_Base + 3         -- 用户请求手机验证码
CMD.Sub_Douhua_SubmitInfoReq                    = CMD.Sub_ClientToServer_Base + 4         -- 用户提交资料
CMD.Sub_Douhua_CheckRecharge                    = CMD.Sub_ClientToServer_Base + 5         -- 用户充值后，查询是否成功(目前是通过客户端来做的)

CMD.Sub_Douhua_UserAgentStatus                  = CMD.Sub_ServerToClient_Base + 1         -- 用户代理状态返回
CMD.Sub_Douhua_CheckPhoneNumberResp             = CMD.Sub_ServerToClient_Base + 2         -- 用户手机号码检测结果
CMD.Sub_Douhua_MPVcodeResp                      = CMD.Sub_ServerToClient_Base + 3         -- 返回用户手机验证码
CMD.Sub_Douhua_SubmitInfoResp                   = CMD.Sub_ServerToClient_Base + 4         -- 用户提交资料返回结果
CMD.Sub_Douhua_RegDone                          = CMD.Sub_ServerToClient_Base + 5         -- 注册完成(充值后)，下发资料
CMD.Sub_Douhua_RechargeResp                     = CMD.Sub_ServerToClient_Base + 6         -- 充值结果，可能导致升级


return CMD
