local _class={}

function class(super)
	local class_type={}
	class_type.ctor=false
	class_type.super=super
	class_type.new=function(...) 
			local obj={}
			do
				local create
				create = function(c,...)
					if c.super then
						create(c.super,...)
					end
					if c.ctor then
						c.ctor(obj,...)
					end
				end
 
				create(class_type,...)
			end
			setmetatable(obj,{ __index=_class[class_type] })
			return obj
		end
	local vtbl={}
	_class[class_type]=vtbl
 
	setmetatable(class_type,{__newindex=
		function(t,k,v)
			vtbl[k]=v
		end
	})
 
	if super then
		setmetatable(vtbl,{__index=
			function(t,k)
				local ret=_class[super][k]
				vtbl[k]=ret
				return ret
			end
		})
	end
 
	return class_type
end


local buffread=class()		
 
function buffread:ctor(msg)
    self.flag = 1
    self.msg = msg
end

function buffread:proto()  
    local outmsg1, outmsg2 = string.unpack(">I2>I2", self.msg, self.flag)
    self.flag = self.flag + 4    
    return outmsg1, outmsg2
end 

function buffread:int1()	
	local outmsg = string.unpack("I1", self.msg, self.flag)
    self.flag = self.flag + 1
    return outmsg
end

function buffread:int2()	
	local outmsg = string.unpack("I2", self.msg, self.flag)
    self.flag = self.flag + 2
    return outmsg
end

function buffread:int4()	
	local outmsg = string.unpack("i4", self.msg, self.flag)
    self.flag = self.flag + 4
    return outmsg
end

function buffread:int8()	
	local outmsg = string.unpack("i8", self.msg, self.flag)
    self.flag = self.flag + 8
    return outmsg
end

function buffread:Int4()	
	local outmsg = string.unpack("I4", self.msg, self.flag)
    self.flag = self.flag + 4
    return outmsg
end

function buffread:Int8()	
	local outmsg = string.unpack("I8", self.msg, self.flag)
    self.flag = self.flag + 8
    return outmsg
end

function buffread:float()
	local outmsg = string.unpack("f", self.msg, self.flag)
    self.flag = self.flag + 4
    return outmsg
end

function buffread:str(strlen)

    local outmsg = string.unpack("c"..strlen, self.msg, self.flag)
    self.flag = self.flag + strlen
    return outmsg
end


local  buffwrite=class()		
 
function buffwrite:ctor()		
	self.buff=""
end
 
function buffwrite:proto(arg1, arg2)

    local addmsg = string.pack(">I2>I2", arg1, arg2)  
    self.buff = self.buff..addmsg
end

function buffwrite:int1(arg)
    
    local addmsg = string.pack("<I1", math.floor(arg))
    self.buff = self.buff..addmsg
end

function buffwrite:int2(arg)
    
    local addmsg = string.pack("<I2", math.floor(arg))   
    self.buff = self.buff..addmsg
end

function buffwrite:int4(arg)	

	local addmsg = string.pack("<i4", math.floor(arg))  
    self.buff = self.buff..addmsg
end

function buffwrite:int8(arg)
    
    local addmsg = string.pack("<i8", math.floor(arg))    
    self.buff = self.buff..addmsg
end

function buffwrite:Int4(arg)	

	local addmsg = string.pack("<I4", math.floor(arg))  
    self.buff = self.buff..addmsg
end

function buffwrite:Int8(arg)	

	local addmsg = string.pack("<I8", math.floor(arg))  
    self.buff = self.buff..addmsg
end

function buffwrite:float(arg)
	local addmsg = string.pack("<f", arg)  
    self.buff = self.buff..addmsg
end

function buffwrite:str(arg, strlen)

   --[[ local temp_len = string.len(arg) 
    if temp_len < strlen then 
        for i = 1, strlen - temp_len do 
            arg = arg.." "
        end
    end ]]--
    local addmsg = string.pack("c"..strlen, arg)  
    self.buff = self.buff..addmsg
end

function buffwrite:clean()
    
    self.buff=""
end

local buffrw = {BuffWrite = buffwrite, BuffRead = buffread}
return buffrw




















