local skynet = require "skynet"
local queue = require "skynet.queue"
local snax = require "snax"

local gateconfig = require "gateconfig"

local lock

function printtable(t)
   for key,value in pairs(t) do
     if type(value) == "table" then
          for key2,value2 in pairs(value) do
              skynet.error("key="..key2.." value="..value2)
          end
      end
    end
end


function reloadLua()
    local fileName = skynet.getenv "tableconfig"
    local ffile = io.open(fileName, "r")  
    local value = ffile:read("*a")
    ffile:close()
    return load(value)()
end

function forgetconfig()
	skynet.fork(function()
        while true do
            gateconfig = reloadLua() 
            skynet.sleep(300)    
        end
    end)
end

function response.getconfig(name, tag)
	if tag == "gateconfig" then
		--skynet.error("getconfig name = ",name)
		return gateconfig
	end

	skynet.error("getconfig nil")

	return nil
end

function response.error()
	skynet.error "throw an error"
end

function init( ... )
	skynet.error("gateconfig = ".. #gateconfig)
	skynet.error("gate config server start:", ...)
	snax.enablecluster()	
	lock = queue()
	forgetconfig()
end

function exit(...)
	skynet.error ("ping server exit:", ...)
end
