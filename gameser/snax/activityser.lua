local skynet = require "skynet"
local mysql = require "mysql"
local snax = require "snax"
local cluster = require "cluster"
local CMD = require "center_proto"
local buffrw = require "buffrw"
local db = require "dblib"

local ACTIVITY = {}
local myDB = {}

function myDB.AddParaIn(querymsg, arg)
    arg = arg or ""
    querymsg = querymsg..string.format("\""..arg.."\",")
	return querymsg
end

function myDB.AddParaOut(querymsg, arg)
	querymsg = querymsg..arg
	return querymsg
end
--************************活动服务，负责微信分享活动...*************************************


function ACTIVITY.IsHasWXShare(playid) -- pr_GetWxShareActive

    local res1, res2 = db.call("call  pr_getwxshareactive("..tonumber(playid)..",@ReturnVal)","select @ReturnVal")
    local result = res2[1]['@ReturnVal']
    skynet.error("[activityser] IsHasWXShare playid: ",playid,result)
    local re = {mcmd = CMD.Main_ActivityAndElse_module,scmd = CMD.Sub_ActivityToModule_IsWXShare, msg = string.pack("<I4",tonumber(result or 0)) }
    return re
end

--返回微信活动结果，和钻石金币
function ACTIVITY.GetShareDiomand(playid) -- pr_GetWxShareDiamond

    local res1 = db.call("call  pr_GetWxShareDiamond("..tonumber(playid)..")")

    local addcount,sumcount

    for index,table_obj in pairs(res1[1]) do
        addcount=table_obj['AddnCardCount']
       -- sumcount=table_obj['nCardCount']
    end
    skynet.error("[activityser] GetShareDiomand playid: ",playid,"adddiomand: ",addcount)
    local re = {mcmd = CMD.Main_ActivityAndElse_module,scmd = CMD.Sub_ActivityToModule_GetWXShare, msg = string.pack("<I4",tonumber(addcount)) }
    return re
    
end

function ACTIVITY.ActiveListQuery(playid,activeid)

    local queryin = "call  pr_GetActivityList("
    queryin = myDB.AddParaIn(queryin, playid)
    queryin = myDB.AddParaIn(queryin, activeid)

    local queryout = ""
	queryout = myDB.AddParaOut(queryout, "@ReturnVal")

    local res1, res2 = db.call(queryin..queryout..");", "select "..queryout)

    local result = res2[1]['@ReturnVal']
    return result
end


--获取活动列表(注：和微信分享活动不同)
function ACTIVITY.GetActiveList(playid, msg)
    local BuffRead = buffrw.BuffRead.new(msg)
    local argnum    = BuffRead:int4()
    local active = {}
    for i=1, argnum do
        local activeid   = BuffRead:int4()
        local result = ACTIVITY.ActiveListQuery(playid,activeid)
        active[activeid] = result
        skynet.error("[activityser] GetActiveList playid: ", playid," activeid: ",activeid)
    end
    local BuffWrite = buffrw.BuffWrite.new()
    for id, av in pairs(active)do
       BuffWrite:int4(id)
       BuffWrite:int4(av)
    end
    
    local re = {mcmd = CMD.Main_ActivityAndElse_module,scmd = CMD.Sub_ActivityToModule_BackActiveList, msg = BuffWrite.buff } 
    return re
end

--设置活动已完成(注：和微信分享活动不同)
function ACTIVITY.SetActivityRead(playid, msg)
    local BuffRead = buffrw.BuffRead.new(msg)
    local activeid = BuffRead:int4()
    skynet.error("[activityser] SetActivityRead playid: ", playid," activeid: ",activeid)
    db.call("call  pr_SetActivityRead("..tonumber(playid)..","..activeid..")")
end



function response.request_act(conf)--{cmd,playid,msg}
   skynet.error("[activityser] request_act",conf.playid,conf.cmd )
   if conf.cmd == CMD.Sub_ModuleToActivity_IsWXShare then -- 是否有微信分享活动       
        re = ACTIVITY.IsHasWXShare(conf.playid)
        return re

    elseif conf.cmd == CMD.Sub_ModuleToActivity_SetWXShare then --分享成功，修改数据库
        re = ACTIVITY.GetShareDiomand(conf.playid)
        return re

    elseif conf.cmd == CMD.Sub_ModuleToActivity_GetActiveList then --获取活动列表
        re = ACTIVITY.GetActiveList(conf.playid, conf.msg)
        return re

    elseif conf.cmd == CMD.Sub_ModuleToActivity_SetActiveRead then --设置活动为已完成
        ACTIVITY.SetActivityRead(conf.playid, conf.msg)
        return
    end
end


function init( ... )
	skynet.error("activityser server start:")
	snax.enablecluster()
end

function exit(...)
	skynet.error ("activityser server exit:", ...)
end