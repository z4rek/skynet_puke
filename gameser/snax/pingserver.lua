local skynet = require "skynet"
local queue = require "skynet.queue"
local snax = require "snax"


local i = 0
local hello = "hello"

function response.ping(recv)
	--skynet.sleep(500)
    print("recv :"..recv)
	return hello..":"..recv
end

-- response.sleep and accept.hello share one lock
local lock

function accept.sleep(queue, n)
	if queue then
		lock(
		function()
			print("queue=",queue, n)
			--skynet.sleep(n)
		end)
	else
		print("queue=",queue, n)
		--skynet.sleep(n)
	end
end

function response.sleep2(queue, n, t)
	if queue then
		lock(
		function()
			local max_item = 1
			--print("queue=",queue, n)
			--skynet.sleep(n)
			if t then
				--print(t)
				for i, v in ipairs(t) do
					for j, x in ipairs(v) do
						--print(i,j,x)
					end
					if i > max_item then
						max_item = i
					end 
				end 
			end
			--print("max_item = "..max_item)
		end)
	else
		print("queue=",queue, n)
		--skynet.sleep(n)
	end
	return "ok"
end

function accept.hello()
	lock(function()
	i = i + 1
	print (i, hello)
	end)
end

function accept.exit(...)
	snax.exit(...)
end

function response.error()
	error "throw an error"
end

function init( ... )
	print ("ping server start:", ...)
	snax.enablecluster()	-- enable cluster call
	-- init queue
	lock = queue()
end

function exit(...)
	print ("ping server exit:", ...)
end
