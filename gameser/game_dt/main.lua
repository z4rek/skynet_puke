local skynet = require "skynet"
local sprotoloader = require "sprotoloader"

local mclient = skynet.getenv "max_client" 
local dport = skynet.getenv "debug_port" 
local gatedst_=skynet.getenv "gatedst" 

skynet.start(function()
	skynet.error("Server start")
	if not skynet.getenv "daemon" then
		local console = skynet.newservice("console")
	end
	skynet.newservice("debug_console",dport)
    local mainserver=skynet.newservice("mainserver")
    skynet.call(mainserver, "lua", "start",{server=mainserver,gatedst=gatedst_})
	skynet.exit()
end)
