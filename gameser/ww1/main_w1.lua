local skynet = require "skynet"
local cluster = require "cluster"

local mclient = skynet.getenv "max_client" 
local lport = skynet.getenv "listen_port" 
local dport = skynet.getenv "debug_port" 
local name  = skynet.getenv "name"

 local configserver = nil;
function request_ip() 
       
        if configserver then
            local v = configserver.req.getconfig("name","gateconfig")
        end
end

skynet.start(function()
	skynet.error("Server start ww1")

	if not skynet.getenv "daemon" then
		local console = skynet.newservice("console")
	end
	
    skynet.newservice("debug_console",dport)
    

	local watchdog = skynet.newservice("watchdog")
	skynet.call(watchdog, "lua", "start", {
		port = lport,
		maxclient = tonumber(mclient),
		nodelay = true,
	})

    skynet.newservice("configdb")
    
    local centerconnect = skynet.newservice("centerdb")
	skynet.call(centerconnect, "lua", "config", {
		watchdog = watchdog
	})

    skynet.error("Watchdog listen on", lport)

end)
