local skynet = require "skynet"
local cluster = require "cluster"
local CMD = require "center_proto"

require "skynet.manager"

local watchdog = nil

local command = {}

local centerserver = nil
local hallser = nil
local msgser = nil
local activityser = nil
local regPhoneSev = nil
local matchSev = nil
local douhua = nil

local lgateid = 0

local request_function = {}

function command.rquest_centerser(conf)
    conf.gateid = lgateid
    return cluster.call("master", centerserver,"request_gate",conf) 
end

function command.rquest_hall(conf)
    return hallser.req.request_hall(conf)
end

function command.rquest_msg(conf)
    return msgser.req.request_msg(conf)
end

function command.rquest_act(conf)
    return activityser.req.request_act(conf)
end

function command.rquest_regphone(conf)
    return regPhoneSev.req.request_regPhone(conf)
end

function command.rquest_match(conf)
    return matchSev.req.recv_from_client(conf)
end

function command.rquest_douhua(conf)
    return douhua.req.request_douhua_system(conf)
end

function command.init_function()
    request_function[CMD.Main_Center_module]            = command.rquest_centerser
    request_function[CMD.Main_hall_module]              = command.rquest_hall
    request_function[CMD.Main_MsgSer_module]            = command.rquest_msg
    request_function[CMD.Main_ActivityAndElse_module]   = command.rquest_act
    request_function[CMD.Main_RegPhoneSer_module]       = command.rquest_regphone
    request_function[CMD.Main_match_module]             = command.rquest_match
    request_function[CMD.Main_Douhua_module]            = command.rquest_douhua
end

function command.REQUEST(conf)
    local f = request_function[conf.mcmd]
    if f then
        skynet.error("command.REQUEST mcmd:"..conf.mcmd)
        return f(conf)
    end
    skynet.error("command.REQUEST no find cmd :"..conf.cmd)
    return nil
end

function command.REGITERTOCENT(playid, lagentid)
    skynet.error("REGITERTOCENT to center db")
    --centerserver.post.regToCent({playerid = playid, gateid = lgateid, agentid = lagentid})
    cluster.send("master", centerserver,"regToCent",{playerid = playid, gateid = lgateid, agentid = lagentid}) 
end

function command.CONFIG(conf)
    skynet.error("config to centerdb")
    watchdog = conf.watchdog
end


--启动时注册到中心服务器,并取得网关ＩＤ与ＫＥＹ
function RegGateToCent()
    skynet.error("RegGateToCent "..lgateid)
    --lgateid = centerserver.req.request_registGate()
    lgateid = cluster.call("master", centerserver,"request_registGate")
end

--
function OnTimerGetMessage()
	skynet.fork(function()
        skynet.error("start OnTimerGetMessage ")
        while true do
            --skynet.error("start OnTimerGetMessage "..lgateid)
            --local msgt = centerserver.req.request_getGateMail(lgateid)
            local msgt = cluster.call("master", centerserver,"request_getGateMail",lgateid)
            if msgt ~= nil and type(msgt) == "table" then
                for key,value in pairs(msgt) do
    			    if type(value) == "table" then

    			        local playid  = value.playerid
    			        local agentid = value.agentid

                        -- 广播到整个游戏
                        if agentid == nil then
                            skynet.send(watchdog, "lua", "sendtoallagnet",value)
                        else
                            skynet.send(agentid,"lua","sendPacket",value)
                        end
    			    end
    		    end
            end

            skynet.sleep(20)
        end
    end)
end

local function AllMapSnax()

    skynet.error("---------------*********AllMapSnax start**********------------")
    matchSev    = cluster.snax("center", "matchser")

    skynet.error("start matchser proxy")

	centerserver = cluster.query("master", "centerser")
    skynet.error("start centerser proxy")

    hallser =  cluster.snax("center", "hallser")
    skynet.error("start hallser proxy")

    msgser = cluster.snax("center", "msgser")
    skynet.error("start msgser proxy")

    activityser = cluster.snax("center", "activityser")
    skynet.error("start activityser proxy")

    regPhoneSev = cluster.snax("center", "regphoneser")
    skynet.error("start regphoneser proxy")

    douhua = cluster.snax("center", "douhua_agent_system")
    skynet.error("start douhua proxy")
    skynet.error("---------------*********AllMapSnax end**********------------")
    RegGateToCent()

    OnTimerGetMessage()
    
end


skynet.start(function()
	skynet.error("start center server")
	skynet.dispatch("lua", function(session, address, cmd, ...)
		cmd = cmd:upper()
		local f = command[cmd]
		if f then
			skynet.ret(skynet.pack(f(...)))
		else
			error(string.format("Unknown command %s", cmd))
		end
	end)
    skynet.register ".centerdb"
    AllMapSnax()
    command.init_function()


end)
