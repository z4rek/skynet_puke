package.cpath = "luaclib/?.so"
package.path = "lualib/?.lua"

local crypt = require "crypt"

if _VERSION ~= "Lua 5.3" then
	error "Use lua 5.3"
end

local rgatefile = "./gate.txt"
local wgatefile = "./wgate.txt"
local deskey  = "89231#42"

function zipfile()
    local file = io.open(rgatefile, "rb")
    if file == nil then
        print(""..rgatefile.." = nil")
        return
    end
    local FileData = file:read("*a")
    io.close(file) 
    local sendstr = crypt.desencode(deskey, FileData)

    local tempFile2 = io.open("testgate.txt", "w+b")
    tempFile2:write(sendstr)
    io.close(tempFile2)

    local base64  = crypt.base64encode(sendstr)

    local tempFile = io.open(wgatefile, "w+b")
    tempFile:write(base64)
    io.close(tempFile)
end

function unzipfile()
    local file = io.open(wgatefile, "rb")
    local FileData = file:read("*a")
    io.close(file) 

    local binstr = crypt.base64decode(FileData)

    local outstr  = crypt.desdecode(deskey,binstr)

    print(outstr)

    print("----------------------- 1")
    
    local i = 1
    for w in string.gmatch(outstr,"%d+%.%d+%.%d+%.%d+:%d+") do 
        local ip,port = string.match(w, "(%d+%.%d+%.%d+%.%d+):(%d+)")
        print("ip="..ip.." port="..port)
    end

    print("----------------------- 2")
end

zipfile()

unzipfile()



