package.cpath = "luaclib/?.so"
package.path = "lualib/?.lua;examples/client/?.lua"

if _VERSION ~= "Lua 5.3" then
	error "Use lua 5.3"
end

local socket = require "clientsocket"
local proto = require "proto"
local sproto = require "sproto"

local host = sproto.new(proto.s2c):host "package"
local request = host:attach(sproto.new(proto.c2s))

local allfd = {}

for i=1,500 do
    local fd = assert(socket.connect("127.0.0.1", 9100))
    allfd[i] = {}
    allfd[i].fd = fd
    allfd[i].last = ""
    allfd[i].session = 0
end


local function send_package(fd, pack)
    --print("send_package 1")
	local package = string.pack(">s2", pack)
    --print("send_package 2")
	socket.send(fd, package)
end

local function unpack_package(text)
	local size = #text
	if size < 2 then
		return nil, text
	end
	local s = text:byte(1) * 256 + text:byte(2)
	if size < s+2 then
		return nil, text
	end

    --print("unpack_package:"..text.." sz:"..s)
	return text:sub(3,2+s), text:sub(3+s)
end

local function recv_package(kfd, last)
	local result
	result, last = unpack_package(last)
	if result then
		return result, last
	end
    --print("recv_pakage kfd:"..kfd)
	local r = socket.recv(kfd)
	if not r then
		return nil, last
	end
	if r == "" then
		--error "Server closed"
		print ("Server closed:"..kfd)
	end
	return unpack_package(last .. r)
end

local session = 0

local function send_w(mcmd, scmd,str)
	for i, k in pairs(allfd) do 
        local data = string.pack(">I2>I2z",mcmd,scmd,str)
	    send_package(allfd[i].fd, data)
	end	
	--print("send_w")
end

local function send_request(name, args)
	for i, k in pairs(allfd) do 
	    allfd[i].session = allfd[i].session + 1
	    local str = request(name, args, session)
		send_package(allfd[i].fd, str)
	    --print("Request:", allfd[i].session)
	end	
end

--local last = ""

local function print_request(name, args)
	--print("REQUEST", name)
	if args then
		for k,v in pairs(args) do
			print(k,v)
		end
	end
end

local function print_response(session, args)
	--print("RESPONSE", session)
	if args then
		for k,v in pairs(args) do
			print(k,v)
		end
	end
end

local function print_package(t, ...)
	if t == "REQUEST" then
		print_request(...)
	else
		assert(t == "RESPONSE")
		print_response(...)
	end
end

local function dispatch_package()
	for i,k in pairs(allfd) do 
    local lfd = allfd[i].fd
	while true and lfd do
		local v
		v, k.last = recv_package(lfd, k.last)
		if not v then
			break
		end

		--print_package(host:dispatch(v))
	end
    end
end

local CMD = {}
local chd = false
local handd = false

function CMD.hand()
    --print("cmd hand")
    send_request("handshake")
    handd = true
end

function CMD.get()
    --print("cmd get")
    send_request("get", { what = "hello"})
end

function CMD.set()
    --print("cmd set")
    send_request("set", { what = "hello", value = "world" })
end

function CMD.ch()
    --print("cmd connecth .....")
    send_w(100, 1,"hello")
    chd = true
end

function CMD.cg()
    --print("cmd connectg")
    send_w(100, 2,"hello")
end

function CMD.quit()
    print("cmd quit")
    send_request("quit")
end

while true do
	dispatch_package()
	local cmd = socket.readstdin()
	if cmd then
        local f = CMD[cmd]
        if f then
           f() 
        end
	else
	    socket.usleep(1000)
        --[[
        if chd == false then
            --CMD.ch()
            CMD.cg()
        elseif handd == false then
            CMD.hand()
        else
            CMD.get()
        end
        --]]
	end
end
