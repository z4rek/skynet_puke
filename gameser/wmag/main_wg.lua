local skynet = require "skynet"
local cluster = require "cluster"
local snax = require "snax"

local mclient = skynet.getenv "max_client" 
local lport = skynet.getenv "listen_port" 
local dport = skynet.getenv "debug_port" 

skynet.start(function()
	skynet.error("Server start wg")
	
	if not skynet.getenv "daemon" then
		local console = skynet.newservice("console")
	end
	skynet.newservice("debug_console",dport)
    cluster.open "master"
    
    local centerser = skynet.uniqueservice("centerser") 
    skynet.call(centerser, "lua", "start")
    cluster.register("centerser", centerser)
    
	snax.uniqueservice("gateconfigser")
   -- snax.uniqueservice("centerser")  
     
    local watchdog = skynet.newservice("watchdog")
	skynet.call(watchdog, "lua", "start", {
		port = lport,
		maxclient = tonumber(mclient),
		nodelay = true,
	})
    skynet.error("Watchdog listen on", lport)
	

end)
