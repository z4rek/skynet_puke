local skynet = require "skynet"

local HuShu = {
    peng = {
        [0x40] = {16,16,16,16,16,4},            --百搭是将时，碰的牌是自己，花牌，1， 9，将， 非将
        [0x30] = {16,8,4,4,4,1},                --花牌是将时，碰的牌是自己，花牌，1， 9，将， 非将
        [0x01] = {8,4,8,4,4,1},                 --碰1
        [0x09] = {8,4,4,8,4,1},
        [0x04] = {4,4,4,4,4,1},                 --4，7
        [0x02] = {4,4,4,4,4,1},                 --2 5 8
        [0x03] = {4,4,4,4,4,1},                 --3 6
    },
    
    minggang = {
        [0x40] = {4,4,4,4,4,1},                  --百搭是将时，明杠的牌是自己，花牌，1， 9，将， 非将
        [0x30] = {32,32,16,16,16,4},             --
        [0x01] = {32,16,32,16,16,4},             --碰1
        [0x09] = {32,16,16,32,16,4},
        [0x04] = {16,16,16,16,16,4},             --4，7
        [0x02] = {16,16,16,16,16,4},             --2 5 8
        [0x03] = {16,16,16,16,16,4},             --3 6
    },
    
    angang = {
        [0x40] = {24,24,24,24,24,6},             --百搭是将时，暗杠的牌是自己，花牌，1， 9，将， 非将
        [0x30] = {48,48,24,24,24,6},             --
        [0x01] = {48,24,48,24,24,6},             --碰1
        [0x09] = {48,24,24,48,24,6},
        [0x04] = {24,24,24,24,24,6},             --4，7
        [0x02] = {24,24,24,24,24,6},             --2 5 8
        [0x03] = {24,24,24,24,24,6},             --3 6
    },
    
    liao = {
        [0x40] = {320,32,32,32,32,8},
        [0x30] = {64,64,32,32,32,8,[0]=200},
        [0x01] = {64,32,32,32,32,8,[0]=200},
        [0x09] = {64,32,32,32,32,8,[0]=200},
        [0x04] = {32,32,32,32,32,8,[0]=200},
        [0x02] = {32,32,32,32,32,8,[0]=200},
        [0x03] = {32,32,32,32,32,8,[0]=200}
    },
    
    xipai = {
        [1] = {
            [0x40] = {20,60,100,320},                --百搭是将时，有百搭的个数
            ['default'] = {10,30,50,70,100},         --将不是百搭时，有百搭的个数
        },
        [2] = {
            [0x80] = {50,100,200},     --两个喜牌
            [0x40] = {30,50,100,200},  --有一个喜牌
            ['default'] = {10,30,50,100,200}
        }
        
    },
    
    shuangjiang = {
        [false] = {    --中心牌是普通牌
            liao=16,angang=12,minggang=8,anke=4,peng=2
        },
        [true] = {
            [22] = {liao=32,angang=24,minggang=16,anke=8,peng=4},            --两个喜牌
            [21] = {liao=64,angang=48,minggang=32,anke=16,peng=8},           --一个喜牌，一个大将
            [20] = {liao=32,angang=24,minggang=16,anke=8,peng=4},            --一个喜牌，一个普通牌
            [11] = {liao=128,angang=96,minggang=64,anke=32,peng=16},         --都是大将
            [10] = {liao=64,angang=48,minggang=32,anke=16,peng=8},           --一个大将，一个普通
            [0]  = {liao=32,angang=24,minggang=16,anke=8,peng=4}             --都是普通
            
        },
    }
}

HuShu.jiagang = HuShu.minggang
HuShu.dugang = HuShu.angang

function HuShu.GetXiPaiHuShu(jConfig, jiang, numXipai)
    if jConfig == 1 then 
        if jiang ~= 0x40 then jiang = 'default' end
        return HuShu['xipai'][1][jiang][numXipai] or 0     
    elseif jConfig == 2 then
        local jiang1 = jiang[1]
        local jiang2 = jiang[2]
        if (jiang1 + jiang2) >= 0x80 then         --两个喜牌
            return HuShu['xipai'][2][0x80][numXipai] or 0     
        elseif (jiang1 | jiang2 ) >= 0x40 then    --有一个喜牌
            return HuShu['xipai'][2][0x40][numXipai] or 0     
        else
            return HuShu['xipai'][2]['default'][numXipai] or 0         
        end
    end
    
end

function HuShu.IsJiangShangPai(jiang, val)             --将上牌
    return ((jiang&0x0F) % 3) ==  ((val&0x0F) % 3)
end

function HuShu.GetLevel(jiang, val)
    local level = -1
    if jiang == val or   -- 1--val==jiang   2--千红白  3--0x01 4--0x9 5--将上牌
       ((jiang <= 0x29 and val<=0x29) and (jiang&0x0F == val&0x0F)) then    --1(9)条和1(9)万相等
        level = 1    
    elseif (val & 0x30) == 0x30 then level = 2
    elseif (val <= 0x29 and (val - (val&0xF0)== 0x01)) then level = 3
    elseif (val <= 0x29 and (val - (val&0xF0)== 0x09)) then level = 4
    elseif HuShu.IsJiangShangPai(jiang, val) then level = 5
    else level = 6
    end
    return level
end

function HuShu.GetHuShu1(scoreType, jiang, val)   --单将
    skynet.error(string.format('GetHuShu scoreType is %s, jiang is %0x, val is %0x', scoreType, jiang, val))
    local level = -1
    if jiang == val or   -- 1--val==jiang   2--千红白  3--0x01 4--0x9 5--将上牌
       ((jiang <= 0x29 and val<=0x29) and (jiang&0x0F == val&0x0F)) then    --1(9)条和1(9)万相等
        level = 1    
    elseif (val & 0x30) == 0x30 then level = 2
    elseif (val <= 0x29 and (val - (val&0xF0)== 0x01)) then level = 3
    elseif (val <= 0x29 and (val - (val&0xF0)== 0x09)) then level = 4
    elseif HuShu.IsJiangShangPai(jiang, val) then level = 5
    else level = 6
    end
    if val >= 0x40 then level = 0 end    --val为喜牌时，只有撂龙
    local j 
    if jiang >= 0x40 then j = 0x40 
    elseif jiang >= 0x30 then j = 0x30
    elseif (jiang & 0x0F) == 1 or (jiang & 0x0F) == 9 then j = (jiang & 0x0F)  --1 or 9
    else j = (jiang & 0x0F) % 3 end
    if j == 0 and (jiang & 0x0F) ~= 9 then j = 3 end     --3,6
    if j == 1 and (jiang & 0x0F) ~= 1 then j = 4 end     --4,7
    if not HuShu[scoreType] then return 0 end
    if not HuShu[scoreType][j] then return 0 end
    if not HuShu[scoreType][j][level] then return 0 end
    return  HuShu[scoreType][j][level]
end

function HuShu.GetHuShu2(scoreType, jiang, val)   --双将
    function IsJiangShangPai(val)
        return (val >= 0x30 or val == 0x19)
    end
    function comps(a,b) return a > b end
    table.sort(jiang, comps)   --jiang已排序，大者在前
    
    local jiang1 = jiang[1]
    local jiang2 = jiang[2]
    local bJiangShangPai = IsJiangShangPai(val)
    if not bJiangShangPai then
        return HuShu['shuangjiang'][false][scoreType]
    else
        if (jiang1 + jiang2) >= 0x80 then                --两个喜牌
            return HuShu['shuangjiang'][true][22][scoreType]
        elseif jiang1 >= 0x40 then     --有一个喜牌
            if IsJiangShangPai(jiang2) then   --有大将
                local ret = HuShu['shuangjiang'][true][21][scoreType]
                if jiang2 == 0x31 and val == 0x31 then
                    local teLi = {liao=64,dugang=48,minggang=8,peng=32}
                    return teLi[scoreType] or ret
                else
                    return ret
                end
            else
                local ret = HuShu['shuangjiang'][true][20][scoreType]
                if (jiang2 == 0x02) and ((val & 0x0F) == 0x02) then
                    local teLi = {}
                    if val == 0x02 then 
                        teLi = {liao=32,dugang=16,peng=16}
                    else
                        teLi = {liao=32,angang=24,minggang=16,peng=4,anke=8}
                    end
                    return teLi[scoreType] or ret
                else
                    return ret
                end
            end
        else    --没有喜牌
            if IsJiangShangPai(jiang1) and IsJiangShangPai(jiang2) then
                local ret = HuShu['shuangjiang'][true][11][scoreType]
                if (jiang1 == 0x31 and jiang2 == 0x32) or (jiang2 == 0x31 and jiang1 == 0x32) then  --千字+红花
                    local teLi = {peng=32,anke=96}
                    return teLi[scoreType] or ret
                else
                    return ret
                end
                
            elseif IsJiangShangPai(jiang1) or IsJiangShangPai(jiang2) then  --一个大将+普通
                local ret = HuShu['shuangjiang'][true][10][scoreType]
                if jiang1 == 0x31 and jiang2 == 0x02 then  --一个千字，1个2万
                    if val == 0x31 then
                        local teLi = {liao=64,dugang=32,peng=16}
                        return teLi[scoreType] or ret
                    elseif (val & 0x0F) == 0x02 then
                        if val == 0x02 then   --0x2
                            local teLi = {liao=32,dugang=24,peng=16}
                            return teLi[scoreType] or ret
                        else     --0x12 0x22
                            local teLi = {liao=32,angang=24,minggang=16,peng=4,anke=8}
                            return teLi[scoreType] or ret
                        end
                    end
                else
                    return ret
                end
            else   --都是普通
                local ret = HuShu['shuangjiang'][true][0][scoreType]
                if jiang1 == 0x02 and jiang2 == 0x02 then
                    if val == 0x02 then
                        local teLi = {liao=64}
                        return teLi[scoreType]
                    elseif (val & 0x0F) == 0x02 then
                        local teLi = {liao=64,minggang=32,peng=8,anke=48}
                        return teLi[scoreType] or ret
                    end
                elseif jiang1 == 0x02 or jiang2 == 0x02 then
                    if val == 0x02 then
                        local teLi = {liao=32,peng=16,anke=24}
                        return teLi[scoreType] or ret
                    elseif (val & 0x0F) == 0x02 then
                        local teLi = {liao=32,angang=24,minggang=16,peng=4,anke=8}
                        return teLi[scoreType] or ret
                    end
                    
                elseif jiang1 == 0x08 or jiang2 == 0x08 then
                    if val == 0x08 then
                        local teLi = {liao=32,peng=16,anke=24}
                        return teLi[scoreType] or ret
                    elseif (val & 0x0F) == 0x08 then
                        local teLi = {liao=32,angang=24,minggang=16,peng=4,anke=8}
                        return teLi[scoreType] or ret
                    end
                end
                return ret
            end
        end
    end
    
end

function HuShu.GetHuShuInHand1(hu, jiang, val, num)  
    local huInHand = {[true] = 8, [false] = 2}  --在没有胡牌得情况下 最后只按3张算底胡   将上8胡  非将上2胡 
    if not hu then 
        local bJiang = (HuShu.GetLevel(jiang, val) ~= 6)
        return huInHand[bJiang]
    else 
        if num == 3 then return HuShu.GetHuShu1('peng', jiang, val)
        elseif num == 4 then return HuShu.GetHuShu1('minggang', jiang, val)
        end
    end
end

function HuShu.GetHuShuInHand2(jiang, val)    
    return HuShu.GetHuShu2('anke', jiang, val)
end

return HuShu