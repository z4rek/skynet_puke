local skynet = require "skynet"
local TableLogic = require "tablelogic"

local GameScore = {}
local maxPlayer = 4
local EGameType = 
{
    CT_INVALID =                      0,         --错误类型
    CT_SINGLE =                       1,         --乌龙:单牌类型
    CT_ONE_DOUBLE =                   2,         --对子:只有一对
    CT_FIVE_TWO_DOUBLE =              3,         --二对:两对牌型
    CT_THREE =                        4,         --三条:三张牌型
    CT_FIVE_MIXED_FLUSH_NO_A =        5,         --顺子:没A杂顺
    CT_FIVE_MIXED_FLUSH_FIRST_A =     6,         --顺子:A在前顺子
    CT_FIVE_MIXED_FLUSH_BACK_A =      7,         --顺子:A在后顺子
    CT_FIVE_FLUSH =                   8,         --同花:同花
    CT_FIVE_THREE_DEOUBLE =           9,         --葫芦:三条一对
    CT_FIVE_FOUR_ONE =                10,        --铁支:四带一张
    CT_FIVE_STRAIGHT_FLUSH =          11,        --同花顺:没A同花顺
    CT_FIVE_STRAIGHT_FLUSH_FIRST_A =  12,        --同花顺:A在前同花顺
    CT_FIVE_STRAIGHT_FLUSH_BACK_A =   13,        --同花顺:A在后同花顺

    ST_SANSHUNZI =                    14,        --三顺子
    ST_SANTONGHUA =                   15,        --三同花
    ST_LIUDUIBAN =                    16,        --六对半
    ST_WUDUICHONGSAN =                17,        --五对冲三
    ST_SITAOCHONGSAN =                18,        --四套冲三
    ST_QUANHEI =                      20,        --全黑
    ST_QUANHONG =                     21,        --全红
    ST_QUANXIAO =                     22,        --全小
    ST_QUANDA =                       23,        --全大
    ST_YITIAOLONG =                   24,        --一条龙
    ST_SANTAOZHADAN =                 25,        --三炸弹
    ST_SHIERHUANGZU =                 26,        --十二皇族
    ST_SANTONGHUASHUN =               27,        --三同花顺
    ST_LITIAOHUALONG =                28,        --一条花龙
    ST_QINGLONG =                     29,        --一条青龙
}

function GameScore:CalculateScore(chairObj)
	local stScore = {}
	for i=0, maxPlayer-1 do
		stScore[i] = 0
	end

	--特殊牌型还是普通牌赋值
	for i=0, maxPlayer-1 do
		if chairObj[i]==nil then 
			break 
		end
		if chairObj[i].chOutCardData.cardgroup~=0  then 
			stScore[i] = self:CheckSTScore(chairObj[i].chOutCardData.cardgroup)
		end
	end

	--四个玩家特殊牌型分
	for i=0, maxPlayer-1 do
		if chairObj[i]==nil then 
			break 
		end
		for j=0, maxPlayer-1 do
			chairObj[i].chSTScore[j] = stScore[i] - stScore[j]
		end
	end

	--最后实际得分 
	for i=0, maxPlayer-1 do
		if chairObj[i] == nil then 
			return
		end
		--玩家i自己是否是特殊牌型
		if stScore[i] ~= 0 then 
			self:CalculateSTScore(i, chairObj, stScore)
		else
			self:CalculateCTScore(i, chairObj, stScore)
		end
	end
end

function GameScore:CalculateSTScore(i, chairObj, stScore)
	local tempValue = stScore[i]
	local tempScore = 0
	for j=0, maxPlayer-1 do
		if chairObj[j]==nil then 
			break
		end
		repeat
			if i==j then 
				break
			end
			if tempValue>stScore[j] then 
				tempScore = tempScore + tempValue
			elseif tempValue<stScore[j] then 
				tempScore = tempScore - stScore[j] 
			elseif tempValue==stScore[j] then 
				tempScore = tempScore
			end

		until true
	end

	chairObj[i].chOneJuGameScore = tempScore
end

function GameScore:CalculateCTScore(i, chairObj, stScore)
	
	skynet.error("###############Before CalculateCTScore###############")
	--[[
	skynet.error("---------------------------")
    for j=0, 6 do
        if chairObj[i].chOutCardData.head[i]~=nil then 
            skynet.error("chairObj["..i.."].chOutCardData.head["..j.."]= ",  chairObj[i].chOutCardData.head[j]%16)
        else
        	skynet.error("chairObj["..i.."].chOutCardData.head["..j.."]= ",  chairObj[i].chOutCardData.head[j])
        end
    end
    skynet.error("---------------------------")
    for j=0, 6 do
        if chairObj[i].chOutCardData.middle[i]~=nil then 
            skynet.error("chairObj["..i.."].chOutCardData.middle["..j.."]= ",  chairObj[i].chOutCardData.middle[j]%16)
        else
        	skynet.error("chairObj["..i.."].chOutCardData.middle["..j.."]= ",  chairObj[i].chOutCardData.middle[j])
        end 
    end
    skynet.error("---------------------------")
    for j=0, 6 do
        if chairObj[i].chOutCardData.tail[i]~=nil then 
            skynet.error("chairObj["..i.."].chOutCardData.tail["..j.."]= ",  chairObj[i].chOutCardData.tail[j]%16)
         else
         	skynet.error("chairObj["..i.."].chOutCardData.tail["..j.."]= ",  chairObj[i].chOutCardData.tail[j])
        end
    end
	--]]

	local tempScore = 0
	local tempCTScore = {}
	tempCTScore.headScore = 0
	tempCTScore.middleScore = 0
	tempCTScore.tailScore = 0

	for j=0, maxPlayer-1 do
		if chairObj[j]==nil then 
			break
		end
		repeat
			if i==j then 
				break
			end
			if stScore[j] ~= 0 then 
				tempScore = tempScore - stScore[j] 
			else
				--头道比
				local iRet = TableLogic:CompareTwoCards(chairObj[i].chOutCardData.head, chairObj[j].chOutCardData.head)
				skynet.error("head return iRet=", iRet)
				if iRet==1 then 
					tempCTScore.headScore = tempCTScore.headScore + self:CheckHeadScore(chairObj[i].chOutCardData.head[1]) 
				elseif iRet==2 then 
					tempCTScore.headScore = tempCTScore.headScore - self:CheckHeadScore(chairObj[j].chOutCardData.head[1])
				else
					tempCTScore.headScore = tempCTScore.headScore
				end
				--中道比
				iRet = TableLogic:CompareTwoCards(chairObj[i].chOutCardData.middle, chairObj[j].chOutCardData.middle)
				skynet.error("middle return iRet=", iRet)
				if iRet==1 then 
					tempCTScore.middleScore = tempCTScore.middleScore + self:CheckMiddleScore(chairObj[i].chOutCardData.middle[1]) 
				elseif iRet==2 then 
					tempCTScore.middleScore = tempCTScore.middleScore - self:CheckMiddleScore(chairObj[j].chOutCardData.middle[1])
				else
					tempCTScore.middleScore = tempCTScore.middleScore
				end
				--尾道比
				iRet = TableLogic:CompareTwoCards(chairObj[i].chOutCardData.tail, chairObj[j].chOutCardData.tail)
				skynet.error("tail return iRet=", iRet)
				if iRet==1 then 
					tempCTScore.tailScore = tempCTScore.tailScore + self:CheckTailScore(chairObj[i].chOutCardData.tail[1]) 
				elseif iRet==2 then 
					tempCTScore.tailScore = tempCTScore.tailScore - self:CheckTailScore(chairObj[j].chOutCardData.tail[1])
				else
					tempCTScore.tailScore = tempCTScore.tailScore
				end
			end
		until true
	end
	
	chairObj[i].chOutCardData.score[0] = tempCTScore.headScore
	chairObj[i].chOutCardData.score[1] = tempCTScore.middleScore
	chairObj[i].chOutCardData.score[2] = tempCTScore.tailScore
	chairObj[i].chOneJuGameScore = tempScore + tempCTScore.headScore + tempCTScore.middleScore + tempCTScore.tailScore
end

function GameScore:CheckHeadScore(cardType)
	--三条
	if cardType==EGameType.CT_THREE then 
		return 3
	--对子
	elseif cardType==EGameType.CT_ONE_DOUBLE then 
		return 1
	--杂牌
	elseif cardType==EGameType.CT_SINGLE then 
		return 1
	end

	skynet.error("CheckHeadScore return nil!")
end

function GameScore:CheckMiddleScore(cardType)
	--同花顺
	if cardType==EGameType.CT_FIVE_STRAIGHT_FLUSH then 
		return 10
	--铁支
	elseif cardType==EGameType.CT_FIVE_FOUR_ONE then 
		return 8
	--葫芦
	elseif cardType==EGameType.CT_FIVE_THREE_DEOUBLE then 
		return 2
	--同花
	elseif cardType==EGameType.CT_FIVE_FLUSH then 
		return 1
	--顺子
	elseif cardType==EGameType.CT_FIVE_MIXED_FLUSH_NO_A then 
		return 1
	--三条
	elseif cardType==EGameType.CT_THREE then 
		return 1
	--两对
	elseif cardType==EGameType.CT_FIVE_TWO_DOUBLE then 
		return 1
	--对子
	elseif cardType==EGameType.CT_ONE_DOUBLE then 
		return 1
	--杂牌
	elseif cardType==EGameType.CT_SINGLE then 
		return 1
	end

	skynet.error("CheckMiddleScore return nil!")
end

function GameScore:CheckTailScore(cardType)
	--同花顺
	if cardType==EGameType.CT_FIVE_STRAIGHT_FLUSH then 
		return 5
	--铁支
	elseif cardType==EGameType.CT_FIVE_FOUR_ONE then 
		return 4
	--葫芦
	elseif cardType==EGameType.CT_FIVE_THREE_DEOUBLE then 
		return 1
	--同花
	elseif cardType==EGameType.CT_FIVE_FLUSH then 
		return 1
	--顺子
	elseif cardType==EGameType.CT_FIVE_MIXED_FLUSH_NO_A then 
		return 1
	--三条
	elseif cardType==EGameType.CT_THREE then 
		return 1
	--两对
	elseif cardType==EGameType.CT_FIVE_TWO_DOUBLE then 
		return 1
	--对子
	elseif cardType==EGameType.CT_ONE_DOUBLE then 
		return 1
	--杂牌
	elseif cardType==EGameType.CT_SINGLE then 
		return 1
	end

	skynet.error("CheckTailScore return nil!")
end

function GameScore:CheckSTScore(cardType)
	--一条清龙
	if cardType==EGameType.ST_QINGLONG then 
		return 108
	--一条花龙
	elseif cardType==EGameType.ST_YITIAOLONG then 
		return 36
	--十二皇族
	elseif cardType==EGameType.ST_SHIERHUANGZU then 
		return 24
	--三同花顺
	elseif cardType==EGameType.ST_SANTONGHUASHUN then 
		return 22
	--三套炸弹
	elseif cardType==EGameType.ST_SANTAOZHADAN then 
		return 20
	--全大
	elseif cardType==EGameType.ST_QUANDA then 
		return 15
	--全小
	elseif cardType==EGameType.ST_QUANXIAO then 
		return 12
	--全红
	elseif cardType==EGameType.ST_QUANHONG then 
		return 10
	--全黑
	elseif cardType==EGameType.ST_QUANHEI then 
		return 10
	--四套冲3
	elseif cardType==EGameType.ST_SITAOCHONGSAN then 
		return 7
	--五对冲3
	elseif cardType==EGameType.ST_WUDUICHONGSAN then 
		return 6
	--六对半
	elseif cardType==EGameType.ST_LIUDUIBAN then 
		return 5
	--三同花
	elseif cardType==EGameType.ST_SANTONGHUA then 
		return 4
	--三顺子
	elseif cardType==EGameType.ST_SANSHUNZI then 
		return 3
	end

	skynet.error("CheckSTScore return nil!")
end

return GameScore