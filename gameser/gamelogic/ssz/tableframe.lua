local skynet = require "skynet"
local gameCmd = require "game_proto"
local TableLogic = require "tablelogic"
local GameScore = require "gamescore"
local Seat = require "Seat"
local GameLogic = require "GameLogic"
local Sraw = require "stable.raw"
Sraw.init()

local TableFrame = GameLogic:extend()
local MainCmdId = gameCmd.Main_ClientAndGameSvr_TableModle
local MAX_SSZ_CARDCOUNT = 13
local MAX_SHISANZHANG_PLAYER = 4
local Invalid_ChairId = 65535
local Invalid_Data = 255

local EGameStatus = 
{
    WAIT = 0,
    START = 1,
    DISCARD = 2,
    USEROUTCARD = 3,
    OVER = 4,
}

--------------------------------- 通用函数 Start ------------------------
function send_data(addr,msg)
    skynet.send(addr,"lua","send_data",msg)
end

function SendDataInLua(sendAddr,subId, msg)    
    local msgSend = string.pack('>I2>I2', MainCmdId, subId)
    if msg then msgSend = msgSend..msg end
    send_data(sendAddr, msgSend)
end

function TableFrame:CalcNextUsr()
    self.currentUser = (self.currentUser+1)%maxPlayer
end

function TableFrame:BroadCastData(subId, msg)    
    local msgSend = string.pack('>I2>I2', MainCmdId, subId)
    if msg then msgSend = msgSend..msg end
    for i,v in pairs (self.chairObj) do
        if v.player.nAgentAddr ~= 0 then 
            send_data(v.player.nAgentAddr, msgSend)
        end
    end
end

function TableFrame:GetChair(player)
    for k,v in pairs(self.chairObj) do
        if player.nAgentAddr == v.player.nAgentAddr then
            return v
        end
    end

    return nil
end

function TableFrame:GetSeatCount()
    skynet.error("GetSeatCount=", self.renShu)

    if self.renShu == 2 then
        return 2, {1, 2}
    elseif self.renShu == 3 then
        return 3, {1, 2, 3}
    else 
        return 4, {1, 2, 3, 4}
    end
end

function TableFrame:SetTableData(Room, nMaxPlayerCount, ndiFen, nMa, nFangPaoOrZiMo, nDianPaoMaOrZiMoMa, nKeQiangGang, externData)
    self.diFen = ndiFen
    self.renShu, self.paiShu = string.unpack('<I4<I4', externData)
    skynet.error("SetTableData, diFen, renShu, paiShu=", self.diFen, self.renShu, self.paiShu)

    self.maData = 0
    self.style = 1
    self.isAdvise = 2
    self.isDaQiang = 0
    self.wanFa = 0

    local msgRenShu = " 人数【"..self.renShu.."】"
    local msgJuShu = " 局数【"..self.room.m_wTotalJu.."】"
    local makeRoomsg = "十三太保"..msgRenShu..msgJuShu.." 一人支付"

    return makeRoomsg
end
--------------------------------- 通用函数 End ----------------------------


--------------------------------- 本文件局部变量汇总 Start ---------------------------
function TableFrame:initialize(room)
    TableFrame.meta.super.initialize(self,room)
    self:TableInit()
end

function TableFrame:TableInit()  
    self.chairId                =   0
    self.chairObj               =   {}
	self.bankerUser				=   nil		   --庄家
	self.currentUser			=   nil		   --当前用户
    self.leftCardCount          =   nil        --剩下的牌张数 
    self.gameStatus             =   nil        --加入游戏状态字段 各玩家只能在相应的状态下才能做对应的操作
    self.alreadyOutCount        =   nil        --已经打出的牌

    --玩法数据
    self.diFen                  =   nil        --底分
    self.renShu                 =   nil        --人数
    self.paiShu                 =   nil        --牌数
    self.maData                 =   nil        --马牌
    self.style                  =   nil        --比牌方式
    self.isAdvise               =   nil        --是否使用推荐牌型 
    self.isDaQiang              =   nil        --是否有打枪 
    self.wanFa                  =   nil        --0普通玩法  1南宁玩法
end

function TableFrame:ChairInit(chairID, player)
    self.chairObj[chairID] = {}
    self.chairObj[chairID].player = player                      --玩家
    self.chairObj[chairID].chairID = chairID                    --椅子ID

	self.chairObj[chairID].chCardData = nil						--椅子手牌
    self.chairObj[chairID].chAdViseData = nil                   --推荐牌型
    self.chairObj[chairID].chIsOutCard = nil                    --椅子是否出牌
    self.chairObj[chairID].chOutCardData = nil                  --出牌数据
    self.chairObj[chairID].chSTScore = nil                      --特殊牌型分数
    self.chairObj[chairID].chOneJuGameScore = nil               --每局分数
    self.chairObj[chairID].chTotalJuGameScore = 0               --总局分数
    self.chairObj[chairID].chTotalTongHuaShun = 0               --总局同花顺次数
    self.chairObj[chairID].chTotalSpecial = 0                   --总局特殊牌型次数
    self.chairObj[chairID].chTotalTieZhi = 0                    --总局铁支次数
    self.chairObj[chairID].chTotalChongSan = 0                  --总局冲三次数
end

function TableFrame:DataReset()
    --通用模板数据   
    --self.bankerUser             =   Invalid_ChairId     --庄家
    --self.diFen                  =   Invalid_Data        --底分
    --self.currentUser            =   Invalid_ChairId     --当前用户

    self.leftCardCount          =   Invalid_Data          --剩下的牌张数
    self.gameStatus             =   Invalid_Data
    self.alreadyOutCount        =   0

    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        if self.chairObj[i]	~= nil then
            self.chairObj[i].chCardData = {}						
            --推荐牌
            self.chairObj[i].chAdViseData = {}
            self.chairObj[i].chAdViseData.head = {}
            self.chairObj[i].chAdViseData.middle = {}
            self.chairObj[i].chAdViseData.tail = {}
            self.chairObj[i].chAdViseData.cardgroup = 0 
            self.chairObj[i].chAdViseData.score = {}
            for j=0, 7 do
                self.chairObj[i].chAdViseData.head[j] = nil
            end
            for j=0, 7 do
                self.chairObj[i].chAdViseData.middle[j] = nil
            end
            for j=0, 7 do
                self.chairObj[i].chAdViseData.tail[j] = nil
            end
            for j=0, 2 do
                self.chairObj[i].chAdViseData.score[j] = 0
            end
            --
            --打出来的牌
            self.chairObj[i].chOutCardData = {}
            self.chairObj[i].chOutCardData.head = {}
            self.chairObj[i].chOutCardData.middle = {}
            self.chairObj[i].chOutCardData.tail = {}
            self.chairObj[i].chOutCardData.cardgroup = 0 
            self.chairObj[i].chOutCardData.score = {}
            for j=0, 6 do
                self.chairObj[i].chOutCardData.head[j] = 0
            end
            for j=0, 6 do
                self.chairObj[i].chOutCardData.middle[j] = 0
            end
            for j=0, 6 do
                self.chairObj[i].chOutCardData.tail[j] = 0
            end
            for j=0, 6 do
                self.chairObj[i].chOutCardData.score[j] = 0
            end
            --特殊牌型分
            self.chairObj[i].chSTScore = {}
            for j=0, 3 do
                self.chairObj[i].chSTScore[j] = 0
            end 
            --
            self.chairObj[i].chOneJuGameScore = 0 
        end
    end
end
--------------------------------- 本文件局部变量汇总 End -----------------------------
function TableFrame:OnSitDownSucess(seat)
    self:ChairInit(self.chairId, seat.Player)

    --skynet.error("self.bankerUser=", self.bankerUser)
    if self.bankerUser == nil then
        self.bankerUser = self.chairId
        --skynet.error("pos!!! self.bankerUser=", self.bankerUser)
    end 

    self.chairId = self.chairId + 1

    --发送消息
    local msg = string.pack(">I2>I2<i4<i4", MainCmdId, gameCmd.Sub_GameSvrToClient_TableModle_SendTableInfo, self.room.m_nGameId, self.renShu)
    --再告诉客户端几人麻将
    send_data(seat.Player.nAgentAddr, msg)
end

function TableFrame:OnGameMsg(scmd, smsg, Player)

    if scmd == gameCmd.Sub_ClientToGameSvr_TableModle_OutCardReq then

        return self:OnNetMsgPukeOutCardReq(smsg, Player)

    elseif scmd == gameCmd.Sub_ClientToGameSvr_TableModle_ntDdzPress then

        return self:OnNetMsgPukePress(smsg, Player)

	end

end

--[[
游戏开始消息协议
//开场发送十三张扑克数据结构
struct GameShiSanZhangStartData
{
    WORD                            wCurJu;
    WORD                            wBankerUser;                                //庄家用户
    WORD                            wCurrentUser;                               //当前用户
    BYTE                            cbCardData[SHISANZHANG_COUNT];              //我的扑克列表
    WORD                            wSendCardNum;                               //推荐牌型个数
    ShiSanZhangCard                 cSendCard[2];                               //推荐牌型列表
    WORD                            wIsAdvise;                                  //1:自由摆牌, 2:推荐牌型
};

    BYTE head[7];//[0]:{0:非同花 1：方 2：梅 3：红 4：黑} [1]：{0：杂牌 1：对子 2：顺子 3：条子 4：同花顺} [2][3][4] 具体扑克
    BYTE middle[7];
    BYTE tail[7];
    BYTE cardgroup;//是否是特殊牌型
    int  m_score[3];//该牌型的得分
]]
function TableFrame:OnEventGameStart(Table)
    self.gameStatus = EGameStatus.START
    --组播游戏开始
    self:BroadCastData(gameCmd.Sub_GameSvrToClient_TableModle_GameStart)
    --复位
    self:DataReset()
    
    --MaData
    local MaData = string.pack('<B', self.maData)
    --skynet.error("gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_MaData=", gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_MaData)
    self:BroadCastData(gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_MaData, MaData)
    --StyleData
    local StyleData = string.pack('<i4', self.style)
    --skynet.error("gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_StyleData=", gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_StyleData)
    self:BroadCastData(gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_StyleData, StyleData)
    --wanFaData
    local wanFaData = string.pack('<i4', self.wanFa)
    --skynet.error("gameCmd.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang_WanFa=", gameCmd.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang_WanFa)
    self:BroadCastData(gameCmd.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang_WanFa, wanFaData)

    --random cards
    local cards = TableLogic:RandCards(self.paiShu)
    TableLogic:StartGameTestFaPai(cards, self.chairObj)  
    self.gameStatus = EGameStatus.DISCARD 

    --每人先发13张
    for i=0, self.renShu-1 do
        if self.chairObj[i] ~= nil then
            --复位
            self.chairObj[i].chIsOutCard = 0
            skynet.error("------------------------------------------------")
            for j=1, MAX_SSZ_CARDCOUNT do
				self.chairObj[i].chCardData[j] = cards[13*(i)+j]
                local vaule = self.chairObj[i].chCardData[j]
                skynet.error(vaule%16)
            end
        end
        --发完就计算推荐牌型
        TableLogic:CalculateCard(self.chairObj[i], self.paiShu)
        --将推荐牌型结果发送给当前玩家
        self:SendSszStartData(self.chairObj[i].player.nAgentAddr, i)
    end

    self.currentUser = self.bankerUser
    skynet.error("OnEventGameStart self.currentUser= "..self.currentUser.." self.bankerUser= "..self.bankerUser)
    self.gameStatus = EGameStatus.USEROUTCARD
    self.alreadyOutCount = 0
end

function TableFrame:OnNetMsgPukeOutCardReq(smsg, Player)
    local chair = self:GetChair(Player)
    if chair.chIsOutCard == 1 then 
        return
    end
    self.alreadyOutCount = self.alreadyOutCount + 1
     --动作复位
    self.room.ResetWorkCount() 
    self.currentUser = chair.chairID
    skynet.error("OnNetMsgPukeOutCardReq self.currentUser= "..self.currentUser.." self.bankerUser= "..self.bankerUser)

    --检测客户端发来的牌是否正确
    local has_head = 0
    local has_middle = 0
    local has_tail = 0
    local s = string.unpack("c34", smsg,  5)
    for i=0, 6 do
        chair.chOutCardData.head[i] = string.byte(s, i+1)
        if self:CheckHasCard(chair.chOutCardData.head[i], chair.chCardData)==true then
            has_head = has_head + 1
        end 
        --skynet.error("chair.chOutcardData.head[i]", chair.chOutCardData.head[i])
    end
    for i=0, 6 do
        chair.chOutCardData.middle[i] = string.byte(s, i+8)
        if self:CheckHasCard(chair.chOutCardData.middle[i], chair.chCardData)==true then
            has_middle = has_middle + 1
        end 
        --skynet.error("chair.chOutCardData.middle[i]", chair.chOutCardData.middle[i])
    end
    for i=0, 6 do
        chair.chOutCardData.tail[i] = string.byte(s, i+15)
        if self:CheckHasCard(chair.chOutCardData.tail[i], chair.chCardData)==true then
            has_tail = has_tail + 1
        end 
        --skynet.error("chair.chOutCardData.tail[i]", chair.chOutCardData.tail[i])
    end
    chair.chOutCardData.cardgroup = string.byte(s, 22)
 
    --出错就打印下手牌并并return
    if has_head~=3 or has_middle~=5 or has_tail~=5 then 
        --skynet.error("head=", has_head)
        --skynet.error("middle=", has_middle)
        --skynet.error("tail=", has_tail)
        local dataBuf = ''
        dataBuf = dataBuf..string.pack("<i4i4", chair.chairID, 0)
        SendDataInLua(chair.player.nAgentAddr, gameCmd.Sub_GameSvrToClient_TableModle_UserOutCardResult, dataBuf)
        return
    end

    --没出错就进入
    local dataBuf = ''
    dataBuf = dataBuf..string.pack("<i4i4", chair.chairID, 1)
    chair.chIsOutCard = 1
    self:BroadCastData(gameCmd.Sub_GameSvrToClient_TableModle_UserOutCardResult, dataBuf)
    --检测是否全部已经出牌
    for i=0, self.renShu-1 do
        if self.chairObj[i].chIsOutCard ~= 1 then 
            return
        end
    end

    --没返回则开始比牌流程了
    self.gameStatus = EGameStatus.OVER
    skynet.error(self.alreadyOutCount.."位玩家全部已经确认出牌，开始比牌")

    for i=0, self.renShu-1 do
        --检测打出的牌头中尾道牌型与分值
        TableLogic:CalculateOutCard(self.chairObj[i].chOutCardData)
        TableLogic:SetSendTotalMsg(self.chairObj[i])
    end 

    --检测分数
    GameScore:CalculateScore(self.chairObj)

    --发送出牌消息
    self:SendCompareResult()
    
    --变量
    self.gameStatus = EGameStatus.WAIT
    self.currentUser = Invalid_ChairId

    --本轮结束
    --把分设给玩家
    for i=0, self.renShu-1 do
        self.chairObj[i].chTotalJuGameScore = self.chairObj[i].chTotalJuGameScore + self.chairObj[i].chOneJuGameScore
        self.chairObj[i].player.nScore = self.chairObj[i].chTotalJuGameScore
    end
    self:GameOver()
end

function TableFrame:SendCompareResult(player)
    local compareResult = {}
    local dataBuf = ''
    dataBuf = dataBuf..string.pack("<I2I2I2", self.room.m_wCurJu, self.bankerUser, self.renShu)
    --C++   struct [ShiSanZhangCard]
    --由于协议照搬C++的为了兼容已有客户端，所以这样制定通讯结构
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        if self.chairObj[i] == nil then
            for j=0, 6 do
                dataBuf = dataBuf..string.pack("<B", 0)
            end
            for j=0, 6 do
                dataBuf = dataBuf..string.pack("<B", 0)
            end
            for j=0, 6 do
                dataBuf = dataBuf..string.pack("<B", 0)
            end
            dataBuf = dataBuf..string.pack("<B", 0)
            for j=0, 2 do
                dataBuf = dataBuf..string.pack("<i4", 0)
            end
        else
            for j=0, 6 do
                dataBuf = dataBuf..string.pack("<B", self.chairObj[i].chOutCardData.head[j])
            end
            for j=0, 6 do
                dataBuf = dataBuf..string.pack("<B", self.chairObj[i].chOutCardData.middle[j])
            end
            for j=0, 6 do
                dataBuf = dataBuf..string.pack("<B", self.chairObj[i].chOutCardData.tail[j])
            end
            dataBuf = dataBuf..string.pack("<B", self.chairObj[i].chOutCardData.cardgroup)
            for j=0, 2 do
                --skynet.error("score= ", self.chairObj[i].chOutCardData.score[j])
                dataBuf = dataBuf..string.pack("<i4", self.chairObj[i].chOutCardData.score[j])
            end
        end
    end
    --nSTScore 特殊牌型分
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        if self.chairObj[i] == nil then
            for j=0, 3 do
                dataBuf = dataBuf..string.pack("<i4", 0)
            end
        else 
            for j=0, 3 do
                skynet.error("self.chairObj[i].chSTScore[j]=" ,self.chairObj[i].chSTScore[j])
                dataBuf = dataBuf..string.pack("<i4", self.chairObj[i].chSTScore[j])
            end
        end
    end
    --打枪list，虽然豆花麻将没有了，但是同上兼容C++版客户端收发结构必须加上
    for i=0, 5 do
        dataBuf = dataBuf..string.pack("<i4i4i4", 0, 0, 0)
    end
    --经过本次比牌四人的确切分数
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        if self.chairObj[i] == nil then 
            dataBuf = dataBuf..string.pack("<i4", 0)
        else
            dataBuf = dataBuf..string.pack("<i4", self.chairObj[i].chOneJuGameScore)
        end
    end

    if player==nil then 
        self:BroadCastData(gameCmd.Sub_GameSvrToClient_TableModle_UserCompareShiSanZhangResult, dataBuf)
    else
        SendDataInLua(player, gameCmd.Sub_GameSvrToClient_TableModle_UserCompareShiSanZhangResult, dataBuf)
    end
end

function TableFrame:ConcludeGame() 
    self.room.ConcludeGameFriend()
end

--服务器检测客户端发来的手牌服务器上有没有
function TableFrame:CheckHasCard(card, chairCards)
    for key, vaule in ipairs(chairCards) do
        if vaule == card then 
            return true
        end
    end

    return false
end

--[[
//断线重连协议
struct DdzOfflineBack
{
    int         nBei;                                           //倍数    
    int         nZhaDanCount;                                   //炸弹个数
    //int         gameType;                                     //游戏类型 1叫地主 3抢 5打牌
    WORD        wBankUser;                                      //地主ID
    WORD        outCardUsr;                                     //出牌玩家
    WORD        currer;                                         //下一个玩家
    BYTE        weiPai[3];                                      //叫牌数据
    BYTE        cbCards[MAX_DDZ_CARDCOUNT];                     //断线玩家手牌
    int         gameScore[DDZ_GAME_PLAYER];                     //三玩家游戏积分
    int         cbCardsCount[DDZ_GAME_PLAYER];                  //三家手牌的张数
    BYTE        cbCard[DDZ_GAME_PLAYER][MAX_DDZ_OUTCOUNT];      //三家打牌
}; 
]]
function TableFrame:OnPlayerOfflineBack(seat, chair)
    local  chair = self:GetChair(seat.Player)
    local dataBuf = ''
    dataBuf = dataBuf..string.pack('<i4', self.wanFa)
    SendDataInLua(seat.Player.nAgentAddr, gameCmd.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang_WanFa, dataBuf)
    skynet.error("断线重连")

    local alreadyCount = 0
    for i=0, self.renShu-1 do
        if self.chairObj[i].chIsOutCard == 1 then 
            alreadyCount = alreadyCount + 1
        end
    end

    --所有人都点出牌，发比牌结果
    if alreadyCount==self.renShu then 
        skynet.error("所有人都已出牌")
        dataBuf = ''
        dataBuf = dataBuf..string.pack('<B', self.maData, self.maData)
        SendDataInLua(seat.Player.nAgentAddr, gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_MaData, dataBuf)
        dataBuf = ''
        dataBuf = dataBuf..string.pack('<B', self.maData, self.style)
        SendDataInLua(seat.Player.nAgentAddr, gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_StyleData, dataBuf)
        --TraceLn(0,"[断线重连] "<<" 桌子ID="<<m_pITable->GetTableID()<<" 当前第"<<m_pITable->GetCurJu()<<"局"<<"用户id： "<<pPlayer->GetPlayerId()<<" 发送比牌结果");
        self:SendCompareResult(seat.Player.nAgentAddr)
        self:SendOffLineBackStat(seat.Player.nAgentAddr, 2)
    --还有人没出牌
    elseif chair.chIsOutCard==1 then 
        skynet.error("还有人没出牌")
        dataBuf = ''
        dataBuf = dataBuf..string.pack('<B', self.maData, self.maData)
        SendDataInLua(seat.Player.nAgentAddr, gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_MaData, dataBuf)
        dataBuf = ''
        dataBuf = dataBuf..string.pack('<B', self.maData, self.style)
        SendDataInLua(seat.Player.nAgentAddr, gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_StyleData, dataBuf)
        self:SendOffLineBackStat(seat.Player.nAgentAddr, 1)
    --当前用户还没出牌
    elseif chair.chIsOutCard==0 then
        skynet.error("当前用户还没出牌")
        dataBuf = ''
        dataBuf = dataBuf..string.pack('<B', self.maData, self.maData)
        SendDataInLua(seat.Player.nAgentAddr, gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_MaData, dataBuf)
        dataBuf = ''
        dataBuf = dataBuf..string.pack('<B', self.maData, self.style)
        SendDataInLua(seat.Player.nAgentAddr, gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_StyleData, dataBuf)
        self:SendSszStartData(chair.player.nAgentAddr, chair.chairID)
        self:SendOffLineBackStat(seat.Player.nAgentAddr, 0)
    end
end

function TableFrame:SendOffLineBackStat(player, status)
    dataBuf = ''
    dataBuf = dataBuf..string.pack('<i4', status)
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        if self.chairObj[i] == nil then 
            dataBuf = dataBuf..string.pack('<i4i4', 0, 0)
        else
            dataBuf = dataBuf..string.pack('<i4i4', self.chairObj[i].chairID, self.chairObj[i].chIsOutCard)
        end
    end
    SendDataInLua(player, gameCmd.Sub_GameSvrToClient_TableModle_PuKeOffBackStat, dataBuf)
end
    
function TableFrame:SendSszStartData(player, chairID)
    local dataBuf = ''
    dataBuf = dataBuf..string.pack('<I2I2I2', self.room.m_wCurJu, self.bankerUser, chairID)
    for j=1, MAX_SSZ_CARDCOUNT do 
        dataBuf = dataBuf..string.pack('<B', self.chairObj[chairID].chCardData[j])
    end
    dataBuf = dataBuf..string.pack('<I2', 1)

    for j=0, 6 do
        --skynet.error("self.chairObj[i].chAdViseData.head[i]= ", self.chairObj[chairID].chAdViseData.head[j])
        dataBuf = dataBuf..string.pack('<B', self.chairObj[chairID].chAdViseData.head[j])
    end
    for j=0, 6 do
        dataBuf = dataBuf..string.pack('<B', self.chairObj[chairID].chAdViseData.middle[j])
    end
    for j=0, 6 do
        dataBuf = dataBuf..string.pack('<B', self.chairObj[chairID].chAdViseData.tail[j])
    end
    dataBuf = dataBuf..string.pack('<I2', self.chairObj[chairID].chAdViseData.cardgroup)
    for j=0, 2 do
        dataBuf = dataBuf..string.pack('<i4', 3)
    end

    skynet.error("self.chairObj[chairID].chAdViseData.tail[1]=", self.chairObj[chairID].chAdViseData.tail[1])
     
    dataBuf = dataBuf..string.pack('<I2', self.isAdvise)
    SendDataInLua(player, gameCmd.Sub_GameSvrToClient_TableModle_Game_ShiSanZhang_StartData, dataBuf)
end

--[[
    int     nGameScoreTotal[GAME_SHISANZHANG_PLAYER]; 
    int     nDaQiangTotal[GAME_SHISANZHANG_PLAYER];
    int     nBeiDaQiangTotal[GAME_SHISANZHANG_PLAYER];
    int     nTongHuaShunTotal[GAME_SHISANZHANG_PLAYER];
    int     nSpecialPaiTotal[GAME_SHISANZHANG_PLAYER];
    int     nQuanLeiDaTotal[GAME_SHISANZHANG_PLAYER];
    char    szEndTime[STR_LEN_32];
    int     nTieZhi[GAME_SHISANZHANG_PLAYER];
    int     nChongSan[GAME_SHISANZHANG_PLAYER];
]]
function TableFrame:ConcluedGameLastRound()

    skynet.error("ConcluedGameLastRound")

    local isLastJu = 0
    
    local firstBuf = ''
    self:BroadCastData(gameCmd.Sub_GameSvrToClient_TableModle_8JuGameOver, firstBuf)
  
    local dataBuf = ''
    --totalGameScore
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        if self.chairObj[i] == nil then 
            dataBuf = dataBuf..string.pack('<i4', 0)
        else
            dataBuf = dataBuf..string.pack('<i4', self.chairObj[i].chTotalJuGameScore)
        end
    end
    --nDaQiangTotal
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        dataBuf = dataBuf..string.pack('<i4', 0)
    end
    --nBeiDaQiangTotal
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        dataBuf = dataBuf..string.pack('<i4', 0)
    end
    --nTongHuaShunTotal
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        if self.chairObj[i] == nil then 
            dataBuf = dataBuf..string.pack('<i4', 0)
        else
            dataBuf = dataBuf..string.pack('<i4', self.chairObj[i].chTotalTongHuaShun)
        end
    end
    --nSpecialPaiTotal
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        if self.chairObj[i] == nil then 
            dataBuf = dataBuf..string.pack('<i4', 0)
        else
            dataBuf = dataBuf..string.pack('<i4', self.chairObj[i].chTotalSpecial)
        end
    end
    --nQuanLeiDaTotal
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        dataBuf = dataBuf..string.pack('<i4', 0)
    end
    --time
    dataBuf = dataBuf..string.pack('<c32',  tostring(os.date("%Y-%m-%d %H:%M:%S",os.time())) )

    ---[[没用到
    --nTieZhi
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        dataBuf = dataBuf..string.pack('<i4', 0)
    end
    --nChongSan
    for i=0, MAX_SHISANZHANG_PLAYER-1 do
        dataBuf = dataBuf..string.pack('<i4', 0)
    end
    --]]
    skynet.error("gameCmd.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang8JuOver!", gameCmd.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang8JuOver)
    self:BroadCastData(gameCmd.Sub_GameSvrToClient_TableModle_PuKeShiSanZhang8JuOver, dataBuf)

end 

function TableFrame:IsPlaying()
    return self.currentUser~=Invalid_ChairId 
end 

function TableFrame:UserComeBack(seat)
    
end



return TableFrame