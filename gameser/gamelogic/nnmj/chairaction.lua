local CMD = require "game_proto"
local skynet = require "skynet"

local CONST = {MAX_INDEX=34}
local CHAIRCONST = {MAX_ACTION_SELECTTION=6, MAX_TINGNUM=13}
WIKTYPE = {WIK_LINE=0x01,WIK_PENG=0x02,WIK_JIAGANG=0x04,WIK_ANGANG=0x08,WIK_JIEGANG=0x10, WIK_HU=0x20}

local FANWAIFAN = {QINGYISE=0x0001,HAIDIHU=0x0002,HAIDIPAO=0x0004,GANGHUA=0x0008,GANGPAO=0x0010,QUANQIUREN=0x0020,QIANGGANGHU=0x0040,TIANHU=0x0080,DIHU=0x0100,MENQINIG=0x0200,QUANQIURENDIANPAO=0x0400,SHISANYAO=0x0800,ZIYISE=0x1000}
local HUTYPE = {HT_NULL=0, HT_PINGHU=1, HT_PENGPENGHU=0x2, HT_QIXIAODUI=0x4, HT_QIDADUI=0x8, HT_HAOQI=0xF, HT_CHAOHAOQI=0xF0, HT_SHISANYAO=0x10}

local TABLECONST = {FULL_PLAYER=4, MAX_COUNT=14,MAX_TING=13, MAX_MA=10, MAXWEAVCOUNT=4, MAXOUTCARDCOUNT=39}

ChairAction = {actType={WIKTYPE.WIK_LINE,WIKTYPE.WIK_PENG,WIKTYPE.WIK_ANGANG,WIKTYPE.WIK_JIEGANG,WIKTYPE.WIK_JIAGANG,WIKTYPE.WIK_HU}}

local UserActions = {}

BufTool = {}
function BufTool.WriteOffset(len)
    local str = ''
    for i = 1, len do
        local usrAct = string.char(0) 
        str = str..usrAct
    end
    return str
end

local MainCmdId = CMD.Main_ClientAndGameSvr_TableModle

function UserActions:New(obj)
	obj = obj or {}
	setmetatable(obj, self)
	self.__index = self
	return obj
end

function UserActions:Reset()
	for i = 1, #self do
		self[i] = nil
	end
end

function UserActions:GetPackString()
    local buf = ''
	local num = #self
    buf = buf..string.pack('B', num)
	local hasHU = false
	for i = 1, num do
		local act = self[i]
        local oneTypeNum = 0
        if act.Type == WIKTYPE.WIK_LINE then 
            oneTypeNum = #act.tabChi    --多少种吃法
        end
		buf = buf..string.pack('BBB', act.Type, act.CardData, oneTypeNum)
        for i = 1, oneTypeNum do 
            buf = buf..string.pack('BB', act.tabChi[i].b, act.tabChi[i].a)   --chi
        end
        buf = buf..BufTool.WriteOffset((3 - oneTypeNum) * 2)   --chi
        buf = buf..BufTool.WriteOffset(32)  
		if act.Type == WIKTYPE.WIK_HU then hasHU = true end
	end
    buf = buf..BufTool.WriteOffset((CHAIRCONST.MAX_ACTION_SELECTTION - num) * 41)    --41是OneActionData的大小
    buf = buf..BufTool.WriteOffset(32)  
    return buf
end

function ChairAction:New(obj)
	obj = obj or {}
	setmetatable(obj, self)
	self.__index = self
	return obj
end

function ChairAction:ClearChair()
    self.tabCbCardIndex = {}
    self.tabCbOutCard = {}
    self.tabWeaveItem = {}
    self.tabUserAction = {}
    self.tabTingCardData={}
    self.tabToDo = {}
    UserActions:Reset()
    self:ResetLou()
end

function ChairAction:MakeReadyChair(tabFrame) 
    self:ClearChair()
    self:Init()
    self.tabFrame = tabFrame
end

function ChairAction:Quit()
    self:ClearChair()
    self.addr = nil
end

function ChairAction:RegisterFun()
    self.DoMsgList = {}
    self.DoMsgList[WIKTYPE.WIK_LINE]    =  {Type = WIKTYPE.WIK_LINE,    Func = self.DoChi}
    self.DoMsgList[WIKTYPE.WIK_PENG]    =  {Type = WIKTYPE.WIK_PENG,    Func = self.DoPeng}
    self.DoMsgList[WIKTYPE.WIK_ANGANG]  =  {Type = WIKTYPE.WIK_ANGANG,  Func = self.DoAnGang}
    self.DoMsgList[WIKTYPE.WIK_JIEGANG] =  {Type = WIKTYPE.WIK_JIEGANG, Func = self.DoJieGang}
    self.DoMsgList[WIKTYPE.WIK_JIAGANG] =  {Type = WIKTYPE.WIK_JIAGANG, Func = self.DoJiaGang}
    self.DoMsgList[WIKTYPE.WIK_HU]      =  {Type = WIKTYPE.WIK_HU,      Func = self.DoHu}
    
    self.CANCELCMD = {}
    self.CANCELCMD[WIKTYPE.WIK_LINE]    = CMD.Sub_GameSvrToClient_TableModle_UserCancelChiOk
    self.CANCELCMD[WIKTYPE.WIK_PENG]    = CMD.Sub_GameSvrToClient_TableModle_UserCancelPengOk
    self.CANCELCMD[WIKTYPE.WIK_JIEGANG] = CMD.Sub_GameSvrToClient_TableModle_UserCancelPengOk
    self.CANCELCMD[WIKTYPE.WIK_JIAGANG] = CMD.Sub_GameSvrToClient_TableModle_UserCancelJiaGangOk
    self.CANCELCMD[WIKTYPE.WIK_ANGANG]  = CMD.Sub_GameSvrToClient_TableModle_UserCancelAnGangOk
    self.CANCELCMD[WIKTYPE.WIK_HU]      = CMD.Sub_GameSvrToClient_TableModle_UserCancelHu
end

function ChairAction:Init()
    self:RegisterFun()
	for i = 1, CONST.MAX_INDEX  do 
		self.tabCbCardIndex[i] = 0
	end
    self.tabUserAction = UserActions:New()
	self.tabCardVal = {}
    
    if self.UserChairID == 0xFF then 
        self.tabCardVal = {0x17,0x04,0x18,0x04,0x04,0x08,0x08,0x23,0x22,0x24,0x25,0x26,0x27, 0}
        for i = 1, 13 do
            local index = MjLogic:ValToIndex(self.tabCardVal[i])
            self.tabCbCardIndex[index] = self.tabCbCardIndex[index] + 1
        end
    elseif self.UserChairID == 0xFF then 
        self.tabCardVal = {0x04,0x12,0x03,0x16,0x19,0x17,0x17,0x17,0x33,0x24,0x25,0x35,0x35, 0}
        for i = 1, 13 do
            local index = MjLogic:ValToIndex(self.tabCardVal[i])
            self.tabCbCardIndex[index] = self.tabCbCardIndex[index] + 1
        end
    end
end

function ChairAction:SendData(msg)
    send_data(self.addr, msg)
end

function ChairAction:SendDataInLua(subId, msg)
    local msgSend = string.pack('>I2>I2', MainCmdId, subId)
    if msg then msgSend = msgSend..msg end
    send_data(self.addr, msgSend)
end

function ChairAction:StartFaPai(wJu)
    local gameStartDataMsg = string.pack("<I2<I2<I2", wJu, self.tabFrame.Banker, self.tabFrame.Banker)
    gameStartDataMsg = gameStartDataMsg..self.tabUserAction:GetPackString()
    gameStartDataMsg = gameStartDataMsg..string.pack('b', 83)
    for i = 1, TABLECONST.MAX_COUNT do 
        local v = self.tabCardVal[i]
        if v then
            gameStartDataMsg = gameStartDataMsg..string.pack('b', v)
        else
            gameStartDataMsg = gameStartDataMsg..string.pack('b', 0)
        end
    end
    self:SendDataInLua(CMD.Sub_GameSvrToClient_TableModle_GameStartData, gameStartDataMsg)
end

function ChairAction:OnDispatchCard(cbCardData, nLeftCardCount)   --椅子摸牌
	local ind = MjLogic:ValToIndex(cbCardData)
	self.tabCbCardIndex[ind] = self.tabCbCardIndex[ind] + 1
	self:ResetLou()
	self:CanGang(cbCardData, WIKTYPE.WIK_ANGANG)
    self:CanGang(cbCardData, WIKTYPE.WIK_JIAGANG)
    self:CanHu(cbCardData)
	
    local dispatchMsg = string.pack(">I2>I2", MainCmdId, CMD.Sub_GameSvrToClient_TableModle_DispatchCardToMe)
    dispatchMsg = dispatchMsg..string.pack("I2B", self.UserChairID, cbCardData)
    dispatchMsg = dispatchMsg..self.tabUserAction:GetPackString()
    dispatchMsg = dispatchMsg..string.pack("I2", nLeftCardCount)
    
    self:SendData(dispatchMsg)
end

function ChairAction:OnOutCard(cbCardData)        --椅子出牌
	local ind = MjLogic:ValToIndex(cbCardData)
	self.tabCbCardIndex[ind] = self.tabCbCardIndex[ind] - 1
	self.tabCbOutCard[#self.tabCbOutCard + 1] = cbCardData
end


function ChairAction:OthersCardToMe(wOtherUserId, nLeftCardCount)
	local msg = string.pack('>I2>I2', MainCmdId, CMD.Sub_GameSvrToClient_TableModle_DispatchCardToOther)
    msg = msg..string.pack('I2B', wOtherUserId, nLeftCardCount)
	self:SendData(msg)
end

function ChairAction:UpdataTingData()
	self.tabTingCardData = MjLogic:IsTing(self.tabCbCardIndex, self.tabWeaveItem)
	local tingLen = #self.tabTingCardData.val
    local msg = string.pack('>I2>I2<B', MainCmdId, CMD.Sub_GameSvrToClient_TableModle_UpdateTingCardData, tingLen)
	if tingLen > 0 then
		for i = 1, #self.tabTingCardData.val do
            local v = string.pack('B', self.tabTingCardData.val[i])
            msg = msg..v
		end
        for i = 1, CHAIRCONST.MAX_TINGNUM - tingLen do 
            local v = string.pack('B', 0)
            msg = msg..v
        end
		self:SendData(msg)
	else
        for i = 1, CHAIRCONST.MAX_TINGNUM + 1 do 
            local v = string.pack('B', 0)
            msg = msg..v
        end    
		self:SendData(msg)
	end
end

function ChairAction:IsBeiFengHu()
    for chair in self.tabFrame:ChairIterator() do
        if chair.UserChairID ~= self.UserChairID then 
            if #chair.tabWeaveItem >= 3 then
                local nPG = 0
                for _, v in pairs(chair.tabWeaveItem) do
                    if (v.Kind == WIKTYPE.WIK_JIEGANG or v.Kind == WIKTYPE.WIK_PENG) and v.Provider == self.UserChairID then 
                        nPG = nPG + 1
                    end
                end
                if nPG >= 3 then return true end
            end
        end
    end
	return false
end

function ChairAction:IsLouPeng(getData)
    if self.LouPeng then 
        if self.LouPeng[getData] then skynet.error(' Chair Id '..self.UserChairID..' LouPeng :'..getData) end
        return self.LouPeng[getData]
    end
    return false
end

function ChairAction:IsLouHu()
    if self.LouHu then skynet.error(' Chair Id '..self.UserChairID..' LouHu') end
	return self.LouHu or false
end

function ChairAction:CheckHu(getData)
	local cardIndex = DeepCopy(self.tabCbCardIndex)
	local bZiMo = true
	if MjLogic:GetHandCardNum(self.tabCbCardIndex) % 3 == 1 then 
		local index = MjLogic:ValToIndex(getData)
		cardIndex[index] = cardIndex[index] + 1
		bZiMo = false
	end 
	if #self.tabTingCardData == 0 or #self.tabTingCardData.val == 0 then return false end
	local bHu = false
	local type = 0
	for i = 1, #self.tabTingCardData.val do
		if self.tabTingCardData.val[i] == getData then
			bHu = true 
			type = self.tabTingCardData.type[i]
			break
		end 
	end 
	if not bHu then return false
	else return true, type, MjLogic:GetFanWaiFan(cardIndex, self.tabWeaveItem) end
end

function ChairAction:CanHu(cbCardData, provider)
	if self:IsBeiFengHu() or self:IsLouHu() then return false end
    local bHu, type = self:CheckHu(cbCardData)
	if bHu then 
        if self.tabFrame.PingHuZiMo and type == HUTYPE.HT_PINGHU and provider then return false end   --平胡需自摸
		local tabAction = {}
		tabAction.Type = WIKTYPE.WIK_HU
		tabAction.CardData = cbCardData
        tabAction.Provider = provider
		self.tabUserAction[#self.tabUserAction + 1] = tabAction
		return true
	end
	return false
end

function ChairAction:CanGang(getData, type, Provider)       --加了该张牌,可能有多个暗杠的牌
	if type == WIKTYPE.WIK_ANGANG then 
		local gang = {}
		for i = 1, CONST.MAX_INDEX do 
			if self.tabCbCardIndex[i] > 3 then 
				local val = MjLogic:IndexToVal(i)
				local tabAction = {}
				tabAction.CardData = val
				tabAction.Type = WIKTYPE.WIK_ANGANG
				self.tabUserAction[#self.tabUserAction + 1] = tabAction
				gang[#gang + 1] = val
			end
		end
		return #gang > 0, gang
	elseif type == WIKTYPE.WIK_JIEGANG then
		local index = MjLogic:ValToIndex(getData)
		local num = self.tabCbCardIndex[index]
		local tabAction = {}
		tabAction.Type = type
		tabAction.CardData = getData
        tabAction.Provider = Provider
		local bJieGang = false
		if num == 3 then 
			self.tabUserAction[#self.tabUserAction + 1] = tabAction
			bJieGang = true
		end
		return bJieGang
    elseif type == WIKTYPE.WIK_JIAGANG then 
        for i = 1, #self.tabWeaveItem do
            local weaveItem = self.tabWeaveItem[i]
            if weaveItem.Kind == WIKTYPE.WIK_PENG and weaveItem.CenterCard == getData then 
                local tabAction = {}
                weaveItem.Kind = type
                local tabAction = {}
                tabAction.Type = type
                tabAction.CardData = getData
                tabAction.Provider = weaveItem.Provider     --加杠的提供者是碰的那个
                self.tabUserAction[#self.tabUserAction + 1] = tabAction
            end
        end
	end
end

function ChairAction:CanPeng(getData, provider)       --还没有加该张牌
    if self:IsLouPeng(getData) then return false end         --漏碰
	local index = MjLogic:ValToIndex(getData)
	local num = self.tabCbCardIndex[index]
	local tabAction = {}
	tabAction.CardData = getData
    tabAction.Provider = provider
	if num >= 2 then 
		tabAction.Type = WIKTYPE.WIK_PENG
		self.tabUserAction[#self.tabUserAction + 1] = tabAction
		return true
	end
	return false
end

function ChairAction:CanChi(getData, provider)       --还没有加该张牌
    if not self.tabFrame.CanChi then return false end
    if getData&0xF0 == 0x30 then return false end    --风
	local index = MjLogic:ValToIndex(getData)
    local tabChi = {}
    if (getData&0x0F > 2) and self.tabCbCardIndex[index - 2] > 0 and self.tabCbCardIndex[index - 1] > 0 then
       local chi = {b = getData - 2, a = getData - 1}
       tabChi[#tabChi + 1] = chi
    end
    
    if (getData&0x0F > 1 and getData&0x0F < 9) and self.tabCbCardIndex[index - 1] > 0 and self.tabCbCardIndex[index + 1] > 0 then
       local chi = {b = getData - 1, a = getData + 1}
       tabChi[#tabChi + 1] = chi
    end
    
    if (getData&0x0F < 8) and self.tabCbCardIndex[index + 1] > 0 and self.tabCbCardIndex[index + 2] > 0 then
        local chi = {b = getData + 1, a = getData + 2}
        tabChi[#tabChi + 1] = chi
    end
    
	if #tabChi > 0 then 
        local tabAction = {}
        tabAction.CardData = getData
        tabAction.Provider = provider
		tabAction.Type = WIKTYPE.WIK_LINE
        tabAction.tabChi = tabChi
		self.tabUserAction[#self.tabUserAction + 1] = tabAction
		return true
	end
	return false
end

function ChairAction:SendOperateHint()	
    local msg = self.tabUserAction:GetPackString()
	local protol = CMD.Sub_GameSvrToClient_TableModle_CanPengOrJieGang
	if hasHU then protol = CMD.Sub_GameSvrToClient_TableModle_CanDianPaoHu end
	self:SendDataInLua(protol, msg)
end

function ChairAction:DoHu(getCardData)
	local bHu, huType, fan = false, 0, 0
    local provider = -1

    bHu, huType, fan = self:CheckHu(getCardData)
    local tabAction = self:CouldOperate(WIKTYPE.WIK_HU, getCardData)
    if bHu then provider = tabAction.Provider self.tabUserAction:Reset() end
	if bHu then 
		self.HuType = huType
		self.HuFan = fan
	end
	return bHu, huType, fan, provider
end

function ChairAction:DoActionReq(act, centerCard, cardBef, cardAft)
    local tabAction = self:CouldOperate(act.Type, centerCard)
	if tabAction then 
        self.tabUserAction:Reset()
        if self.tabFrame:WaitForOtherActs(self, act.Type, centerCard, cardBef, cardAft, tabAction.Provider) then return false end
        local bDone = act.Func(self, tabAction, centerCard, cardBef, cardAft)
		return bDone and tabAction.Provider
	else return nil end
end

function ChairAction:DoDelayAction(type, tabCardData, provider)
    if type ~= WIKTYPE.WIK_HU then 
        self:DoAction(type, tabCardData, provider)
        self.tabFrame:DoActionCmd(type, self, provider, tabCardData[1], tabCardData[2] or 0, tabCardData[3] or 0)
        self.tabFrame:SetNextUser()
        if type == WIKTYPE.WIK_JIAGANG then 
            self.tabFrame:DispatchCard(self, type)
        end
    else 
        local bHu, huType, fan, prid = self:DoHu(tabCardData[1])
        if bHu then
            self.tabFrame:DoHuCmd(self, prid, tabCardData[1], huType, fan)
        end
    end
end

function ChairAction:DoPeng(tabAction, centerCard)
	return self:DoAction(WIKTYPE.WIK_PENG, {centerCard}, tabAction.Provider)
end

function ChairAction:DoChi(tabAction, centerCard, cardBef, cardAft)
    if self.tabFrame:DoOthersHigherAct(self, WIKTYPE.WIK_LINE) then 
        if self.tabUserAction then 
            self.tabUserAction:Reset() 
        end
        self.tabToDo = nil
        return false
    end
    
    local index1 = MjLogic:ValToIndex(cardBef)
    local index2 = MjLogic:ValToIndex(cardAft)
    self.tabCbCardIndex[index1] = self.tabCbCardIndex[index1] - 1
    self.tabCbCardIndex[index2] = self.tabCbCardIndex[index2] - 1
	self:ResetLou()
	local weaveItem = {}
	weaveItem.CenterCard = centerCard
    weaveItem.Bef = cardBef
    weaveItem.Aft = cardAft
	weaveItem.Provider = tabAction.Provider;
	weaveItem.Kind = WIKTYPE.WIK_LINE
	table.insert(self.tabWeaveItem, weaveItem) 
    
	if self.tabUserAction then 
		self.tabUserAction:Reset() 
	end
    for chair in self.tabFrame:ChairIterator() do 
       chair.tabToDo = nil     --动作都执行了,每个延迟的就清空
    end
    return true
end

function ChairAction:DoJieGang(tabAction, centerCard)
	self:DoAction(WIKTYPE.WIK_JIEGANG, {centerCard}, tabAction.Provider)
    self:UpdataTingData()
    self.LianGang = 1
    return true
end

function ChairAction:DoJiaGang(tabAction, centerCard)
    local nBeiqiangNum = self.tabFrame:CheckQiangGangHu(self, centerCard)
    if nBeiqiangNum > 0 then   --能被抢杠
        self:SendDataInLua(CMD.Sub_GameSvrToClient_TableModle_WaitingForAnotherHu)
        self.tabToDo = {}
        self.tabToDo.Num = nBeiqiangNum
        self.tabToDo.Func = self.DoDelayAction
        self.tabToDo.tabParam = {WIKTYPE.WIK_JIAGANG, {centerCard}, tabAction.Provider}
        return false 
    end     
	self:DoAction(WIKTYPE.WIK_JIAGANG, {centerCard}, tabAction.Provider)
    self:UpdataTingData()
    self.LianGang = (self.LianGang or 0) + 1 
    return true
end

function ChairAction:DoAnGang(tabAction, centerCard)
    tabAction.Provider = self.UserChairID
	self:DoAction(WIKTYPE.WIK_ANGANG, {centerCard})
    self:UpdataTingData()
    self.LianGang = (self.LianGang or 0) + 1
    return true
end

function ChairAction:DoAction(type, cardData, provider)
    local centerCard = cardData[1]
	local valIndex = MjLogic:ValToIndex(centerCard)
	local numToMinus = 0
    local nGang = #self.tabFrame.tabGang
	if type == WIKTYPE.WIK_PENG then numToMinus = 2 
	elseif type == WIKTYPE.WIK_JIEGANG then numToMinus = 3 self.tabFrame.tabGang[nGang + 1] = {Type = type, Doer = self.UserChairID, Provider = provider} self.nMingGang = (self.nMingGang or 0) + 1
	elseif type == WIKTYPE.WIK_JIAGANG then numToMinus = 1 self.tabFrame.tabGang[nGang + 1] = {Type = type, Doer = self.UserChairID, Provider = provider} self.nMingGang = (self.nMingGang or 0) + 1
	elseif type == WIKTYPE.WIK_ANGANG then numToMinus = 4 self.tabFrame.tabGang[nGang + 1] = {Type = type, Doer = self.UserChairID, Provider = provider} self.nAnGang = (self.nAnGang or 0) + 1
	else numToMinus = 0 end
	self.tabCbCardIndex[valIndex] = self.tabCbCardIndex[valIndex] - numToMinus
	self:ResetLou()
	local weaveItem = {}
	weaveItem.CenterCard = centerCard
	weaveItem.Provider = provider;
	weaveItem.Kind = type
	table.insert(self.tabWeaveItem, weaveItem)
    
    if self.tabUserAction then 
		self.tabUserAction:Reset() 
	end
    for chair in self.tabFrame:ChairIterator() do 
       chair.tabToDo = nil     --动作都执行了,每个延迟的就清空
    end
    return true    
end

function ChairAction:ResetLou()
    self.LouHu = false
    self.LouPeng = {}
end

function ChairAction:CouldOperate(type, centerCard)
    local useAct = nil
	for i = 1, #self.tabUserAction do
		if self.tabUserAction[i].Type == type and self.tabUserAction[i].CardData == centerCard then
            useAct = self.tabUserAction[i]
			break
		end
	end
	return useAct
end

function ChairAction:CancelActionReq(type)     --有可能已经取消,要防止重复
    local bOk, centerCard = self:CancelAction(type)
	if bOk then
        self:SendDataInLua(self.CANCELCMD[type])
        if type == WIKTYPE.WIK_HU then         --漏胡
            self.LouHu = true
        elseif type == WIKTYPE.WIK_PENG then
            self.LouPeng = self.LouPeng or {}
            if centerCard then self.LouPeng[centerCard] = true end
        end
        skynet.error('Chair Id '..self.UserChairID..' Cancel type is'..type..' Card is '..centerCard)
		return true
	end
	return false
end

function ChairAction:CancelAction(type)
	local bHas = false
    local centerCard
	for i = 1, #self.tabUserAction do
		if self.tabUserAction[i].Type == type then
			bHas = true
            centerCard = self.tabUserAction[i].CardData
			self.tabUserAction:Reset()
			break
		end
	end
	return bHas, centerCard
end



function ChairAction:OnLiuJu()
	
end

function ChairAction:ConcludeGame(huData)
	self.standCard = {}
	self.FengHu = 0
	self.ResultMsg = ""
	local nStand = 0
	for i = 1, #self.tabCbCardIndex do 
		local indexNum = self.tabCbCardIndex[i]
		for j = 1, indexNum do
			nStand = nStand + 1
			self.standCard[nStand] = MjLogic:IndexToVal(i) 	
		end
	end
	if self.DoneHu and huData.ZiMo then
		local zimoMsg = "自摸  "
		local huTypeMsg, huFanMsg = "平胡", ""
		if self.HuType == HUTYPE.HT_PINGHU then huTypeMsg = "平胡"
		elseif self.HuType == HUTYPE.HT_PENGPENGHU then huTypeMsg = "碰碰胡"
		elseif self.HuType == HUTYPE.HT_QIXIAODUI then huTypeMsg = "七对"
		elseif self.HuType == HUTYPE.HT_QIDADUI then huTypeMsg = "七大对"
		elseif self.HuType == HUTYPE.HT_HAOQI then huTypeMsg = "豪七"
		elseif self.HuType == HUTYPE.HT_CHAOHAOQI then huTypeMsg = "超豪七"
		elseif self.HuType == HUTYPE.HT_SHISANYAO then huTypeMsg = "十三幺"
		else end
		if self.Fan and (self.Fan | QINGYISE) then huFanMsg = "清一色" end
        self.ResultMsg = zimoMsg..huFanMsg..huTypeMsg
	end
	if huData.ProviderId == self.UserChairID then self.ResultMsg = "点炮" end
    if huData.LiuJu then self.ResultMsg = "流局" end
    if self.DoneHu and self.MaGot and self.MaGot > 0 then self.ResultMsg = self.ResultMsg..string.format("  中%d马", self.MaGot) end
    if self.LianGang and self.LianGang > 1 then self.ResultMsg = self.ResultMsg..string.format('  %d连杠', self.LianGang) end
    if self.GangScoreJu ~= 0 then self.ResultMsg = self.ResultMsg..string.format('  杠分%d', self.GangScoreJu) end
end

function ChairAction:GetFanWaiFan()
    function IsMengQing()
        if #self.tabWeaveItem == 0 then return true end
        for _, v in pairs(self.tabWeaveItem) do
            if v.Kind == WIKTYPE.WIK_LINE then return false 
            elseif v.Kind == WIKTYPE.WIK_PENG then return false 
            elseif v.Kind == WIKTYPE.WIK_JIAGANG then return false 
            elseif v.Kind == WIKTYPE.WIK_JIEGANG then return false 
            end
        end
        return true
    end
    
    function IsTianHu()
        if self.tabFrame.Banker == self.UserChairID and #self.tabCbOutCard == 0 then return true else return false end
    end
    function IsDiHu()
        for chair in self.tabFrame:ChairIterator() do
            if chair.UserChairID == self.tabFrame.Banker then 
                if #chair.tabCbOutCard ~= 1 then return false end
            else
                if chair.UserChairID ~= 0 then return false end
            end
        end
        return true
    end
	function IsQuanQiuRen()
        if #self.tabWeaveItem == 4 then 
            for _, v in pairs(self.tabWeaveItem) do
                if v.Kind == WIKTYPE.WIK_ANGANG then return false end
            end
            return true
        else return false end
    end
    local fan = 0
    if IsMengQing() then skynet.error('Chair Id '..self.UserChairID..' Hu Meng Qing') fan=fan|FANWAIFAN.MENQINIG end
    if IsTianHu() then skynet.error('Chair Id '..self.UserChairID..' Hu TianHu') fan=fan|FANWAIFAN.TIANHU end
    if IsDiHu() then skynet.error('Chair Id '..self.UserChairID..' Hu DiHu') fan=fan|FANWAIFAN.DIHU end
    if IsQuanQiuRen() then skynet.error('Chair Id '..self.UserChairID..' Hu QuanQiuRen') fan=fan|FANWAIFAN.QUANQIUREN end
    return fan
end

function ChairAction:GetFanBeiScore()  --翻倍的分数,包括了马,杠花
    if not self.tabHuResult.HuType then return 0 end
    local nBei = 0
    nBei = (self.MaGot or 0) + 1         --马
    nBei = nBei + (self.LianGang or 0)   --包括了杠花
    if self.tabHuResult.HuType > HUTYPE.HT_PINGHU then 
        if self.tabHuResult.Fan&(FANWAIFAN.HAIDIPAO|FANWAIFAN.GANGPAO|FANWAIFAN.QIANGGANGHU|FANWAIFAN.QUANQIUREN|FANWAIFAN.QUANQIURENDIANPAO) > 0 then
            nBei = nBei + 1
        end
    end
    if self.tabHuResult.Fan&FANWAIFAN.HAIDIHU > 0 then nBei = nBei + 1 end   --海底胡
    return nBei
end

function ChairAction:GetStandCard()
    local standCard = {}
    local nStand = 0
    for i = 1, #self.tabCbCardIndex do 
        local num = self.tabCbCardIndex[i]
        for j = 1, num do
            nStand = nStand + 1
            standCard[nStand] = MjLogic:IndexToVal(i) 	
        end
    end
    self.standCard = standCard
    return standCard
end

function ChairAction:SendMeOfflineBackData(index)
    local bufData = ''
	if index == 'standCard' then
        if not self.standCard then self:GetStandCard() end
		for i = 1, TABLECONST.MAX_COUNT do
			local val = self.standCard[i]
            bufData = bufData..string.pack('B', val or 0)
		end
        
	elseif index == 'weaveItem' then 
		for i = 1, TABLECONST.MAXWEAVCOUNT do
            local weave = self.tabWeaveItem[i]
            if weave then 
                bufData = bufData..string.pack('BBI2BB', weave.Kind, weave.CenterCard, weave.Provider or 255, weave.Bef or 0, weave.Aft or 0)        
            else 
                bufData = bufData..BufTool.WriteOffset(6)
            end
		end
        
	elseif index == 'outCard' then 
        if not self.tabCbOutCard then 
            bufData = bufData..BufTool.WriteOffset(TABLECONST.MAXOUTCARDCOUNT)
        else 
            for i = 1, TABLECONST.MAXOUTCARDCOUNT do
                bufData = bufData..string.pack('B', self.tabCbOutCard[i] or 0)
            end
        end

	elseif index == 'tingData' then
        if not self.tabTingCardData or not self.tabTingCardData.val then 
            bufData = bufData..BufTool.WriteOffset(TABLECONST.MAX_TING)
        else
            for i = 1, TABLECONST.MAX_TING do                 
                local vt = self.tabTingCardData.val[i]
                bufData = bufData..string.pack('B', vt or 0)
            end
        end	
	end
    return bufData
end
