local skynet = require "skynet"
local cluster = require "cluster"
local snax = require "snax"
local mclient = skynet.getenv "max_client" 
local lport = skynet.getenv "listen_port" 
local dport = skynet.getenv "debug_port" 
local name  = skynet.getenv "name"
--about sql conf
local sql_host = skynet.getenv "sql_host"
local sql_port = skynet.getenv "sql_port"
local sql_database = skynet.getenv "sql_database"
local sql_user = skynet.getenv "sql_user"
local sql_password = skynet.getenv "sql_password"
local sql_max_packet_size = skynet.getenv "sql_max_packet_size"

local center_ip = skynet.getenv "center_ip"
local center_port = skynet.getenv "center_port"
local room_type     = skynet.getenv "room_type"
local table_pre   = skynet.getenv "table_id_pre"
local game_id     = skynet.getenv "game_id"

local Sraw = require "stable.raw"
Sraw.init()
local db   = require "dblib"
--local login = require "login"

local playerList = Sraw.create()   --table_uid:playerinfo

local sqlconf = {
        host = sql_host,
        port = sql_port,
        database = sql_database,
        user = sql_user,
        password = sql_password,
        max_packet_size = sql_max_packet_size,
}

skynet.start(function()
	skynet.error("Server start wcenter")

	if not skynet.getenv "daemon" then
		local console = skynet.newservice("console")
	end
	
    skynet.newservice("debug_console",dport)

	local watchdog = skynet.newservice("watchdog")
	
    skynet.call(watchdog, "lua", "start", {
		port = lport,
		maxclient = tonumber(mclient),
		nodelay = true,
        playerlist = playerList,
	})

    db.init(sqlconf, 1)

    skynet.error("Watchdog listen on", lport)
    local login = skynet.uniqueservice("login")
    skynet.call(login,"lua","Init",playerList)
    --login.Init(playerlist)
    
    local manager =  skynet.uniqueservice("manager")
    local sqlserver = skynet.uniqueservice("sql")
    
    local center_con =  skynet.uniqueservice("center_con")
    skynet.call(center_con, "lua", "start", {center_ip = center_ip, center_port = center_port, room_type=room_type, game_id = game_id, table_pre = table_pre})
    
    skynet.call(manager, "lua", "start",playerList)

end)
