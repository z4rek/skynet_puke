local Object = require("core").Object
local Seat = Object:extend()

Seat.SEAT_STATUS_WAIT = 0			--待准备
Seat.SEAT_STATUS_READY = 1			--已准备
Seat.SEAT_STATUS_START = 2			--游戏开始

function Seat:initialize(seatId)
	self.m_wChairId = seatId
	self.m_isValid = 0
	self.lock_  = false -- 独占锁 用于坐下前锁住
	self:reset()
	-------------------------------兼容以前的
	self.nMingGang = 0
	self.nAnGang = 0
	self.cbHP = 0
	self.cbHT = 0
	self.HuPaiTotal = 0
	self.nScoreJu=0			--单局输赢
end

function Seat:lock(timeout)
	--已经锁了 或者 位置上已经有人 加锁失败
	if self.lock_ or self.Player then
		return false
	end

	-- timeout ms内没有解锁则自动解锁
	self.lockTimeoutHandler = setTimeout(timeout, function()
		self.lockTimeoutHandler = nil
		self:unlock()
	end)
	self.lock_ = true
	return true
end

function Seat:unlock()
	if self.lockTimeoutHandler then
		clearTimeout(self.lockTimeoutHandler)
	end
	self.lock_ = false
end

function Seat:Sit(user)
	if self.Player == nil then
		self:reset()
		self.Player = user
		self.IsOffLine = false
		return true
	end
	return false
end

function Seat:stand()
	self:reset()
end

-- 座位reset
function Seat:reset()
	self.Player = nil
	self.IsReady = 0
	self.nScores = 0
	self.nAnGang = 0 
	self.nMingGang = 0
	self.m_nHuPaiTotal = 0
	self.cbHP = 0
    self.cbHT = 0 
    self.HuPaiTotal = 0
    self.m_nJieSanChoice = 0
    self.playCount = 0
    self.IsOffLine = true
    self.GPS_X = 0
    self.GPS_Y = 0
end

function Seat:isEmpty()
    return (nil == self.Player) and (true) or (false)
end


function Seat:reduceMoney(addmoney)
	if addmoney <= 0 then
		return false
	end
	self.money = self.money - addmoney
	self.lastWinMoney = -addmoney
	return self.money
end

function Seat:addMoney(addmoney)
	if addmoney <= 0 then
		return false
	end
	self.money = self.money + addmoney
	self.lastWinMoney = addmoney
	return self.money
end

function Seat:win()
	self.winCount = self.winCount + 1
end

function Seat:lose()
	self.loseCount = self.loseCount + 1
end

return Seat
