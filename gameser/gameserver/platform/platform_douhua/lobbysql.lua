local skynet = require "skynet"
local db   = require "dblib"
local CMD = require "main_proto"
local CMDGAME = require "game_proto"
local Sraw = require "stable.raw"
local bufferrw = require "buffrw"
local buffwrite = bufferrw.BuffWrite
local buffread  = bufferrw.BuffRead

Sraw.init()
local QueryStr = require("core").QueryStr

local function SendData(addr,msg)
	skynet.send(math.floor(addr),"lua","send_data",msg)
end



local lobby = {}
--钻石兑换老友豆
function lobby.ChangeJinBiReq(smsg,player)
	local nDiamondCount = string.unpack("I4",smsg,5)
	if tonumber(player.nDiamond) < nDiamondCount then
		return
	end

	local nCoins = nDiamondCount * 2500


	if lobby.AddOrSubPlayerDiamond(player,-nDiamondCount,"兑换 余额 :"..tostring(player.nDiamond)) then
		lobby.AddOrSubPlayerJinBi(player,nCoins,"兑换 余额 :"..tostring(player.nDiamond))
	else
		return
	end

	skynet.error("player.nDiamond = ",player.nDiamond," player.llJinBi = ",player.llJinBi)
	local msg = string.pack(">I2>I2<I4I4I4I8I4",CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_PlayerBasicUpdate,
		math.floor(player.nPlayerId),player.nDiamond,player.nScore,player.llJinBi,player.nExternCode)
	SendData(player.nAgentAddr,msg)

	msg = string.pack(">I2>I2<c96",CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_HingMsg,"兑换黄豆成功")
	SendData(player.nAgentAddr,msg)
end
--增减金币
function lobby.AddOrSubPlayerJinBi(player,nCount,szMsg)
	if nCount == 0 then return end
	if nCount < 0 then
		if player.llJinBi < math.abs(nCount) then return false end
	end
	player.llJinBi = player.llJinBi + nCount
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_AddOrSubPlayerJinBi(")
	QueryStr:AddStr(math.floor(player.nPlayerId))
	QueryStr:AddStr(nCount)
	QueryStr:AddStr(")")
	db.call(QueryStr.str)
	--增减金币日志
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_UserGoldActionLog(")
	QueryStr:AddStr(math.floor(player.nPlayerId))
	QueryStr:AddStr(player.szName)
	QueryStr:AddStr(nCount)
	QueryStr:AddStr(szMsg)
	QueryStr:AddStr(")")
	db.call(QueryStr.str)
	return true
end

--增减钻石
function lobby.AddOrSubPlayerDiamond(player,nDiamondCount,szMsg)
	if nDiamondCount == 0 then return end
	if nDiamondCount < 0 then
		if player.nDiamond < math.abs(nDiamondCount) then return false end
	end
	player.nDiamond = player.nDiamond + nDiamondCount
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_AddOrSubPlayerDiamond(")
	QueryStr:AddStr(math.floor(player.nPlayerId))
	QueryStr:AddStr(nDiamondCount)
	QueryStr:AddStr(")")
	db.call(QueryStr.str)
	--增减钻石日志
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_UserDiamondActionLog(")
	QueryStr:AddStr(math.floor(player.nPlayerId))
	QueryStr:AddStr(player.szName)
	QueryStr:AddStr(nDiamondCount)
	QueryStr:AddStr(szMsg)
	QueryStr:AddStr(")")
	db.call(QueryStr.str)

	return true
end

function lobby.BuyDiamondReq(smsg,player)
	local szPrepayNo = string.unpack("c128")

	QueryStr:Clean()
	QueryStr:AddStr("call Pr_PayGetDiamond(")
	QueryStr:AddStr(math.floor(player.nPlayerId))
	QueryStr:AddOutParam("@intOutTotalDiamond")
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddStr(")")
	db.call(QueryStr.str)

	local res1,res2 = db.call(QueryStr.str,"select @intOutTotalDiamond,@returnVal")

	if res2 and res2[1] then
		local nOuTotalDiamond = res2[1]['@intOutTotalDiamond']
		local nAddDiamond = nOuTotalDiamond - math.floor(player.nDiamond)
		msg = string.pack(">I2>I2<c96",CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_HingMsg,"钻石购买成功")
		SendData(player.nAgentAddr,msg)

		player.nDiamond = nOuTotalDiamond
		lobby.UpdatePlayerBasicInfo(player)
	else
		msg = string.pack(">I2>I2<c96",CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_HingMsg,"system error")
		SendData(player.nAgentAddr,msg)
	end
end

function lobby.ZhanjiReq(player)
	local function QueryZhanJi(playId)
		QueryStr:Clean()
		QueryStr:AddStr("call Pr_LoadZhanJi4(")
		QueryStr:AddStr(math.floor(playId))
		QueryStr:AddStr(")")
		local res = db.call(QueryStr.str)

		local result = {nPlayerId = nil, nCount = nil, OneZhanJiList = {}, bSucc = nil}
	    if res and res[1] and #res[1] == 0 then  -- 返回列表长度
	        result.bSucc = false
	        return result
	    end

	    local count = 0    
	    result.nPlayerId = play_id
	    for index,table_obj in pairs(res[1]) do 
	        count = count + 1
	        local OneZhanJi = {nIdx = nil, nTableId = nil, wMyChairId = nil, nRound = nil, nGameId = nil, szTime = nil,szNames = {}, nScores = {}}
	        OneZhanJi['nIdx'] = table_obj['@intOutIdx']
	        OneZhanJi['nTableId'] = table_obj['@intOutTableId']
	        OneZhanJi['wMyChairId'] = table_obj['@intOutMyChair']
	        OneZhanJi['nRound'] = table_obj['@intTotalRound']
	        OneZhanJi['nGameId'] = table_obj['@intOutGameID']        
	        OneZhanJi['szTime'] = table_obj['@szOutTime']	
	        table.insert(OneZhanJi.szNames,table_obj['@szChair0Name'])
	        table.insert(OneZhanJi.szNames,table_obj['@szChair1Name'])
	        table.insert(OneZhanJi.szNames,table_obj['@szChair2Name'])
	        table.insert(OneZhanJi.szNames,table_obj['@szChair3Name'])
	        table.insert(OneZhanJi.szNames,table_obj['@szChair4Name'])
	        table.insert(OneZhanJi.szNames,table_obj['@szChair5Name'])
	        table.insert(OneZhanJi.szNames,table_obj['@szChair6Name'])
	        table.insert(OneZhanJi.szNames,table_obj['@szChair7Name'])
	        
	        table.insert(OneZhanJi.nScores,table_obj['@intChair0Score'])
	        table.insert(OneZhanJi.nScores,table_obj['@intChair1Score'])
	        table.insert(OneZhanJi.nScores,table_obj['@intChair2Score'])
	        table.insert(OneZhanJi.nScores,table_obj['@intChair3Score'])
	        table.insert(OneZhanJi.nScores,table_obj['@intChair4Score'])
	        table.insert(OneZhanJi.nScores,table_obj['@intChair5Score'])
	        table.insert(OneZhanJi.nScores,table_obj['@intChair6Score'])
	        table.insert(OneZhanJi.nScores,table_obj['@intChair7Score'])
	        table.insert(result.OneZhanJiList,OneZhanJi)
	    end
	    result.nCount = count
	    result.bSucc = true    
	    return result
	end

	local result = QueryZhanJi(player.nPlayerId)
	print(result.nCount)
	local BuffWrite = buffwrite.new()
    BuffWrite:proto(CMD.Main_ClientAndGameSvr_UserModle, CMD.Sub_GameSvrToClient_UserModle_ZhanJi)
    if result.bSucc then         
        BuffWrite:int4(result.nCount)        
        for k , OneZhanJi in pairs(result.OneZhanJiList) do
            BuffWrite:int4(OneZhanJi.nIdx)
            BuffWrite:int4(OneZhanJi.nTableId)
            BuffWrite:int4(OneZhanJi.nRound)           
            BuffWrite:int4(OneZhanJi.nGameId)
            BuffWrite:int2(OneZhanJi.wMyChairId)
            
            for i = 1, 8 do
                BuffWrite:int4(OneZhanJi.nScores[i])
            end
            for i = 1, 8 do
                BuffWrite:str(OneZhanJi.szNames[i],64)
            end            
            BuffWrite:str(OneZhanJi.szTime,32)           
        end        
    end
    SendData(player.nAgentAddr,BuffWrite.buff)
end
--代理开房数据库记录
function lobby.AddAgentRoom(pReq)
	QueryStr:Clean()
	QueryStr:AddStr("call pr_AgentCreateTable2(")
	QueryStr:AddStr(math.floor(pReq.nPlayerID))
	QueryStr:AddStr(math.floor(pReq.nTableID))
	QueryStr:AddStr(math.floor(pReq.nGameID))
	QueryStr:AddStr(math.floor(pReq.nMode))
	QueryStr:AddStr(math.floor(pReq.nPlayerCount))
	QueryStr:AddStr(math.floor(pReq.nRound))
	QueryStr:AddStr(math.floor(pReq.nCost))
	QueryStr:AddStr(pReq.szRoomInfo)
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddOutParam("@r")
	QueryStr:AddStr(")")
	local res1,res2 = db.call(QueryStr.str,"select @returnVal,@r")
	local pResp = {}
	pResp.nPlayerID = math.floor(pReq.nPlayerID)
    pResp.nTableID  = math.floor(pReq.nTableID)
    pResp.nResult = 1;
    if res2[1]['@returnVal'] then
    	pResp.nResult = res2[1]['@returnVal']
    else
    	pResp.nResult = -1;
	end
	return pResp
end

--代理解散房间
function lobby.OnAgentDismissTable(nRoomId,nAgentID)
	QueryStr:Clean()
	QueryStr:AddStr("call pr_AgentDismissTable(")
	QueryStr:AddStr(math.floor(nAgentID))
	QueryStr:AddStr(math.floor(nRoomId))
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddOutParam("@r")
	QueryStr:AddStr(")")
	local res1,res2 = db.call(QueryStr.str,"select @returnVal,@r")
	local result = 0
	if res2 then
		result = res2[1]['@returnVal']
	end
	local pResp = {}
	pResp.nPlayerID    = nAgentID
    pResp.nTableID     = nRoomId
    pResp.nResult 	= result
    return pResp
end

function lobby.UpdatePlayerBasicInfo(player)
	local msg = string.pack(">I2>I2<I4I4I4I8I4",
		CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_PlayerBasicUpdate,
		math.floor(player.nPlayerId),math.floor(player.nDiamond),math.floor(player.nScore),math.floor(player.llJinBi),math.floor(player.nExternCode or 0))
	SendData(player.nAgentAddr,msg)
end
--更新用户信息
function lobby.OnUserUpdateInfo(player)
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_UserGetDiamondAndJinBi(")
	QueryStr:AddStr(math.floor(player.nPlayerId))
	QueryStr:AddOutParam("@nDiamond")
	QueryStr:AddOutParam("@nJinBi")
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddStr(")")
	local res1,res2 = db.call(QueryStr.str,"select @nDiamond,@nJinBi,@returnVal")
	player.nDiamond = res2[1]['@nDiamond']
	player.llJinBi = res2[1]['@nJinBi']
	lobby.UpdatePlayerBasicInfo(player)
end

function lobby.OnNetGetUserPhoneInformation(player)
	local bReg = 0
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_GetUserPhone(")
	QueryStr:AddStr(math.floor(player.nPlayerId))
	QueryStr:AddOutParam("@phone")
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddStr(")")
	local res1,res2 = db.call(QueryStr.str,"select @phone,@returnVal")
	if res2 and res2[1] then
		bReg = 1
	end
	local msg = string.pack(">I2>I2<I1",
		CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_UserInformation,bReg)
	SendData(player.nAgentAddr,msg)
end
--写总战绩
function lobby.WriteZhanJiDb(pData)
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_WriteZhanJi3(")
	QueryStr:AddStr(math.floor(pData.nPlayerId))
	QueryStr:AddStr(tonumber(pData.nGameId))
	QueryStr:AddStr(pData.nTableId)
	QueryStr:AddStr(pData.nGameRound)
	QueryStr:AddStr(pData.nTotalRound)
	QueryStr:AddStr(pData.wMyChairId)
	QueryStr:AddStr(pData.nScores[1])
	QueryStr:AddStr(pData.nScores[2])
	QueryStr:AddStr(pData.nScores[3])
	QueryStr:AddStr(pData.nScores[4])
	QueryStr:AddStr(pData.nScores[5])
	QueryStr:AddStr(pData.nScores[6])
	QueryStr:AddStr(pData.nScores[7])
	QueryStr:AddStr(pData.nScores[8])

	QueryStr:AddStr(pData.szNames[1])
	QueryStr:AddStr(pData.szNames[2])
	QueryStr:AddStr(pData.szNames[3])
	QueryStr:AddStr(pData.szNames[4])
	QueryStr:AddStr(pData.szNames[5])
	QueryStr:AddStr(pData.szNames[6])
	QueryStr:AddStr(pData.szNames[7])
	QueryStr:AddStr(pData.szNames[8])
	QueryStr:AddStr(pData.szTime)
	QueryStr:AddOutParam("@intOutIdx")
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddStr(")")
	skynet.error("写总战绩",QueryStr.str)
	local res1,res2 = db.call(QueryStr.str,"select @intOutIdx,@returnVal")
end

function lobby.OnAgentTableStatus(pReq)
	QueryStr:Clean()
	QueryStr:AddStr("call pr_AgentSetTableStatus(")
	QueryStr:AddStr(math.floor(pReq.nPlayerID))
	QueryStr:AddStr(math.floor(pReq.nTableID))
	QueryStr:AddStr(math.floor(pReq.nStatus))
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddOutParam("@r")
	QueryStr:AddStr(")")
	local res1,res2 = db.call(QueryStr.str,"select @returnVal,@r")

	if res2 and res2[1]['@returnVal'] == 1 then
		return true
	end

	return false
end

--写分战绩
function lobby.WriteUserZhanJi(pReg)
	skynet.error("数据库写战绩")
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_WriteUserZhanJi3(")
	QueryStr:AddStr(pReg.nGameId)
	QueryStr:AddStr(pReg.nTableId)
	QueryStr:AddStr(pReg.nPlayerId)
	QueryStr:AddStr(pReg.szNickName)

	QueryStr:AddStr(pReg.szStarTime)
	QueryStr:AddStr(pReg.szEndTime)
	QueryStr:AddStr(pReg.szRecToken)
	QueryStr:AddStr(pReg.nDiFen)
	QueryStr:AddStr(pReg.jushu)
	QueryStr:AddStr(pReg.nTotalJu)
	QueryStr:AddStr(pReg.fuju)
	QueryStr:AddStr(pReg.fuType)
	QueryStr:AddStr(pReg.fuFangShi)
	QueryStr:AddStr(pReg.zhangjia)

	QueryStr:AddStr(pReg.nPlayerIdArr[1] or 0)
	QueryStr:AddStr(pReg.nArrowArr[1] or 0)
	QueryStr:AddStr(pReg.nScores[1] or 0)
	QueryStr:AddStr(pReg.szNames[1] or "")

	QueryStr:AddStr(pReg.nPlayerIdArr[2] or 0)
	QueryStr:AddStr(pReg.nArrowArr[2] or 0)
	QueryStr:AddStr(pReg.nScores[2] or 0)
	QueryStr:AddStr(pReg.szNames[2] or "")

	QueryStr:AddStr(pReg.nPlayerIdArr[3] or 0)
	QueryStr:AddStr(pReg.nArrowArr[3] or 0)
	QueryStr:AddStr(pReg.nScores[3] or 0)
	QueryStr:AddStr(pReg.szNames[3] or "")

	QueryStr:AddStr(pReg.nPlayerIdArr[4] or 0)
	QueryStr:AddStr(pReg.nArrowArr[4] or 0)
	QueryStr:AddStr(pReg.nScores[4] or 0)
	QueryStr:AddStr(pReg.szNames[4] or "")

	QueryStr:AddStr(pReg.nPlayerIdArr[5] or 0)
	QueryStr:AddStr(pReg.nArrowArr[5] or 0)
	QueryStr:AddStr(pReg.nScores[5] or 0)
	QueryStr:AddStr(pReg.szNames[5] or "")

	QueryStr:AddStr(pReg.nPlayerIdArr[6] or 0)
	QueryStr:AddStr(pReg.nArrowArr[6] or 0)
	QueryStr:AddStr(pReg.nScores[6] or 0)
	QueryStr:AddStr(pReg.szNames[6] or "")

	QueryStr:AddStr(pReg.nPlayerIdArr[7] or 0)
	QueryStr:AddStr(pReg.nArrowArr[7] or 0)
	QueryStr:AddStr(pReg.nScores[7] or 0)
	QueryStr:AddStr(pReg.szNames[7] or "")

	QueryStr:AddStr(pReg.nPlayerIdArr[8] or 0)
	QueryStr:AddStr(pReg.nArrowArr[8] or 0)
	QueryStr:AddStr(pReg.nScores[8] or 0)
	QueryStr:AddStr(pReg.szNames[8] or "")

	QueryStr:AddStr(pReg.mingGanArr[1] or 0)
	QueryStr:AddStr(pReg.mingGanArr[2] or 0)
	QueryStr:AddStr(pReg.mingGanArr[3] or 0)
	QueryStr:AddStr(pReg.mingGanArr[4] or 0)
	QueryStr:AddStr(pReg.mingGanArr[5] or 0)
	QueryStr:AddStr(pReg.mingGanArr[6] or 0)
	QueryStr:AddStr(pReg.mingGanArr[7] or 0)
	QueryStr:AddStr(pReg.mingGanArr[8] or 0)

	QueryStr:AddStr(pReg.anganArr[1] or 0)
	QueryStr:AddStr(pReg.anganArr[2] or 0)
	QueryStr:AddStr(pReg.anganArr[3] or 0)
	QueryStr:AddStr(pReg.anganArr[4] or 0)
	QueryStr:AddStr(pReg.anganArr[5] or 0)
	QueryStr:AddStr(pReg.anganArr[6] or 0)
	QueryStr:AddStr(pReg.anganArr[7] or 0)
	QueryStr:AddStr(pReg.anganArr[8] or 0)

	QueryStr:AddStr(pReg.wMyChairId)
	QueryStr:AddStr(pReg.bEndJu)

	QueryStr:AddOutParam("@intOutIdx")
	QueryStr:AddOutParam("@returnVal")
	skynet.error("写数据库完成",QueryStr.str)
	QueryStr:AddStr(")")
	local res1,res2 = db.call(QueryStr.str,"select @intOutIdx,@returnVal")

	for k,v in pairs(res1) do
		skynet.error(k,v)
	end
end

function lobby.CheckPlayerInAgentTable(pReq)
	QueryStr:Clean()
	QueryStr:AddStr("call pr_CheckPlayerInAgentTable2(")
	QueryStr:AddStr(math.floor(pReq.nPlayerID))
	QueryStr:AddStr(math.floor(pReq.nAgentID))
	QueryStr:AddOutParam("@intOutVal")
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddStr(")")
	local res1,res2 = db.call(QueryStr.str,"select @intOutVal,@returnVal")

	skynet.error("CheckPlayerInAgentTable = ",res2[1]['@intOutVal'])
	if res2 and res2[1]['@intOutVal'] == 1 then
		return true
	end

	return false
end

function lobby.OnUserLoginReq(play_id, sex, logintype, nickname, url, password, client)
	local strpass=string.sub(password,1,string.find(password,string.char(0))-1)
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_UserLogin6(")

	QueryStr:AddStr(play_id)
	QueryStr:AddStr(strpass)
	QueryStr:AddStr(nickname)
	QueryStr:AddStr(client)
	QueryStr:AddStr(url)

	QueryStr:AddOutParam("@CardCount")
	QueryStr:AddOutParam("@JinBi")
	QueryStr:AddOutParam("@FirstLogin")
	QueryStr:AddOutParam("@PoChanJinBi")
	QueryStr:AddOutParam("@ExtentCode")
	QueryStr:AddOutParam("@UserSign")
	QueryStr:AddOutParam("@GoldRankPos")
	QueryStr:AddOutParam("@RewardLogin")
	QueryStr:AddOutParam("@RewardAlms")
	QueryStr:AddOutParam("@ExGoldCount")
	QueryStr:AddOutParam("@intOutVal")
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddStr(")")
	local res1,res = db.call(QueryStr.str,"select @CardCount,@JinBi,@FirstLogin,@PoChanJinBi,@ExtentCode,@UserSign,@GoldRankPos,@RewardLogin,@RewardAlms,@ExGoldCount,@intOutVal,@returnVal")

	local result = {nPlayerId=nil, nDiamond=nil, nSex=nil, nLoginType=nil, nScore=nil, nFirstLogin=nil, nPoChanJinBi=nil, llJinBi=nil, 
                    szName=nil,szHeadUrl=nil, szPass=nil, szClient=nil, szSign=nil, szErrMsg=nil, bSucc=nil, nExternCode=nil, 
                    nGoldRankPos=nil,nRewardLogin=nil,nRewardAlms=nil,llExGoldCount=nil}
    
    result.nPlayerId        = play_id
    result.nDiamond         = res[1]['@CardCount']
    result.nScore           = 0
    result.llJinBi          = res[1]['@JinBi']
    result.nFirstLogin      = res[1]['@FirstLogin']
    result.nPoChanJinBi     = res[1]['@PoChanJinBi']
    result.nLoginType       = logintype
    result.nSex             = sex
    result.szName           = nickname
    result.szHeadUrl        = url
    result.szPass           = strpass
    result.szClient         = client
    result.szSign           = res[1]['@UserSign']
    result.nGoldRankPos     = res[1]['@GoldRankPos']
    result.nRewardLogin     = res[1]['@RewardLogin']
    result.nRewardAlms      = res[1]['@RewardAlms']
    result.llExGoldCount    = res[1]['@ExGoldCount']
    result.nExternCode      = res[1]['@ExtentCode']
    result.szErrMsg         = res[1]['@ErrMsg']
    result.bSucc            = res[1]['@returnval']  
    return result
end

function lobby.OnGetRewardReq(smsg,player)
	local nType = string.unpack("i",smsg,5)
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_UserGetReward(")
	QueryStr:AddStr(math.floor(player.nPlayerId))
	QueryStr:AddStr(math.floor(nType))
	QueryStr:AddOutParam("@intResult")
	QueryStr:AddOutParam("@intAmount")
	QueryStr:AddOutParam("@intRewardLogin")
	QueryStr:AddOutParam("@intRewardAlms")
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddStr(")")

	local res1,res2 = db.call(QueryStr.str,"select @intResult,@intAmount,@intRewardLogin,@intRewardAlms,@returnVal")
	
	if res2 and res2[1] then
		local pResp = {}
		pResp.nPlayerID	= math.floor(player.nPlayerId)
		pResp.nType = nType
		pResp.nResult = res2[1]['@intResult']
		pResp.nAmount = res2[1]['@intAmount']
		pResp.nReLoginCount = res2[1]['@intRewardLogin']
		pResp.nReAlmsCount = res2[1]['@intRewardAlms']

		if pResp.nResult == 1 then
			player.llJinBi = player.llJinBi+pResp.nAmount
			lobby.UpdatePlayerBasicInfo(player)
		end

		local msg = string.pack(">I2>I2<iiiii",CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_GetRewardResp,
			pResp.nType,pResp.nResult,pResp.nAmount,pResp.nReLoginCount,pResp.nReAlmsCount)
		SendData(player.nAgentAddr,msg)
	end
end

function lobby.OnPreCreate(playerId,nMaxPlayerCount,nGroupID,nNeedCard,nCreateTrue)
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_SpcTableUserCreateTable(")
	QueryStr:AddStr(math.floor(playerId))
	QueryStr:AddStr(math.floor(nMaxPlayerCount))
	QueryStr:AddStr(math.floor(nGroupID))
	QueryStr:AddStr(math.floor(nNeedCard))
	QueryStr:AddStr(math.floor(nCreateTrue))
	QueryStr:AddOutParam("@intResult")
	QueryStr:AddOutParam("@szReturnMsg")
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddStr(")")

	local res1,res2 = db.call(QueryStr.str,"select @intResult,@szReturnMsg,@returnVal")

	if res2 and res2[1] then
		return res2[1]['@intResult'],res2[1]['@szReturnMsg']
	end

	return 0,"失败"
end

function lobby.OnSpcTableStatus(nRoomId,nGroupNo,nStatus)
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_SpcTableUserSetTableStatus(")
	QueryStr:AddStr(math.floor(nRoomId))
	QueryStr:AddStr(math.floor(nGroupNo))
	QueryStr:AddStr(math.floor(nStatus))
	QueryStr:AddStr(")")

	local res1 = db.call(QueryStr.str)
end

function lobby.InputExternReq(smsg,player)
	if player.nExternCode ~= 0 then
		local msg = string.pack(">I2>I2<c96",CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_HingMsg,"已绑定了缴请码")
		SendData(player.nAgentAddr,msg)
		return
	end

	local  playId,nExternCode = string.unpack("ii",smsg,5)

	QueryStr:Clean()
	QueryStr:AddStr("call Pr_WriteExternCode(")
	QueryStr:AddStr(math.floor(player.nPlayerId))
	QueryStr:AddStr(math.floor(nExternCode))
	QueryStr:AddOutParam("@strOutErrMsg")
	QueryStr:AddOutParam("@returnVal")
	QueryStr:AddStr(")")
	local res1,res2 = db.call(QueryStr.str,"select @strOutErrMsg,@returnVal")

	if res2 and res2[1] then
		if res2[1]['@returnVal'] == 1 then
			player.nExternCode = nExternCode
			lobby.AddOrSubPlayerDiamond(player,6,"绑定推荐码")

			local msg = string.pack(">I2>I2<c96",CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_HingMsg,"成功绑定,送你6钻石")
			SendData(player.nAgentAddr,msg)

			lobby.UpdatePlayerBasicInfo(player)
		else
			local msg = string.pack(">I2>I2<c96",CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_HingMsg,res2[1]['@strOutErrMsg'])
			SendData(player.nAgentAddr,msg)
		end
		return
	end

	local msg = string.pack(">I2>I2<c96",CMD.Main_ClientAndGameSvr_UserModle,CMD.Sub_GameSvrToClient_UserModle_HingMsg,"system error")
	SendData(player.nAgentAddr,msg)
end

function lobby.SpcWriteGameScore(pData)
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_SpcTableWriteGameScore(")
	QueryStr:AddStr(pData.nTableId)
	QueryStr:AddStr(math.floor(pData.nGroupID))

	QueryStr:AddStr(math.floor(pData.nCost))
	QueryStr:AddStr(math.floor(pData.nCreateID))
	QueryStr:AddStr(pData.szTime)

	QueryStr:AddStr(pData.nScores[1])
	QueryStr:AddStr(pData.nPlayerIds[1])

	QueryStr:AddStr(pData.nScores[2])
	QueryStr:AddStr(pData.nPlayerIds[2])

	QueryStr:AddStr(pData.nScores[3])
	QueryStr:AddStr(pData.nPlayerIds[3])

	QueryStr:AddStr(pData.nScores[4])
	QueryStr:AddStr(pData.nPlayerIds[4])

	QueryStr:AddStr(pData.nScores[5])
	QueryStr:AddStr(pData.nPlayerIds[5])

	QueryStr:AddStr(pData.nScores[6])
	QueryStr:AddStr(pData.nPlayerIds[6])

	QueryStr:AddStr(pData.nScores[7])
	QueryStr:AddStr(pData.nPlayerIds[7])

	QueryStr:AddStr(pData.nScores[8])
	QueryStr:AddStr(pData.nPlayerIds[8])
	
	QueryStr:AddStr(")")
	skynet.error("写总战绩",QueryStr.str)
	local res1,res2 = db.call(QueryStr.str)
end

function lobby.OnSpcMakeTable(pDB)
	QueryStr:Clean()
	QueryStr:AddStr("call Pr_SpcTableMakeTableRec(")
	QueryStr:AddStr(math.floor(pDB.nPlayerID))
	QueryStr:AddStr(math.floor(pDB.nTableId))
	QueryStr:AddStr(math.floor(pDB.nMaxPlayerCount))
	QueryStr:AddStr(math.floor(pDB.nGroupID))
	QueryStr:AddStr(math.floor(pDB.nNeedCard))
	QueryStr:AddStr(pDB.szRoomMsg)
	QueryStr:AddStr(")")
	local res1 = db.call(QueryStr.str)
end

return lobby