--[[
	此类为所有游戏逻辑的父类。
]]

local Object    = require('core').Object
local Seat      = require('Seat')
--游戏逻辑父类
local GameLogic = Object:extend()
local CMD = require "game_proto"
local MainCmdId = CMD.Main_ClientAndGameSvr_TableModle
local skynet = require "skynet"

--逻辑初始化
function GameLogic:initialize(room)
	self.room = room
	self.m_pChairs = room.m_pChairs
	self.m_tOneMinTimer = false
    self.m_nOneMinCount = 0
    self.m_nThinkTime = 15
end
--设置房间数据
function GameLogic:SetTableData(RoomType, nMaxPlayerCount, ndiFen, nMa, nFangPaoOrZiMo, nDianPaoMaOrZiMoMa, nKeQiangGang, externData)
	return ""
end

--获取座位数量，初始化座位时会根据此函数返回值创建相当数量的座位
function GameLogic:GetSeatCount()
	return 4,{1,2,3,4}
end

--初始化房间数据 房间创建完座位后调用
function GameLogic:InitGameData()
	return true
end
--玩家坐下成功
function GameLogic:OnSitDownSucess(seat)
	return
end
--玩家断线重回
function GameLogic:UserComeBack(seat)
	return
end
--判断是否在游戏中
function GameLogic:IsPlaying()
    return self.room.m_nGameState ~= 2
end
--玩家断线重连发送游戏数据
function GameLogic:OnPlayerOfflineBack(seat)
	return
end
--游戏开始 此函数由Room中直接调用
function GameLogic:OnEventGameStart()
	return true
end
--游戏结束
function GameLogic:ConcludeGame(...)
	return false
end
--游戏大结算
function GameLogic:ConcluedGameLastRound()
	return
end
--处理玩家游戏交互消息
function GameLogic:OnGameMsg(player,scmd,smsg)
	return
end
--游戏开始，由游戏逻辑控制调用，如要自己控制游戏开始重写此方法
function GameLogic:GameStart()
	self.room.GameStart()
end
--游戏结束时调用此函数，此函数会回调GameFinish函数 此函数不需要重写
function GameLogic:GameOver(...)
	self.room.ConcludeGame(...)
end
--[[
	以下两个函数为服务端发送数据到客户端，直接调用不需重写
]]
--发送游戏消息给所有玩家
function GameLogic:SendMsgToAll(cmd,data)
	local msgSend = string.pack('>I2>I2', MainCmdId, cmd)
	if data then msgSend = msgSend..data end
	self.room.SendMsgToAll(msgSend)
end
--发送游戏消息给单个玩家
function GameLogic:SendMsgToUser(seat,cmd,data)
	local msgSend = string.pack('>I2>I2', MainCmdId, cmd)
	if data then msgSend = msgSend..data end
	self.room.SendMsgToUser(seat,data)
end

function GameLogic:GetSeatByPlayer(player)
	for _,seat in pairs(self.m_pChairs) do
		if seat.Player.nPlayerId == player.nPlayerId then
			return seat
		end
	end
	return nil
end

function GameLogic:send_data(seat,data)
	self.room.SendMsgToUser(seat,data)
end
--玩家断线
function GameLogic:UserNetCut(seat)
	return
end
--启动一秒定时器
function GameLogic:StartOneMinTimer()
	self.m_tOneMinTimer = true
	self.m_nOneMinCount = 0
	skynet.fork(GameLogic.OnOneMinTimer,self)
end
--一秒定时器
function GameLogic:OnOneMinTimer()
	while self.m_tOneMinTimer do
		skynet.sleep(100)
		self.m_nOneMinCount = self.m_nOneMinCount + 1
		if self.m_nOneMinCount == 2 then
			self:CheckStandBy()
		else
			if self.m_nOneMinCount == self.m_nThinkTime then
				self:ActionTimeOut()
			end
		end
		--skynet.error("self.m_nOneMinCount = ",self.m_nOneMinCount)
	end
end
--重置一秒定时器
function GameLogic:ResetOneMinTimer()
	self.m_nOneMinCount = 0
end
--结束一秒定时器
function GameLogic:StopOneMinTimer()
	self.m_tOneMinTimer = false
end
--看看是否有托管的
function GameLogic:CheckStandBy()
	return 
end
--看看是否有超时的
function GameLogic:ActionTimeOut()
	self:ResetOneMinTimer()
	return 
end
return GameLogic