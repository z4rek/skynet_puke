local skynet = require "skynet"
local cluster = require "cluster"

local mclient = skynet.getenv "max_client" 
local lport = skynet.getenv "listen_port" 
local dport = skynet.getenv "debug_port" 
local name  = skynet.getenv "name"

 local configserver = nil;


skynet.start(function()
	skynet.error("Server start voice")

	if not skynet.getenv "daemon" then
		local console = skynet.newservice("console")
	end
	
    skynet.newservice("debug_console",dport)

	local watchdog = skynet.newservice("watchdog")
	skynet.call(watchdog, "lua", "start", {
		port = lport,
		maxclient = tonumber(mclient),
		nodelay = true,
	})

    skynet.error("Watchdog listen on", lport)

end)
