
local core = {}

function core.instanceof(obj, class)
  if type(obj) ~= 'table' or obj.meta == nil or not class then
    return false
  end
  if obj.meta.__index == class then
    return true
  end
  local meta = obj.meta
  while meta do
    if meta.super == class then
      return true
    elseif meta.super == nil then
      return false
    end
    meta = meta.super.meta
  end
  return false
end

--------------------------------------------------------------------------------
local Object = {}
core.Object = Object
Object.meta = {__index = Object}

-- Create a new instance of this object
function Object:create()
  local meta = rawget(self, "meta")
  if not meta then error("Cannot inherit from instance object") end
  return setmetatable({}, meta)
end

function Object:new(...)
  local obj = self:create()
  if type(obj.initialize) == "function" then
    obj:initialize(...)
  end
  return obj
end

function Object:extend()
  local obj = self:create()
  local meta = {}
  -- move the meta methods defined in our ancestors meta into our own
  --to preserve expected behavior in children (like __tostring, __add, etc)
  for k, v in pairs(self.meta) do
    meta[k] = v
  end
  meta.__index = obj
  meta.super=self
  obj.meta = meta
  return obj
end


local QueryStr = {}
core.QueryStr = QueryStr
function QueryStr:AddStr(str)
    if type(str) == "number" and string.sub(self.str,string.len(self.str))=="(" then        
        self.str = self.str..str
  elseif type(str) == "number" then        
        self.str = self.str..","..str
    elseif type(str) == "string" and  string.sub(self.str,string.len(self.str))=="(" then 
        self.str = self.str.."\""..str.."\""
    elseif type(str) == "string" and  string.len(self.str)== 0 or string.sub(self.str,string.len(self.str))=="(" then 
    self.str = self.str..str
  elseif type(str) == "string" and  string.len(self.str) >0 then 
    if str == ")" then
      self.str = self.str..str
    else    
      self.str = self.str..",\""..str.."\""
    end
  end
end

function QueryStr:AddOutParam(str)
    self.str = self.str..","..str
end

function QueryStr:Clean()
  self.str = ""
end

return core