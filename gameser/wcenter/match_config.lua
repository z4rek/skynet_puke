local skynet = require "skynet"
local cjson = require "cjson"
local util = require "cjson.util"

local matchConfCmd = {}

local full_match_defalut = [[
{
                "match_number": 1,
                "match_type": 1,
                "match_name": "人满开赛",
                "game_type": 1001,
                "min_man": 4,
                "max_man": 6,
                "pay_type": 1,
                "pay_num": 6,
                "win_man": 8,
                "start_time": 3000,
                "end_time": 9000,
                "start_date": 1505866088,
                "end_date": 1508458088,
                "sort_id":1,
                "room_config": {
                    "player_num": 4,
                    "pingdian": 1,
                    "pinghuazimo": 1,
                    "shishangyao": 1,
                    "huapaima": 1,
                    "zimozhuama": 1,
                    "nozhuama": 1,
                    "twoma": 1,
                    "fourma": 1,
                    "sixma": 1
                },
                "match_config": {
                    "grader_num": 3,
                    "graden": [
                        {
                            "index": 1,
                            "difen": 2,
                            "jushu": 4,
                            "men": 4
                        },
                        {
                            "index": 2,
                            "difen": 2,
                            "jushu": 4,
                            "men": 4
                        },
                        {
                            "index": 3,
                            "difen": 2,
                            "jushu": 4,
                            "men": 4
                        }
                    ]
                },
                "prize_config": {
                    "prize_num": 4,
                    "prize": [
                        {
                            "index": 1,
                            "prize_type": 1,
                            "num": 1,
                            "start_level": 1,
                            "end_level": 1
                        },
                        {
                            "index": 2,
                            "prize_type": 2,
                            "num": 1,
                            "start_level": 2,
                            "end_level": 2
                        },
                        {
                            "index": 3,
                            "prize_type": 2,
                            "num": 3,
                            "start_level": 3,
                            "end_level": 3
                        },
                        {
                            "index": 4,
                            "prize_type": 2,
                            "num": 4,
                            "start_level": 4,
                            "end_level": 8
                        }
                    ]
                },
                "icon_config": {
                    "hall_prize_icon": 1,
                    "bao_min": 2,
                    "bao_min_sai_zi": 3,
                    "baomin_name": 4,
                    "table_tu": 5,
                    "help": 6
                }
            }
 ]]

local time_match_defalut = [[
{
                "match_number": 2,
                "match_type": 2,
                "match_name": "定时开赛",
                "game_type": 1001,
                "min_man": 4,
                "max_man": 6,
                "pay_type": 1,
                "pay_num": 6,
                "win_man": 8,
                "start_time": 3000,
                "end_time": 9000,
                "start_date": 1505866088,
                "end_date": 1508458088,
                "sort_id":2,
                "room_config": {
                    "player_num": 4,
                    "pingdian": 1,
                    "pinghuazimo": 1,
                    "shishangyao": 1,
                    "huapaima": 1,
                    "zimozhuama": 1,
                    "nozhuama": 1,
                    "twoma": 1,
                    "fourma": 1,
                    "sixma": 1
                },
                "match_config": {
                    "grader_num": 3,
                    "graden": [
                        {
                            "index": 1,
                            "difen": 1,
                            "jushu": 4,
                            "men": 4
                        },
                        {
                            "index": 2,
                            "difen": 1,
                            "jushu": 4,
                            "men": 4
                        },
                        {
                            "index": 3,
                            "difen": 1,
                            "jushu": 4,
                            "men": 4
                        }
                    ]
                },
                "prize_config": {
                    "prize_num": 4,
                    "prize": [
                        {
                            "index": 1,
                            "prize_type": 1,
                            "num": 1,
                            "start_level": 1,
                            "end_level": 1
                        },
                        {
                            "index": 2,
                            "prize_type": 1,
                            "num": 1,
                            "start_level": 2,
                            "end_level": 2
                        },
                        {
                            "index": 3,
                            "prize_type": 1,
                            "num": 1,
                            "start_level": 3,
                            "end_level": 3
                        },
                        {
                            "index": 4,
                            "prize_type": 1,
                            "num": 2,
                            "start_level": 4,
                            "end_level": 8
                        }
                    ]
                },
                "icon_config": {
                    "hall_prize_icon": 1,
                    "bao_min": 2,
                    "bao_min_sai_zi": 3,
                    "baomin_name": 4,
                    "table_tu": 5,
                    "help": 6
                }
            }
 ]]

local match_select_type = [[
{
    "game_type_def": {
        "南宁麻将": 1001,
        "柳州麻将": 1007,
        "摆牌八张": 2001,
        "摆牌十三张": 2003,
        "大话水鱼": 2002,
        "桂林麻将": 1008,
        "玉林麻将": 1002,
        "上林麻将": 1003,
        "贵港麻将": 1005,
        "横县麻将": 1004
    },
    "prize_type_def": {
        "钻石": 1,
        "老友豆": 2,
        "话费": 3,
        "积分": 4
    },
    "baoming_type_def": {
        "钻石": 1,
        "老友豆": 2       
    },
    "icon_type_def": {
        "Lobby_Award1": 1,
        "Lobby_Award2": 2,
        "Lobby_Award3": 3,
        "Lobby_Award4": 4,
        "Lobby_Award5": 5,
        "award1": 6,
        "award2": 7,
        "award3": 8,
        "award4": 9,
        "award5": 10,
        "saizhi1": 11,
        "saizhi2": 12,
        "saizhi3": 13,
        "saizhi4": 14,
        "saizhi5": 15,
        "top1": 16,
        "top2": 17,
        "top3": 18,
        "top4": 19,
        "top5": 20,
        "zhuo1": 21,
        "zhuo2": 22,
        "zhuo3": 23,
        "zhuo4": 24,
        "zhuo5": 25,
        "bisai_help1":26,
        "bisai_help2":27,
        "bisai_help3":28,
        "bisai_help4":29,
        "bisai_help5":30
    }
}
 ]]

local match_file_name = "match_all_config.txt"

--加载控制比赛文件
function matchConfCmd.loadDb()

    local configpath = skynet.getenv "config_path"
    
    skynet.error("start loadContrl file:",configpath,match_file_name)

    local f = io.open(configpath.."/"..match_file_name, "r")
    if not f then
        skynet.error("loadContrl file fail :"..match_file_name)
        return nil
    end

    local jsonstr = f:read("*a")

    f:close()

    local ok, jsonobj  = pcall(cjson.decode, jsonstr)
    
    if ok then
        skynet.error("matchConfCmd.loadContrl ok : "..match_file_name)
        return jsonobj
    end
    skynet.error("matchConfCmd.loadContrl fail : "..match_file_name)
    return nil
end

--加载控制比赛文件
function matchConfCmd.saveDb(json_obj)

    local configpath = skynet.getenv "config_path"

    local save_file = configpath.."/"..match_file_name

    skynet.error("start matchConfCmd.saveDb file:",save_file)

    local ok, json_str  = pcall(cjson.encode, json_obj)
    if ok then
        util.file_save(save_file, json_str)
        return true
    end

    skynet.error("matchConfCmd.saveDb fail:",save_file)
    
    return false
end

--加载单个比赛配置文件
function matchConfCmd.loadConfigItem(match_config)
   local configpath = skynet.getenv "config_path"

    local f = io.open(configpath.."/"..match_config, "r")
    if not f then
        skynet.error("loadContrl file fail :"..match_config)
        return nil
    end

    local jsonstr = f:read("*a")
    
    f:close()

    return jsonstr
end

function matchConfCmd.loaddefalut(max_number, match_type)
    local jsonstr = full_match_defalut
    local lmatch_type = tonumber(match_type)
    if lmatch_type == 1 then
        skynet.error("loaddefalut 1", full_match_defalut)
        jsonstr = full_match_defalut

    elseif lmatch_type == 2 then
        skynet.error("loaddefalut 2", time_match_defalut)
        jsonstr = time_match_defalut
    else
        return [[{"error":"no find match_type"}]]
    end

    return string.format([[{"max_number":%d,"default_config":%s}]],max_number, jsonstr)
end

function matchConfCmd.load_select_def()
  
    return match_select_type
end

return matchConfCmd
