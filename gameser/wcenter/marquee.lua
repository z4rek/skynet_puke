local skynet = require "skynet"
local cluster = require "cluster"
local mysql = require "mysql"
require "skynet.manager"
local db = require "dblib"

local CMD = {}
local MARQUEE = {}
local dataser = nil 

function MARQUEE.SendMarqueeMsg(...)
    if  dataser then 
        cluster.send("master", dataser,"send_BoradMsg",...)
    else        
        dataser = cluster.query("master", "centerser")
    end
    
end


function RequireMarqueeMsg()--send_BoradMsg(gameid, istable, msg)
    while true do 
        local res1, res2 = db.call("call  pr_GetMarqueeMsg()")
        if #res1[1] > 0 then
            for index,table_obj in pairs(res1[1]) do           
                pcall(MARQUEE.SendMarqueeMsg,table_obj['nGameId'],table_obj['Istable'],table_obj['sMsg'])                   
            end
        end
        skynet.sleep(6000)
    end
end

function CMD.RequireMarqueeMsg()--立即发送

    local res1, res2 = db.call("call  pr_GetMarqueeMsg()")
    if #res1[1] > 0 then
        for index,table_obj in pairs(res1[1]) do
            skynet.error("[marquee] RequireMarqueeMsg Now Send ",table_obj['nGameId'],table_obj['Istable'],table_obj['sMsg'])
            MARQUEE.SendMarqueeMsg(table_obj['nGameId'],table_obj['Istable'],table_obj['sMsg'])
        end
    end
 
end

function CMD.start(...)

    skynet.fork(RequireMarqueeMsg)
    
end


skynet.start(function()
    skynet.error("marquee start...")
	skynet.dispatch("lua", function(_,_, command, ...)
		local f = CMD[command]
		skynet.ret(skynet.pack(f(...)))
	end)
    skynet.register ".marquee"    
    --centerserver = cluster.snax("master", "centerser")
    math.randomseed(os.time())
end)