local needCardCmd = {}

local function Getkey(paymen, round, playcount)
    return string.format('%d%d%d', paymen, round, playcount)
end

local function GetkeyNnmj(paymen, round, playcount)
    return Getkey(paymen, round, playcount)
end

local function GetkeyBaiZ(paymen, round, playcount)
   return Getkey(paymen, round, playcount)
end

local function GetkeyShuiY(paymen, round, playcount)
   return Getkey(paymen, round, playcount)
end

local function GetkeyShiSanZ(paymen, round, playcount)
   return Getkey(paymen, round, playcount)
end

local function GetkeyWuShiK(paymen, round, playcount)
   return Getkey(paymen, round, playcount)
end

local function GetkeyDDZ(paymen, round, playcount)
   return Getkey(paymen, round, playcount)
end

local function GetkeyNTCP(paymen, round, playcount)
   return Getkey(paymen, round, playcount)
end

local function GetkeyNTDDZ(paymen, round, playcount)
    return Getkey(paymen, round, playcount)
end

local tabNeedCard = {
    --paymen, round, playcount
    ['mj'] = {
        [GetkeyNnmj(1, 4, 4)] = 20,
        [GetkeyNnmj(1, 8, 4)] = 30,
        [GetkeyNnmj(1, 16, 4)] = 60,
        [GetkeyNnmj(4, 4, 4)] = 6,
        [GetkeyNnmj(4, 8, 4)] = 8,
        [GetkeyNnmj(4, 16, 4)] = 16,

        [GetkeyNnmj(1, 4, 3)] = 16,
        [GetkeyNnmj(1, 8, 3)] = 22,
        [GetkeyNnmj(1, 16, 3)] = 44,
        [GetkeyNnmj(4, 4, 3)] = 6,
        [GetkeyNnmj(4, 8, 3)] = 8,
        [GetkeyNnmj(4, 16, 3)] = 16,

        [GetkeyNnmj(1, 4, 2)] = 10,
        [GetkeyNnmj(1, 8, 2)] = 16,
        [GetkeyNnmj(1, 16, 2)] = 30,
        [GetkeyNnmj(4, 4, 2)] = 6,
        [GetkeyNnmj(4, 8, 2)] = 8,
        [GetkeyNnmj(4, 16, 2)] = 16
    },

    ['baiz'] = {
        [GetkeyBaiZ(1, 15, 2)] = 10,
        [GetkeyBaiZ(1, 15, 3)] = 14,
        [GetkeyBaiZ(1, 15, 4)] = 20,
        [GetkeyBaiZ(1, 15, 6)] = 30,

        [GetkeyBaiZ(1, 30, 2)] = 10,
        [GetkeyBaiZ(1, 30, 3)] = 14,
        [GetkeyBaiZ(1, 30, 4)] = 20,
        [GetkeyBaiZ(1, 30, 6)] = 30,

        [GetkeyBaiZ(1, 60, 2)] = 20,
        [GetkeyBaiZ(1, 60, 3)] = 30,
        [GetkeyBaiZ(1, 60, 4)] = 40,
        [GetkeyBaiZ(1, 60, 6)] = 60,

        [GetkeyBaiZ(1, 90, 2)] = 30,
        [GetkeyBaiZ(1, 90, 3)] = 44,
        [GetkeyBaiZ(1, 90, 4)] = 60,
        [GetkeyBaiZ(1, 90, 6)] = 90,

        [GetkeyBaiZ(1, 120, 2)] = 40,
        [GetkeyBaiZ(1, 120, 3)] = 60,
        [GetkeyBaiZ(1, 120, 4)] = 80,
        [GetkeyBaiZ(1, 120, 6)] = 120,

        [GetkeyBaiZ(4, 15, 0)] = 4,
        [GetkeyBaiZ(4, 30, 0)] = 40,
        [GetkeyBaiZ(4, 60, 0)] = 14,
        [GetkeyBaiZ(4, 120, 0)] = 20
    },

    ['shuiy'] = {
        [GetkeyShuiY(1, 10, 2)] = 4,
        [GetkeyShuiY(1, 10, 3)] = 6,
        [GetkeyShuiY(1, 10, 4)] = 8,
        [GetkeyShuiY(1, 10, 6)] = 10,

        [GetkeyShuiY(1, 20, 2)] = 8,
        [GetkeyShuiY(1, 20, 3)] = 10,
        [GetkeyShuiY(1, 20, 4)] = 14,
        [GetkeyShuiY(1, 20, 6)] = 20,

        [GetkeyShuiY(1, 30, 2)] = 10,
        [GetkeyShuiY(1, 30, 3)] = 14,
        [GetkeyShuiY(1, 30, 4)] = 20,
        [GetkeyShuiY(1, 30, 6)] = 30,

        [GetkeyShuiY(4, 10, 0)] = 2,
        [GetkeyShuiY(4, 20, 0)] = 4,
        [GetkeyShuiY(4, 30, 0)] = 6
    },

    ['shisz'] = {
        [GetkeyShiSanZ(1, 10, 2)] = 8,
        [GetkeyShiSanZ(1, 10, 3)] = 10,
        [GetkeyShiSanZ(1, 10, 4)] = 14,

        [GetkeyShiSanZ(1, 20, 2)] = 12,
        [GetkeyShiSanZ(1, 20, 3)] = 16,
        [GetkeyShiSanZ(1, 20, 4)] = 20,

        [GetkeyShiSanZ(1, 30, 2)] = 16,
        [GetkeyShiSanZ(1, 30, 3)] = 20,
        [GetkeyShiSanZ(1, 30, 4)] = 30,

        [GetkeyShiSanZ(1, 60, 2)] = 32,
        [GetkeyShiSanZ(1, 60, 3)] = 40,
        [GetkeyShiSanZ(1, 60, 4)] = 60,

        [GetkeyShiSanZ(4, 10, 0)] = 4,
        [GetkeyShiSanZ(4, 20, 0)] = 6,
        [GetkeyShiSanZ(4, 30, 0)] = 8,
        [GetkeyShiSanZ(4, 60, 0)] = 8
    },

    ['wusk'] = {
        [GetkeyWuShiK(1, 2, 0)] = 20,
        [GetkeyWuShiK(1, 4, 0)] = 30,
        [GetkeyWuShiK(1, 8, 0)] = 60,

        [GetkeyWuShiK(4, 2, 0)] = 6,
        [GetkeyWuShiK(4, 4, 0)] = 8,
        [GetkeyWuShiK(4, 8, 0)] = 16
    },

    ['ddz'] = {
        [GetkeyDDZ(1, 6, 0)] = 30,
        [GetkeyDDZ(1, 12, 0)] = 60,
        [GetkeyDDZ(1, 20, 0)] = 90,

        [GetkeyDDZ(4, 6, 0)] = 10,
        [GetkeyDDZ(4, 12, 0)] = 20,
        [GetkeyDDZ(4, 20, 0)] = 30
    },
    
    ['ntcp'] = {
        [GetkeyNTCP(1, 6, 0)] = 30,
        [GetkeyNTCP(1, 12, 0)] = 60,
    },

    ['ntddz'] = {
        [GetkeyNTDDZ(1, 6, 0)] = 30,
        [GetkeyNTDDZ(1, 12, 0)] = 60,
    }

}

function needCardCmd.GetNeedCard(paymen, round, playcount, gameid)
    if gameid >= 1001 and gameid <= 1011 then
        return tabNeedCard['mj'][Getkey(paymen, round, playcount)]
    elseif gameid == 2001 then
        if paymen == 4 then 
            playcount = 0
        end

        return tabNeedCard['baiz'][Getkey(paymen, round, playcount)]
    elseif gameid == 2002 then
        if paymen == 4 then 
            playcount = 0
        end

        return tabNeedCard['shuiy'][Getkey(paymen, round, playcount)]
    elseif gameid == 2003 then
        if paymen == 4 then 
            playcount = 0
        end

        return tabNeedCard['shisz'][Getkey(paymen, round, playcount)]
    elseif gameid == 2004 then
        playcount = 0
        return tabNeedCard['wusk'][Getkey(paymen, round, playcount)]
    elseif gameid == 2005 then
        playcount = 0
        return tabNeedCard['ddz'][Getkey(paymen, round, playcount)]
    elseif gameid == 2006 then
        playcount = 0
        return tabNeedCard['ntcp'][Getkey(paymen, round, playcount)]
    elseif gameid == 2007 then
        playcount = 0
        return tabNeedCard['ntddz'][Getkey(paymen, round, playcount)]
    end
end

return needCardCmd

-- print (GetNeedCard(1, 4, 4, 2003))
