local skynet = require "skynet"
local cluster = require "cluster"
local snax = require "snax"
local db   = require "dblib"

local mclient = skynet.getenv "max_client" 
local lport = skynet.getenv "listen_port" 
local dport = skynet.getenv "debug_port" 
local name  = skynet.getenv "name"

local sqlconf = {
        host = skynet.getenv "sql_host",
        port = skynet.getenv "sql_port",
        database = skynet.getenv "sql_database",
        user = skynet.getenv "sql_user",
        password = skynet.getenv "sql_password",
        max_packet_size = skynet.getenv "sql_max_packet_size",
}

skynet.start(function()
	skynet.error("Server start wcenter")

	if not skynet.getenv "daemon" then
		local console = skynet.newservice("console")
	end
	
    skynet.newservice("debug_console",dport)
    
    cluster.open "center"

    --初如化db为10个连接,在此进程其它地方则直接调用
    db.init(sqlconf, 10)
    local proxyser = skynet.uniqueservice ("proxyser") --启动center
    skynet.send(proxyser, "lua", "start")
    cluster.register("proxyser", proxyser)
    
    --snax.uniqueservice("centerser")             --启动中心服务，负责用户和桌子的创建解散记录
    snax.uniqueservice("hallser")       --启动大厅服务，负责代理开房
    snax.uniqueservice("msgser")        --启动消息服务，负责邮件消息
    snax.uniqueservice("activityser")   --启动活动服务，负责活动和一些其他业务
    snax.uniqueservice("matchser")              --启动比赛协调服务
    snax.uniqueservice("regphoneser")              --启动比赛协调服务  
    snax.uniqueservice("douhua_agent_system")      --启动豆花代理系统

    --skynet.error("Watchdog listen on", lport)

    local marquee = skynet.uniqueservice("marquee") --启动跑马灯服务
    skynet.send(marquee, "lua", "start")

    skynet.newservice("centerweb")              --启动web接口服务，负责实现立即发送跑马灯消息

    skynet.error("Server end wcenter")
    
end)
