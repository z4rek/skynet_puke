local skynet = require "skynet"  

local db = {}

--[[
使用示例

    local db = require "dblib"
    db.call("call XXXX")

    callstr:  请求的查询名，只能是call语句
    res_sql: 第二句访问字串, 是否要求有返回值     
    dbindex: 请求的db对列，有数据库访问严格前后关系
             可使用单一队列， 如为 nil ，轮询访问

    return: 返回两个结果集, 对于有返回参数的存贮过程，只需使用第二个结果集


    local sqlstr = "call pr_MatchChagre("..UserId..","..MatchConf["pay_type"]..","..MatchConf["pay_num"]..",@ReturnVal);"
    local res_sql = "select @ReturnVal;"
    local res1, res2 = db.call(sqlstr, res_sql)
    
    print(res2[1]['@ReturnVal'])
    
--]]

function db.call(callstr, res_sql, dbindex)
    return skynet.call(".powerdb", "lua", "query", callstr, res_sql, dbindex)
end

--[[

数据库初始化

local sqlconf = {
        host = skynet.getenv "sql_host",
        port = skynet.getenv "sql_port",
        database = skynet.getenv "sql_database",
        user = skynet.getenv "sql_user",
        password = skynet.getenv "sql_password",
        max_packet_size = skynet.getenv "sql_max_packet_size",
}

db.init(sqlconf, 10)

--]]

function db.init(conf, max_db_num)
    local power_db = skynet.uniqueservice ("dbser")
    skynet.call(power_db, "lua", "runService", conf, max_db_num)
end

return db
