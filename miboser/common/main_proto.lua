local CMD = {}

CMD.Main_ClientAndGameSvr_UserModle      =300; 
--子码
CMD.Sub_ClientToGameSvr_UserModle_Base   =1000;
CMD.Sub_GameSvrToClient_UserModle_Base   =2000;

--客户端到服务器
CMD.Sub_ClientToGameSvr_UserModle_CreateAccountReq   = CMD.Sub_ClientToGameSvr_UserModle_Base + 1;   
CMD.Sub_ClientToGameSvr_UserModle_LoginReq           = CMD.Sub_ClientToGameSvr_UserModle_Base + 2;
CMD.Sub_ClientToGameSvr_UserModle_LogoutReq          = CMD.Sub_ClientToGameSvr_UserModle_Base + 3;
CMD.Sub_ClientToGameSvr_UserModle_WriteSignReq       = CMD.Sub_ClientToGameSvr_UserModle_Base + 4;   --客户端修改签名
CMD.Sub_ClientToGameSvr_UserModle_GetZhanJiInforReq	 = CMD.Sub_ClientToGameSvr_UserModle_Base + 5;	--获取战绩详情
CMD.Sub_ClientToGameSvr_UserModle_PlayBackReq        = CMD.Sub_ClientToGameSvr_UserModle_Base + 6;   --客户端请求回放
CMD.Sub_ClientToGameSvr_UserModle_EndPlayBackReq     = CMD.Sub_ClientToGameSvr_UserModle_Base + 7;   --客户端请求结束回放
CMD.Sub_ClientToGameSvr_UserModle_PlayBackCtrl       = CMD.Sub_ClientToGameSvr_UserModle_Base + 8;   --客户端请求回放控制,上一步等等
CMD.Sub_ClientToGameSvr_UserModle_PlayBackClientReady= CMD.Sub_ClientToGameSvr_UserModle_Base + 9;   --客户端请求回放已就绪,进入了桌子
CMD.Sub_ClientToGameSvr_UserModle_PlayBackFrameCtrl  = CMD.Sub_GameSvrToClient_UserModle_Base + 10;  --牌局回放帧控制
CMD.Sub_ClientToGameSvr_UserModle_GetRecShareCodeReq = CMD.Sub_GameSvrToClient_UserModle_Base + 11;  --获得牌局分享码
CMD.Sub_ClientToGameSvr_UserModle_PlayRecByShareCode = CMD.Sub_GameSvrToClient_UserModle_Base + 12;  --请求通过分享码播放牌局录像
CMD.Sub_ClientToGameSvr_UserModle_GateInfor          = CMD.Sub_ClientToGameSvr_UserModle_Base + 13;  --网关返回连接的外部IP等信息
CMD.Sub_ClientToGameSvr_UserModle_ZhanjiReq          = CMD.Sub_ClientToGameSvr_UserModle_Base + 14;  --客户端请求战绩信息
CMD.Sub_ClientToGameSvr_UserModle_UserUpdateInfoReq  = CMD.Sub_ClientToGameSvr_UserModle_Base + 18;  --客户端请求更新信息
CMD.Sub_ClientToGameSvr_UserModle_UserAuthedInfoReq  = CMD.Sub_ClientToGameSvr_UserModle_Base + 19;  --客户端请求认证信息

--服务器到客户端
CMD.Sub_GameSvrToClient_UserModle_CreateAccount_Succ = CMD.Sub_GameSvrToClient_UserModle_Base + 1; 
CMD.Sub_GameSvrToClient_UserModle_CreateAccount_Fail = CMD.Sub_GameSvrToClient_UserModle_Base + 2; 
CMD.Sub_GameSvrToClient_UserModle_LoginSucc          = CMD.Sub_GameSvrToClient_UserModle_Base + 3;
CMD.Sub_GameSvrToClient_UserModle_LoginFail          = CMD.Sub_GameSvrToClient_UserModle_Base + 4;

CMD.Sub_GameSvrToClient_UserModle_OfflineBack        = CMD.Sub_GameSvrToClient_UserModle_Base +6;    --断线重回
CMD.Sub_GameSvrToClient_UserModle_PlayerBasicUpdate  = CMD.Sub_GameSvrToClient_UserModle_Base +7;
CMD.Sub_GameSvrToClient_UserModle_HingMsg            = CMD.Sub_GameSvrToClient_UserModle_Base +8;
CMD.Sub_GameSvrToClient_UserModle_ZhanJi             = CMD.Sub_GameSvrToClient_UserModle_Base +9;    --战绩信息
CMD.Sub_GameSvrToClient_UserModle_RoomID_Error       = CMD.Sub_GameSvrToClient_UserModle_Base +10;
CMD.Sub_GameSvrToClient_UserModle_WriteSignResp      = CMD.Sub_GameSvrToClient_UserModle_Base +11;   --服务器返回修改签名结果
CMD.Sub_GameSvrToClient_UserModle_GetZhanJiInfor	 = CMD.Sub_GameSvrToClient_UserModle_Base +12;   --服务器返回战绩详情信息
CMD.Sub_GameSvrToClient_UserModle_PlayBackRetInfo    = CMD.Sub_GameSvrToClient_UserModle_Base +13;   --牌局回放请求信息返回结果
CMD.Sub_GameSvrToClient_UserModle_PlayBackFrameIndex = CMD.Sub_GameSvrToClient_UserModle_Base +14;   --牌局回放的帧index
CMD.Sub_GameSvrToClient_UserModle_RecShareCodeResp   = CMD.Sub_GameSvrToClient_UserModle_Base +15;   --牌局的分享码
CMD.Sub_GameSvrToClient_UserModle_HingMsgBuyZuan     = CMD.Sub_GameSvrToClient_UserModle_Base +24;	--加入和邀请时钻石不足消息协议
CMD.Sub_GameSvrToClient_UserModle_UserInformation    = CMD.Sub_GameSvrToClient_UserModle_Base +27;  --查询自己个人信息返回结果(大厅个人信息界面)


--大厅交互指令

--主码
 CMD.Main_ClientAndGameSvr_LobbyModle     =301;       

--子码的开始索引
 CMD.Sub_ClientToGameSvr_LobbyModle_Base  =1000;      --客户端到服务器
 CMD.Sub_GameSvrToClient_LobbyModle_Base  =2000;      --服务器到客户端

--客户端到服务器
 CMD.Sub_ClientToGameSvr_LobbyModle_CreateRoomReq         = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 1;          --创建房间(桌子)
 CMD.Sub_ClientToGameSvr_LobbyModle_EnterRoomReq          = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 2;          --进房间(桌子)
 CMD.Sub_ClientToGameSvr_LobbyModle_LogouReq              = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 3;          --退出登录

 CMD.Sub_ClientToGameSvr_LobbyModle_ChangeJinBiReq        = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 5;          --改变金币请求
 CMD.Sub_ClientToGameSvr_LobbyModle_NewYear_UserRecordReq = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 6;          --新年活动用户请求中奖记录
 CMD.Sub_ClientToGameSvr_LobbyModle_NewYear_UserShakeResult   = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 7;      --新年活动上传摇奖完成情况
 CMD.Sub_ClientToGameSvr_LobbyModle_ReqExternCode         = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 8;          --请求扩展码
 CMD.Sub_ClientToGameSvr_LobbyModle_NewYear_UserApplyReq  = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 9;          --新年活动用户申请兑奖
 CMD.Sub_ClientToGameSvr_LobbyModle_BuyDiamondReq         = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 10;         --买钻石请求
 CMD.Sub_ClientToGameSvr_LobbyModle_PreCreateRoomReq      = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 11;         --Lua版客户端，发起预创建房间请求
 CMD.Sub_ClientToGameSvr_LobbyModle_PreEnterRoomReq       = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 12;         --Lua版客户端，发起预进房间(桌子)请求
 CMD.Sub_ClientToGameSvr_LobbyModle_GetServerTimeReq      = CMD.Sub_ClientToGameSvr_LobbyModle_Base + 13;         --获取服务器时间


--------------------------------
--服务器到客户端
 CMD.Sub_GameSvrToClient_LobbyModle_CreateRoomSucc        = CMD.Sub_GameSvrToClient_LobbyModle_Base +1;
 CMD.Sub_GameSvrToClient_LobbyModle_EnterRoomSucc         = CMD.Sub_GameSvrToClient_LobbyModle_Base +2;
 CMD.Sub_GameSvrToClient_LobbyModle_RoomOfflineBackSucc   = CMD.Sub_GameSvrToClient_LobbyModle_Base +3;       --游戏断线返回完成
 CMD.Sub_GameSvrToClient_LobbyModle_UpdateNoticeMsg       = CMD.Sub_GameSvrToClient_LobbyModle_Base +4;
 CMD.Sub_GameSvrToClient_LobbyModle_UpdateBuyFangKa       = CMD.Sub_GameSvrToClient_LobbyModle_Base +5;
 CMD.Sub_GameSvrToClient_LobbyModle_HideBuyFangKa         = CMD.Sub_GameSvrToClient_LobbyModle_Base +6;

 CMD.Sub_GameSvrToClient_LobbyModle_NewYear_Msg           = CMD.Sub_GameSvrToClient_LobbyModle_Base +9;       --新年活动下发广播消息
 CMD.Sub_GameSvrToClient_LobbyModle_NewYear_UserRecordResp= CMD.Sub_GameSvrToClient_LobbyModle_Base +10;      --新年活动下发用户中奖记录
 CMD.Sub_GameSvrToClient_LobbyModle_NewYear_StartPrize    = CMD.Sub_GameSvrToClient_LobbyModle_Base +11;      --新年活动开始摇奖
 CMD.Sub_GameSvrToClient_LobbyModle_NewYear_PrizeData     = CMD.Sub_GameSvrToClient_LobbyModle_Base +12;      --新年活动下发用户中奖信息
 CMD.Sub_GameSvrToClient_LobbyModle_NewYear_UserPayInfo   = CMD.Sub_GameSvrToClient_LobbyModle_Base +13;      --新年活动下发用户申请，兑奖金额信息

 CMD.Sub_GameSvrToClient_LobbyModle_RoomInfo              = CMD.Sub_GameSvrToClient_LobbyModle_Base +14;      --房间信息
 CMD.Sub_GameSvrToClient_LobbyModle_GoldRank              = CMD.Sub_GameSvrToClient_LobbyModle_Base +15;      --充金榜
 CMD.Sub_GameSvrToClient_LobbyModle_PreCreateResult       = CMD.Sub_GameSvrToClient_LobbyModle_Base +16;      --服务器返回预创建房间结果
 CMD.Sub_GameSvrToClient_LobbyModle_PreEnterResult        = CMD.Sub_GameSvrToClient_LobbyModle_Base +17;      --服务器返回预进入房间结果
 CMD.Sub_GameSvrToClient_LobbyModle_EnterRoomFail         = CMD.Sub_GameSvrToClient_LobbyModle_Base +18;      --客户端进入桌子失败(针对预进入时会有多个客户端尝试进入同一个桌子时)
 CMD.Sub_GameSvrToClient_LobbyModle_GetServerTimeResp     = CMD.Sub_GameSvrToClient_LobbyModle_Base +19;      --返回服务器时间

 CMD.Sub_GameSvrToClient_LobbyModle_Ext                   = CMD.Sub_GameSvrToClient_LobbyModle_Base +500;     --预留扩展


--/******************************************
--	VOICESRV_MODULE
--*******************************************/
 CMD.Main_ClientAndVoiceSvr_Modle						=400;  

 CMD.Sub_ClientToVoiceSvr_Modle_Base					=1000;
 CMD.Sub_VoiceSvrToClient_Modle_Base					=2000;


 CMD.Sub_ClientToVoiceSvr_LoginVoiceSrvReq			= CMD.Sub_ClientToVoiceSvr_Modle_Base + 1;	--客户端要求登录语音服务器 
 CMD.Sub_ClientToVoiceSvr_ReLoginVoiceSrvReq		= CMD.Sub_ClientToVoiceSvr_Modle_Base + 2;	--客户端要求登录语音服务器 
 CMD.Sub_ClientToVoiceSvr_SendVoiceReq				= CMD.Sub_ClientToVoiceSvr_Modle_Base + 3;	--客户端要求语音服务器转播语音
 CMD.Sub_ClientToVoiceSvr_LogoutReq					= CMD.Sub_ClientToVoiceSvr_Modle_Base + 4;	--客户端下线
 
 CMD.Sub_VoiceSvrToClient_SendVoice					= CMD.Sub_VoiceSvrToClient_Modle_Base + 1;  --服务端组播语音给当前桌子玩家
--代理开房协议 主码
CMD.Main_Hall_CMD = 215
 --子码
CMD.Sub_HallToGameSvr_CMD_Base           = 1000;
CMD.Sub_GameSvrToHall_CMD_Base           = 2000;

CMD.Sub_CreateAgentTableReq2             = CMD.Sub_HallToGameSvr_CMD_Base + 102;   --创建代理桌2次确定
CMD.Sub_AgentGetRoomList                 = CMD.Sub_HallToGameSvr_CMD_Base + 103;   --代理获取等待桌列表
CMD.Sub_AgentDismissTableReq             = CMD.Sub_HallToGameSvr_CMD_Base + 104;   --代理解散桌子请求
CMD.Sub_ClientUpdateDiamondReq           = CMD.Sub_HallToGameSvr_CMD_Base + 105;   --客户端请求刷新钻石

CMD.Sub_RegToHall                        = CMD.Sub_GameSvrToHall_CMD_Base + 100;   --注册游戏服务器
CMD.Sub_CreateAgentTableResp2            = CMD.Sub_GameSvrToHall_CMD_Base + 102;   --创建代理桌返回结果
CMD.Sub_AgentRoomList                    = CMD.Sub_GameSvrToHall_CMD_Base + 103;   --代理获取等待桌列表
CMD.Sub_AgentDismissTableResp            = CMD.Sub_GameSvrToHall_CMD_Base + 104;   --代理解散桌子结果
CMD.Sub_NotifyClientUpdateDiamond        = CMD.Sub_GameSvrToHall_CMD_Base + 105;   --通知客户端刷新钻石
CMD.Sub_CiientUpdateDiamondResp          = CMD.Sub_GameSvrToHall_CMD_Base + 106;   --返回钻石信息
CMD.Sub_NotifyTableDismiss               = CMD.Sub_GameSvrToHall_CMD_Base + 107;   --通知桌子解散
return CMD