local skynet = require "skynet"

local dport = skynet.getenv "debug_port" 

skynet.start(function()
	skynet.error("Server start wcenter")

	if not skynet.getenv "daemon" then
		local console = skynet.newservice("console")
	end
	
    skynet.newservice("debug_console",dport)

    skynet.newservice("switch")      

    skynet.newservice("centerweb")           

    skynet.error("Server end wcenter")
    
end)
