local skynet = require "skynet"
local sharemap = require "sharemap"
local sraw = require "stable.raw"
local skynet = require "skynet"
local sharemap = require "sharemap"

local mode = ...

sraw.init()
function testdb()

    local a  = sraw.create()

    for j=1,10 do
        local b  = sraw.create()
        b.world0 = j
        b.world1 = j
        b.world2 = j
        b.world3 = "testdsfdf"..j
        a[j] = b
    end
    return a
end    

function printtable(t)
    for k,v in pairs(t) do
        if v then
            print(k,v.world0)
            print(k,v.world1)
            print(k,v.world2)
            print(k,v.world3)
        end
    end
end

if mode == "slave" then
--slave

local function dump(reader)
    reader:update()
    print("x=", reader.x)
    print("y=", reader.y)
    print("s=", reader.s)
end

skynet.start(function()
 local reader
 skynet.dispatch("lua", function(_,_,cmd,t)
         if cmd == "init" then
             reader = t
             print("-")
         else
             assert(cmd == "ping")
             print("*")
             printtable(reader) 
         end
         skynet.ret()
     end)
 end)
 
 else
 -- master
 skynet.start(function()
    local slave= {}
    local t = testdb()
    for i=1,5 do
        slave[i] = skynet.newservice(SERVICE_NAME, "slave")
        skynet.call(slave[i], "lua", "init",  t)
    end

    skynet.sleep(5)

    for i=1,5 do
        skynet.call(slave[i], "lua", "ping" )
    end
    
end)

end


