local skynet = require "skynet"
local cjson = require "cjson"
local json_safe = require "cjson.safe"
local util = require "cjson.util"

skynet.start(function()
    
    local sampleJson = [[{"age":"23","testArray":{"array":[8,9,11,14,25]},"Himi":"himigame.com"}]];
    local data = cjson.decode(sampleJson);
    print(data["age"]);
    print(data["testArray"]["array"][1]);   

end)

